<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

                'surname'=> 'required',
                'name'=> 'required',
                'kanaSurname'=> 'required',
                'kanaName'=> 'required',
                'nickname'=> 'required',
                'course'=> 'required',
                'schoolYear'=> 'required',
                'class'=> 'required',
                'attendanceNumber'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'surname.required' => '姓を入力してください',
            'name.required' => '名を入力してください',
            'kanaSurname.required' => '名字をカタカナで入力お願いします',
            'kanaName.required' => '名前を入力してください',
            'nickname.required' => 'ニックネームを入力してください',
            'course.required' => 'コースを入力してください',
            'schoolYear.required' => '学年を入力してください',
            'class.required' => 'クラスを入力してください',
            'attendanceNumber.required' => '出席番号を入力する必要があります'
        ];
    }
}
