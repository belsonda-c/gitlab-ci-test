<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'course' => 'required',
            'schoolYear' => 'required',
            'schoolClass' => 'required',
            'addtl' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'course.required' => 'コースが必要です',
            'schoolYear.required' => '学年が必要です',
            'schoolClass.required' => '学校の授業が必要です',
            'addtl.required' => '追加人数を入力してください'
        ];
    }
}
