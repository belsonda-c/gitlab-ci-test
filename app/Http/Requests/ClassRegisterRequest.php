<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'schoolYear' => 'required',
            'class' => 'required',
            'studentCount' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'schoolYear.required' => '学年を入力してください',
            'class.required' => 'クラスを入力してください',
            'studentCount.required' => '人数を入力してください',
        ];
    }
}
