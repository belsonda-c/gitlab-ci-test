<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'resDate'=> 'required',
            'resTime'=> 'required',
            'resPart'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'resDate.required'=> '予約日を入力してください',
            'resTime.required'=> '開始時間を入力してください',
            'resPart.required'=> '受講人数を入力してください'
        ];
    }
}
