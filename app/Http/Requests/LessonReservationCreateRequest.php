<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonReservationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_start_time' => 'required',
            'lesson_end_time' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'lesson_start_time.required' => '開始時間が必要です',
            'lesson_end_time.required' => '終了時間が必要です'
        ];
    }
}
