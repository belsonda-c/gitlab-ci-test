<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeacherRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'surname' => 'required',
            'name' => 'required',
            'surnameKana' => 'required',
            'nameKana' => 'required',
            'email' => 'required|unique:teachers,email|regex:/^\b[_A-Za-z0-9+-.]+(\.[_A-Za-z0-9-]+)*(\@[A-Za-z0-9-]+)+(\.[A-Za-z0-9-]+)+(\.[A-Za-z]{2,})\b$/ix'
        ];
    }

    public function messages()
    {
        return [
            'surname.required' => '姓を入力してください',
            'name.required' => '名を入力してください',
            'surnameKana.required' => '姓（カナ）を入力してください',
            'nameKana.required' => '名（カナ）を入力してください',
            'email.required' => 'メールアドレスを入力してください',
            'email.unique' => 'このメールアドレスはすでに登録済み 異なるメールアドレスを入力してください',
            'email.regex' => '正しいメールアドレスを入力してください',
        ];
    }
}
