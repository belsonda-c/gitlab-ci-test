<?php
namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRegisterRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Services\StudentService;
use App\Services\LessonDetailService;

class StudentController extends Controller
{
    private $studentService;
    private $lessonDetailService;

    public function __construct(StudentService $studentService, LessonDetailService $lessonDetailService)
    {
        $this->studentService = $studentService;
        $this->lessonDetailService = $lessonDetailService;
    }

    public function record()
    {
        return $this->studentService->registrationForm();
    }

    public function create()
    {
        $courses = $this->lessonDetailService->courseList();
        $classes = $this->lessonDetailService->classList();
        return view('student.register', compact(['courses', 'classes']));
    }

    public function store(StudentRegisterRequest $request)
    {
        $request->validated();

        $data = $request->request->add([
                    'schooId' => 1,
                    'fname' => 'Lim',
                    'lname' => 'Peter',
                    'kfname' => 'Lum',
                    'klname' => 'Pedro',
                    'nname' => 'Pedro',
                    'update' => 'Peter'
                ]);
        $data = $request->except(['_token','_method']);

        $registerStudent = $this->studentService->create($data);

        if($registerStudent) {
            return response()->json([
                'message' => '登録が完了しました'
            ], 201);
        } else {
            return response()->json([
                'message' => '申請に失敗しました'
            ], 400);
        }
    }

    public function details($id)
    {
        $student = $this->studentService->view($id);

        if (!$student) {
            return response()->json([
                'success' => false,
                'message' => '生徒の情報更新に失敗しました'
            ]);
        }

        return view('student.details', compact(['student']));
    }

    public function edit(Request $request, $id)
    {
        $courses = $this->lessonDetailService->courseList();
        $classes = $this->lessonDetailService->classList();
        $editStudent = $this->studentService->view($id);
        return view('student/update', compact(['editStudent', 'courses', 'classes']));

    }
    public function update(StudentUpdateRequest $request)
    {
        $requestData = $request->validated();
        $data = $request->except(['_token','_method']);
        $updateStudent = $this->studentService->updated($data); 

    }

    public function delete($id)
    {   
        $viewStudent=$this->studentService->view($id);
        return view('student/delete', compact(['viewStudent']));
    }

    public function destroy($id)
    {
        $deleted = $this->studentService->delete($id);

        if (!$deleted) {
            return response()->json([
                'success' => false,
                'message' => '生徒の削除に失敗しました'
            ], 500);
        }

        return view('/student/deleted');    
    }

    public function deleted()
    {   
        return view('student/deleted');
    }

    public function status()
    {
        $registeredClass = $this->studentService->registeredClass();
        return view('student.status', compact(['registeredClass']));
    }

    public function condition($schoolId, $classId)
    {
        $registerCondition = $this->studentService->registerCondition($schoolId, $classId);
        return view('student.condition', compact(['classId', 'registerCondition']));
    }
    
    public function generatePdfDetails($id)
    {
        return $this->studentService->generatePdfDetails($id);
    }

    public function generatePdfByClassListId($id)
    {
        return $this->studentService->generatePdfByClassListId($id);
    }
}
