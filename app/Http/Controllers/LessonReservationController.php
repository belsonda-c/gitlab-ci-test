<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\LessonReservation;
use App\Models\ClassList;
use App\Http\Requests\BookingEditRequest;
use App\Http\Requests\LessonReservationCreateRequest;
use App\Services\LessonReservationService;
use App\Services\LessonDetailService;
use App\Services\MstClassService;
use App\Services\MstCourseService;
use App\Services\MstYearService;
use Illuminate\Pagination\LengthAwarePaginator;

class LessonReservationController extends Controller
{
    const RESERVATION_COMPLETE = '予約が完了しました';
    const RESERVATION_FAILED = '予約が完了できませんでした';
    const CANCEL_FAILED = 'レッスン予約のキャンセルに失敗しました';
    const RESULT_TO_SHOW = 2;
    private $lessonReservationService;
    private $lessonDetailService;
    private $mstClassService;
    private $mstCourseService;
    private $mstYearService;

    public function __construct(LessonReservationService $lessonReservationService, LessonDetailService $lessonDetailService, MstClassService $mstClassService, MstCourseService $mstCourseService, MstYearService $mstYearService)
    {
        $this->lessonReservationService = $lessonReservationService;
        $this->lessonDetailService = $lessonDetailService;
        $this->mstClassService = $mstClassService;
        $this->mstCourseService = $mstCourseService;
        $this->mstYearService = $mstYearService;
    }
    

    public function getAll()
    {
        return $this->lessonReservationService->getAll();
    }

    public function getById(Request $request, $id)
    {
        return $this->lessonReservationService->getById($id, $request);
    }

    public function booking($id, $datetime)
    {
        $courses = $this->lessonDetailService->courseList();
        $years = $this->lessonDetailService->yearList();
        $classes = $this->lessonDetailService->classList();
        $students = $this->lessonDetailService->studentList();
        $reservation = (object)[ 
                            'id' => $id,
                            'datetime' => $datetime
                        ]; 
        $textBooks = $this->lessonDetailService->textBookList();
 
        return view('reservation.create', compact(['courses', 'years', 'classes', 'students', 'reservation', 'textBooks']));
    }

    public function checkClass(Request $request) 
    {
        $checkClass = $this->lessonReservationService->checkClass($request->course, $request->year, $request->class);
    
        if($checkClass) {
            $students = $this->lessonDetailService->classAttendees($checkClass->id);
                        
            return response()->json(array(
                "count" => $students->count(),
                "studentList" => $students,
                "class" => $checkClass->id,
                "exist" => 1
            ));
        } 

        return response()->json(array(
            "count" => 0,
            "studentList" => [],
            "class"=> 0,
            "exist" => 0
        ));
    }

    public function reservation(Request $request) 
    {
        $data = $request->request->add([
            'resUser' => auth()->guard('teacher')->user()->id
        ]);
        $data = $request->except(['_token','_method']);
        $updateBooking = $this->lessonReservationService->before($data);

        if($updateBooking) {
            return response()->json([
                'message' => self::RESERVATION_COMPLETE
            ], 201);
        } else {
            return response()->json([
                'message' => self::RESERVATION_FAILED
            ], 400);
        }       
    }

    public function lists()
    {   
        $data = $this->lessonReservationService->getAll();
        return view('/reservation/list',compact(['data']));   
    }

    public function reserve(BookingEditRequest $request)
    {
        $data = $request->request->add([
            'resUser' => auth()->guard('teacher')->user()->id
        ]);
        $data = $request->except(['_token','_method']);
        $reserveBooking = $this->lessonReservationService->reserve($data);
        $bookedStudent = $reserveBooking->getData();

        if($bookedStudent->status) {
            return response()->json([
                'message' => self::RESERVATION_COMPLETE,
                'data' => $bookedStudent->data
            ], 201);
        } else {
            return response()->json([
                'message' => self::RESERVATION_FAILED
            ], 400);
        }
    }

    public function edit($slot, $class)
    {
        $courses = $this->lessonDetailService->courseList();
        $years = $this->lessonDetailService->yearList();
        $classes = $this->lessonDetailService->classList();
        $students = $this->lessonDetailService->studentList();
        $textBooks = $this->lessonDetailService->textBookList();
        $lessonDetails = $this->lessonDetailService->lessonDetails($slot, $class);

        return view('reservation.edit', compact(['slot', 'class', 'courses', 'years', 'classes', 'students', 'textBooks', 'lessonDetails']));
    }

    public function update(BookingEditRequest $request)
    {
        $data = $request->request->add([
            'resUser' => auth()->guard('teacher')->user()->id
        ]);
        $data = $request->except(['_token','_method']);
        $updateBooking = $this->lessonReservationService->before($data);
        $bookedStudent = $updateBooking->getData();

        if($bookedStudent->status) {
            return response()->json([
                'message' => self::RESERVATION_COMPLETE,
                'data' => $bookedStudent->data
            ], 201);
        } else {
            return response()->json([
                'message' => self::RESERVATION_FAILED
            ], 400);
        }
    }

    public function cancel($id)
    {
        $reservationDtls = $updateBooking  = $this->lessonReservationService->reservationDtls($id);
        return view('reservation.cancel', compact(['reservationDtls']));
    }

    public function deleteByReservationSlotId($id) 
    {
        $deleted = $this->lessonReservationService->deleteByReservationSlotId($id);

        if (!$deleted) {
            return response()->json([
                'success' => false,
                'message' => self::CANCEL_FAILED
            ], 500);
        }

        return view('reservation.deleted');   
    }

    public function index()
    {
        return  view('lesson.reservation'); 
    }

    public function view()
    {
        $lessonReservations = $this->lessonReservationService->getAll();
        return response()->json([
            'lesson_reservations' => $lessonReservations,
            'success' => true
        ], 200);
    }

    public function store(LessonReservationCreateRequest $request)
    {
        $request->validated();

        $data = $request->except(['_token','_method']);
        $createLessonReservation = $this->lessonReservationService->create($data);

        if ($createLessonReservation) {
            return response()->json([
                'message' => 'Lesson has been created'
            ], 201);
        } else {
            return response()->json([
                'message' => 'failed request'
            ], 400);
        }
    }

    public function exists(LessonReservationCreateRequest $request)
    {
        $request->validated();

        $data = $request->except(['_token','_method']);

        if ($this->lessonReservationService->exists($data)){
            return response()->json([
                'message' => 'Slot is not available',
                'conflict' => true
            ], 201);
        } else {
            return response()->json([
                'message' => 'Slot is available',
                'conflict' => false
            ], 404);
        }
    }

    public function destroy($id)
    {
        $deleted = $this->lessonReservationService->delete($id);

        if (!$deleted) {
            return response()->json([
                'success' => false,
                'message' => 'Unable to delete lesson'
            ], 500);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Lesson has been deleted'
            ], 201);
        }
    }
    
    public function list(Request $request)
    {
        $data = $this->lessonReservationService->show();
        $dataMstClass = $this->mstClassService->showMstClass();
        $dataMstCourse = $this->mstCourseService->showMstCourse();
        $dataMstYear = $this->mstYearService->showMstYear();
        $page = LengthAwarePaginator::resolveCurrentPage();
        $pageNumber  = (!empty($request->p))? $request->p : 1;
        $collect = collect($data);
        $paginationData = new LengthAwarePaginator(
            $collect->forPage($request->page ?: 1, self::RESULT_TO_SHOW),
            $collect->count($data), 
            self::RESULT_TO_SHOW, 
            $page,
            ['path'=>url('reservation/list')]
        );
        return view('/reservation/list',compact('paginationData','data','dataMstClass','dataMstCourse','dataMstYear'));   
    }
    
    public function search(Request $request)
    {
        $data = $this->lessonReservationService->show();
        $dataMstClass = $this->mstClassService->showMstClass();
        $dataMstCourse = $this->mstCourseService->showMstCourse();
        $dataMstYear = $this->mstYearService->showMstYear();
        $dataSearch= $this->lessonReservationService->refineSearch($request);
        $page = LengthAwarePaginator::resolveCurrentPage();
        $pageNumber  = (!empty($request->p))? $request->p : 1;
        $collect = collect($dataSearch);
        $paginationDataSearch = new LengthAwarePaginator
        (
            $collect->forPage($request->page ?: 1, self::RESULT_TO_SHOW),
            $collect->count($dataSearch), 
            self::RESULT_TO_SHOW, 
            $page,
            ['path'=>url('reservation/search/'),'query' => $request->query()]
        );
        return view('/reservation/list',compact('data','dataMstClass','dataMstCourse','dataMstYear','paginationDataSearch'));  
    }
}