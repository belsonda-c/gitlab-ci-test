<?php

namespace App\Http\Controllers;

use App\Models\LessonConditionLog;
use Illuminate\Http\Request;
use App\Services\LessonMonitoringService;
use App\Services\LessonReservationService;
use App\Services\ClassListService;
use Illuminate\Support\Facades\Log;

class LessonMonitoringController extends Controller {
    private $lessonMonitoringService;
    private $lessonReservationService;
    private $classListService;

    public function __construct(
      LessonMonitoringService $LMonitoringService,
      LessonReservationService $LRService,
      ClassListService $CLService) 
    {
        $this->lessonMonitoringService = $LMonitoringService;
        $this->lessonReservationService = $LRService;
        $this->classListService = $CLService;
    }

    public function getAll()
    {
        return $this->lessonMonitoringService->getAll();
    }
    
    public function logs($lessonReservationId)
    {
        $lessonReservation = json_decode($this->lessonReservationService->getById($lessonReservationId, 
            null, "schoolReservationSlot"));

        if(!$lessonReservation) {
            return response()->json([
                'success' => false,
                'message' => 'データが見つかりません'
            ]);

        }    
        $classList = json_decode($this->classListService->getById($lessonReservation->class_list_id, null, "masterClass,masterYear"));
        
        $data = array(
            "year" => $classList->master_year->name."".$classList->master_class->name,
            "schedule" => $lessonReservation->school_reservation_slot->reservation_slot_date." ".
                $lessonReservation->school_reservation_slot->start_time." - ".
                $lessonReservation->school_reservation_slot->end_time
        );
        return view("lesson-monitoring.logs", compact(['lessonReservationId']))->with($data);
    }

    public function bellbirdCallback(Request $request)
    {
        $lessonReservationId=1; //temporary lesson reservation id
        $actor_id = $request['peer_id'];
        $action = $request['presence_type'];
        $actor_type = $request['role'];
        $action_datetime = $request['timestamp'];  
        $createLogs= $this->lessonMonitoringService->create($lessonReservationId, $actor_id, $action, $actor_type,  $action_datetime);
    }
}