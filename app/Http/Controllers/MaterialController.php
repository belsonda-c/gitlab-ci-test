<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangeMaterialRequest;
use App\Services\MaterialService;
use App\Services\SchoolReservationSlotService;
use App\Services\LessonDetailService;

class MaterialController extends Controller
{
    private $materialService;
    private $schoolReservationSlotService;
    private $lessonDetailService;

    public function __construct(MaterialService $materialService, SchoolReservationSlotService $schoolReservationSlotService, LessonDetailService $lessonDetailService)
    {
        $this->materialService = $materialService;
        $this->schoolReservationSlotService = $schoolReservationSlotService;
        $this->lessonDetailService = $lessonDetailService;
    }

    public function change($id)
    {
        $reservationDetails = $this->schoolReservationSlotService->schoolSlotDetails($id);
        $textBooks = $this->lessonDetailService->textBookList();
        return view('material.change', compact(['reservationDetails', 'textBooks']));
    }

    public function update(ChangeMaterialRequest $request)
    {
        $request->validated();

        $data = $request->request->add([
            'updateBy' => 88
        ]);
        $data = $request->except(['_token','_method']);
        $updateMaterial = $this->materialService->update($data);

        if($updateMaterial) {
            return response()->json([
                'message' => 'レッスンが更新されました'
            ], 201);
        } else {
            return response()->json([
                'message' => '申請に失敗しました'
            ], 400);
        }
    }
}
