<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Requests\TeacherRegisterRequest;
use App\Http\Requests\TeacherUpdateRequest;
use App\Services\TeacherService;

class TeacherController extends Controller
{
    private $teacherService;

    public function __construct(TeacherService $teacherService)
    {
        $this->teacherService = $teacherService;
    }

    public function dashboard()
    {
        $newDtls = $this->teacherService->newsNotif();
        $classList = $this->teacherService->classList();

        return view('teacher.dashboard', compact(['newDtls', 'classList']));
    }

    public function index()
    {
        $teacherList = $this->teacherService->listAll();

        return view('teacher.management', compact(['teacherList']));
    }

    public function create()
    {
        return view('teacher.register');
    }

    public function store(TeacherRegisterRequest $request)
    {
        $request->validated();

        $data = $request->request->add([
            'schooId' => 1,
            'updateBy' => 88
        ]);        
        $data = $request->except(['_token','_method']);
        $registerTeacher = $this->teacherService->create($data);

        if($registerTeacher) {
            return response()->json([
                'message' => '登録が完了しました'
            ], 201);
        } else {
            return response()->json([
                'message' => '申請に失敗しました'
            ], 400);
        }
    }
    
    public function show($id)
    {
        $teacher = $this->teacherService->find($id);

        if (!$teacher) {
            return response()->json([
                'success' => false,
                'message' => '先生の情報を更新することが出来ませんでした'
            ]);
        }

        return view('teacher.details', compact(['teacher']));
    }

    public function confirmDelete($tid)
    {
        return view('teacher.confirm', compact(['tid']));
    }

    public function remove() {
        return view('teacher.deleted');
    }

    public function edit($tid)
    {
        $teacherData = $this->teacherService->find($tid);
        return view('teacher.update', compact(['teacherData']));
    }

    public function update(TeacherUpdateRequest $request)
    {
        $data = $request->except(['_token','_method']);
        $updateTeacher = $this->teacherService->update($data);

        if($updateTeacher) {
            return response()->json([
                'message' => '登録が完了しました'
            ], 201);
        } else {
            return response()->json([
                'message' => '申請に失敗しました'
            ], 400);
        }
    }
    
    public function destroy($id)
    {
        $deleted = $this->teacherService->delete($id);

        if (!$deleted) {
            return response()->json([
                'success' => false,
                'message' => '先生を削除することが出来ませんでした'
            ], 500);
        }

        return redirect('/teacher/deleted');
    }
}
