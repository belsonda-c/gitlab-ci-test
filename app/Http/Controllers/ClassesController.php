<?php

namespace App\Http\Controllers;

use App\Models\ClassList;
use Illuminate\Http\Request;
use App\Http\Requests\ClassRegisterRequest;
use App\Services\ClassListService;
use App\Services\RareJobStudentService;
use App\Services\StudentService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ClassesController extends Controller
{
    private $classListService;

    public function __construct(ClassListService $classListService,RareJobStudentService $rareJobStudentService, StudentService $studentService)
    {
        $this->classListService = $classListService;
        $this->rareJobStudentService = $rareJobStudentService;
        $this->studentService = $studentService;
    }

    public function showAll()
    {
        $data=$this->classListService->show();
        return view('/class/manage', compact(['data']));   
    }
    
    public function apiGetAll()
    {
        return $this->classListService->show();
    }

    public function apiGetById(Request $request, $id)
    {
        return $this->classListService->getById($id, $request);
    }

    public function search(Request $request)
    {
        $data=$this->classListService->show();
        $class_search=$this->classListService->search($request);
        return view('/class/manage',compact('data','class_search'));
    }
    
    public function classView(Request $request, $id)
    {
        $viewClass=$this->classListService->view($id);
        if (!$viewClass) {
            return response()->json([
                'success' => false,
                'message' => 'Failed to retrieve Class data'
            ]);
        }
        return view('class/view', compact(['viewClass']));
    }
    
    public function register()
    {   
        return view('/class/register');
    }

    public function store(ClassRegisterRequest $request)
    {
       $requestData = $request->validated();
       $data = $request->except(['_token','_method']);
       $schoolId=1; //temporary school id
       for ($x = 1; $x <= $data['studentCount']; $x++) {
            $randPassword = Str::random(15);
            $randEmail = Str::random(10);
            $rarejobData[] = [
                'email' => 'student_temp_email'.$randEmail.'@temp.email',
                'password' => $randPassword
            ];   
        }
        \DB::transaction(function () use($data, $schoolId, $rarejobData){
            $createClass = $this->classListService->create($data, $schoolId);
            foreach($rarejobData as $result) {
                $email = $result['email'];
                $password = $result['password'];
                $rarejobStudent = $this->rareJobStudentService->create($email,$password);
                $createWithClassAttendee = $this->studentService->createWithClassAttendee( $schoolId, $createClass, $rarejobStudent, $password);
            }
        });
    }

    public function confirmDelete(Request $request, $id)
    {
        return view('class.delete', compact(['id']));
    }
   
    public function destroy($id)
    {
        $deleted = $this->classListService->delete($id);

        if (!$deleted) {
            return response()->json([
                'success' => false,
                'message' => 'クラスの削除に失敗しました。'
            ], 500);
        }

        return view('/class/deleted');
    }

    public function remove() {
        return view('/class/manage');
    }
}