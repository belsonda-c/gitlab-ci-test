<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;

use App\Services\AuthService;

class CustomAuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService; 
    }
    
    public function index()
    {
        return view('auth.login');
    }

    public function forget()
    {
        return view('auth.forget');
    }

    public function login(Request $request)
    {
        $login = $this->authService->doLogin($request->only(['school_id', 'email', 'password']));
        if($login) {
            $teacher = Auth::guard('teacher')->user();
            // Check user role here...

            return redirect('/teacher/dashboard');
        }

        return redirect()->back()->with('error', 'ログインに失敗しました。学校ID、メールアドレス、及びパスワードをご確認ください。');
    }

    public function resetLink(ForgotPasswordRequest $request)
    {
        $request->validated();

        $this->authService->storeRequest($request);

        return redirect()->back()->with('success', 'パスワードリセットリンクを送信しました。');
    }

    public function resetForm($token) { 
        $validated = $this->authService->checkToken($token);

        if(!$validated) {
            return view('auth.error', compact(['token']))->with('error', 'パスワードリセットのリンクに誤りがありました。もう一度パスワードリセットを試してください。');
        }

        $expiryDate = $this->authService->checkExpiration($token);

        if(!$expiryDate) {
            return view('auth.error', compact(['token']))->with('error', '有効期限が切れました。もう一度パスワードリセットを試してください。');
        }

        return view('auth.reset', compact(['token']));
    }

    public function passwordReset(ResetPasswordRequest $request)
    {
        $request->validated();

        $validated = $this->authService->checkToken($request->token);

        $this->authService->resetPassword($validated->email, $request->password);
        $this->authService->removeRequest($validated->email);

        return redirect('/login')->with('success', 'パスワードが変更されました。');
    }

    public function signOut() 
    {
        Session::flush();
        Auth::guard('teacher')->logout();
  
        return redirect('/login');
    }
}
