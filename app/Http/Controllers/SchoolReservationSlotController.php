<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SchoolReservationSlotService;

class SchoolReservationSlotController extends Controller
{
    private $schoolReservationSlotService;

    public function __construct(SchoolReservationSlotService $schoolReservationSlotService)
    {
        $this->schoolReservationSlotService = $schoolReservationSlotService;
    }

    public function show()
    {   
        $schoolReservationSlots = $this->schoolReservationSlotService->getAll();

        return response()->json([
            'school_reservation_slots' => $schoolReservationSlots,
            'success' => true
        ], 200);
    }
}
