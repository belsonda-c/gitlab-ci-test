<?php

namespace App\Console\Commands;

use App\Services\LessonReservationService;
use App\Services\TutorSlotService;
use Illuminate\Console\Command;

class LessonMatching extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lesson:match';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lesson matching';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $lessonReservationService = new LessonReservationService();
        $lessonReservations = $lessonReservationService->getReservationsToMatch();

        if (!$lessonReservations || $lessonReservations->isEmpty()) {
            $this->error('[ERROR] No available reservation to match');
            return 1;
        }

        $tutorSlotService = new TutorSlotService();

        \DB::beginTransaction();
        try {
            foreach ($lessonReservations as $lessonReservation) {
                $tutor = $tutorSlotService->getEarliestTutorInSlot($lessonReservation->school_reservation_slot_id);
                if (!$tutor) {
                    $this->error(
                        '[ERROR] No available tutor for school_reservation_slots.id ' . $lessonReservation->school_reservation_slot_id
                    );

                    continue;
                }

                $lessonReservation->tutor_id = $tutor->tutor_id;
                $lessonReservation->status = LessonReservationService::STATUS_CONFIRMED;
                $tutor->status = TutorSlotService::STATUS_BOOKED;

                $this->line(
                    '[INFO] Assigning tutor_id ' .
                    $tutor->tutor_id .
                    ' in reservation_slot_id ' .
                    $lessonReservation->id
                );

                $lessonReservation->save();
                $tutor->save();
            }

            \DB::commit();
            return 0;
        } catch (\Exception $e) {
            \DB::rollBack();
            $this->error('[ERROR]' . $e->getMessage());
            return 1;
        }
    }
}
