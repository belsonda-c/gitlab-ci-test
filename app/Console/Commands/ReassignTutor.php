<?php

namespace App\Console\Commands;

use App\Services\LessonReservationService;
use App\Services\TutorSlotService;
use Illuminate\Console\Command;

class ReassignTutor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lesson:reassign-tutor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reassign another tutor for incoming reservation with absent tutor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $lessonReservationService = new LessonReservationService();
        $lessonReservations = $lessonReservationService->getReservationsToReassign();

        if (!$lessonReservations || $lessonReservations->isEmpty()) {
            $this->info('[INFO] No reservations to reassign tutor');
            return 1;
        }

        $tutorSlotService = new TutorSlotService();

        \DB::beginTransaction();
        try {
            foreach ($lessonReservations as $lessonReservation) {
                $tutorSlot = $tutorSlotService->getEarliestTutorInSlot($lessonReservation->school_reservation_slot_id);
                if (!$tutorSlot) {
                    $this->error(
                        '[ERROR] No available tutor for reassignment. school_reservation_slots.id ' . $lessonReservation->school_reservation_slot_id
                    );

                    continue;
                }

                $prevTutor = $tutorSlotService->findByTutorIdReservationSlotId(
                    $lessonReservation->tutor_id,
                    $lessonReservation->school_reservation_slot_id
                );

                $prevTutor->status = TutorSlotService::STATUS_ABSENT;
                $prevTutor->save();

                if (!$lessonReservation->claas_list_id) {
                    // for individual reservation
                    $lessonReservation->status = LessonReservationService::STATUS_CANCELLED;
                } else {
                    $lessonReservation->tutor_id = $tutorSlot->tutor_id;
                    $tutorSlot->status = TutorSlotService::STATUS_BOOKED;
                    $tutorSlot->save();
                }

                $lessonReservation->save();
            }

            \DB::commit();
            return 0;
        } catch (\Exception $e) {
            \DB::rollBack();
            $this->error('[ERROR]' . $e->getMessage());
            return 1;
        }
    }
}
