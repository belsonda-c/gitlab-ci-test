<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'students';
    protected $primaryKey = 'id';
    protected $fillable = [
        'school_id',
        'class_list_id', 
        'attendance_id', 
        'firstname', 
        'lastname', 
        'kana_firstname', 
        'kana_lastname', 
        'nickname', 
        'password',
        'course_id',
        'year_id',
        'last_login_in',
        'updated_by'
    ];
    public $timestamps = true;

    protected $hidden = [
        'password'
    ];
}
