<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;


class LessonReservation extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'lesson_reservations';
    protected $primaryKey = 'id';
    protected $fillable = [
        'lesson_date',
        'lesson_start_time',
        'lesson_end_time',
        'duration',
        'student_id',
        'class_list_id',
        'tutor_id',
        'mst_textbook_id',
        'memo',
        'status',
        'updated_by'
    ];
    public $timestamps = true;

    public function lessonConditionLogs()
    {
        return $this->hasMany(LessonConditionLog::class);
    }

    public function classList()
    {
        return $this->belongsTo(ClassList::class);
    }

    public function schoolReservationSlot()
    {
        return $this->belongsTo(SchoolReservationSlot::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'id', 'student_id');
    }
}
