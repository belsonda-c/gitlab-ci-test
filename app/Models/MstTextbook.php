<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstTextbook extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'mst_textbooks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'mst_textbook_category_id', 
        'unit',
        'student_url', 
        'teacher_url'
    ];
    public $timestamps = true;
}
