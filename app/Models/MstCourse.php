<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstCourse extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'mst_courses';
    protected $primaryKey = 'id';
    protected $fillable = [
        'school_id',
        'name', 
        'updated_by',
        'created_at', 
        'updated_at'
    ];
    public $timestamps = true;
}
