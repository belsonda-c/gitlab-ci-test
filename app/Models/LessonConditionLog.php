<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonConditionLog extends Model
{
    use HasFactory;

    protected $table = 'lesson_condition_logs';
    protected $primaryKey = 'id';
    
    public $timestamps = true;

    protected $fillable = [
        "lesson_reservation_id",
        "actor_id",
        "actor_type",
        "action",
        "action_datetime"
    ];

    public function lessonReservation()
    {
        return $this->belongsTo(LessonReservation::class);
    }
}
