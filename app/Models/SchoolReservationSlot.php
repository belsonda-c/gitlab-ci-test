<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolReservationSlot extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'school_reservation_slots';
    protected $primaryKey = 'id';
    protected $fillable = [
        'school_id', 
        'reservation_slot_date', 
        'start_time', 
        'end_time',
        'slot_count',
        'created_by',
        'updated_by',
        'deleted_by',
    ];
    public $timestamps = true;

    public function lessonReservation()
    {
        return $this->hasMany('App\Models\LessonReservation', 'school_reservation_slot_id', 'id');
    }
}
