<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MstClass extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'mst_classes';
    protected $primaryKey = 'id';
    protected $fillable = [
        'school_id',
        'name', 
        'created_at', 
        'updated_at'
    ];
    public $timestamps = true;

    public function classList() 
    {
        return $this->hasOne(ClassList::class);
    }
}
