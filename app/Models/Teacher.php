<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Authenticatable
{
    use HasFactory, SoftDeletes;

    protected $table = 'teachers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'school_id',
        'first_name', 
        'last_name', 
        'first_name_kana', 
        'last_name_kana', 
        'email', 
        'password',
        'role',
        'is_verified',
        'updated_by'
    ];
    public $timestamps = true;

    protected $hidden = [
        'password'
    ];
}
