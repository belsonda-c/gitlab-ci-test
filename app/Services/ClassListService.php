<?php

namespace App\Services;

use App\Models\ClassList;
use App\Models\Student;
use App\Models\ClassAttendee;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ClassListService
{
    private $validIncludeProps = [
        "masterClass",
        "masterYear",
    ];

    public function show()
    {
        return Classlist::all();
    }

    public function getById($id, $request, $include =  null) {
        $classList = ClassList::find($id);
        $includeProperty = [];

        if(!$classList) {
            return response()->json([
                'success' => false,
                'message' => 'データが見つかりません'
            ]);

        } 
        
        if($classList && ($request && $request->include) ||  $include) {
            $include = explode(",", $include ? $include : $request->include);

            foreach($this->validIncludeProps as $props) {
                if(in_array($props, $include)) {
                    array_push($includeProperty, $props);
                }
            }
        }

        return $classList->load($includeProperty);
    }

    public function search($request)
    {
        $year_now = $request->get('year_now');
        $course = $request->get('course');
        $class = $request->get('group');
        $fiscal_year = $request->get('fiscal_year');
        return ClassList::where('year_id','LIKE','%'.$year_now.'%')
        ->orWhere('mst_course_id','LIKE','%'.$course.'%')
        ->orWhere('mst_class_id','LIKE','%'.$class.'%')
        ->orWhere('fiscal_year','LIKE','%'.$fiscal_year.'%')->get();   
    }

    public function view($id)
    {
        return Classlist::find($id);
    }

    public function create($data, $schoolId)
    {
        $classSave = new ClassList([
            "school_id" => $schoolId,
            "mst_course_id" => $data['course'],
            "mst_year_id" => $data['schoolYear'],
            "mst_class_id" => $data['class']
        ]);
        if (!$classSave->save()) {
            return false;
        }
        return $classSave;
    }

    public function delete($id)
    {
        return ClassList::where('id', $id)->delete();
    }

   

}