<?php

namespace App\Services;

use App\Models\MstCourse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;

class MstCourseService
{
    
    public function showMstCourse()
    {
        return MstCourse::all();
    }

}