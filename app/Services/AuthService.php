<?php

namespace App\Services;

use Carbon\Carbon;

use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Mail\PasswordResetMail;

class AuthService
{
    public function sendResetPasswordEmail($receiver, $details)
    {
        Mail::to($receiver)->send(new PasswordResetMail($details));        
    }

    public function doLogin($credentials)
    {
        return Auth::guard('teacher')->attempt($credentials);
    }

    public function checkExpiration($token)
    {
        $password_resets = DB::table('password_resets')
                ->where('token', $token)
                ->first();

        $now = Carbon::now();

        if($now >= $password_resets->expires_at) {
            return false;
        }

        return true;
    }

    public function storeRequest(Request $request)
    {
        $receiver = $request->email;
        $token = Str::random(64);
        $expiration = Carbon::now()->addHours(1);
        $details = [
            'token' => $token
        ];

        $this->sendResetPasswordEmail($receiver, $details);

        return DB::table('password_resets')->insert([
            'email' => $receiver, 
            'token' => $token, 
            'expires_at' => $expiration,
            'created_at' => Carbon::now()
        ]);
    }

    public function checkToken($token)
    {
        return  DB::table('password_resets')
            ->where(['token' => $token])
            ->first();
    }

    public function resetPassword($email, $password)
    {
        return Teacher::where('email', $email)
            ->update(['password' => Hash::make($password)]);
    }

    public function removeRequest($email)
    {
        return DB::table('password_resets')
            ->where(['email'=> $email])
            ->delete();
    }
}