<?php

namespace App\Services;

use App\Models\SchoolReservationSlot;

class SchoolReservationSlotService
{
    public function getAll()
    {
        return SchoolReservationSlot::all();
    }

    public function schoolSlotDetails($id)
    {
        return SchoolReservationSlot::where("id", $id)->first();
    }
}