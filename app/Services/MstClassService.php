<?php

namespace App\Services;

use App\Models\MstClass;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;

class MstClassService
{
    
    public function showMstClass()
    {
        return MstClass::all();
    }

}