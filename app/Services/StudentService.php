<?php

namespace App\Services;

use Carbon\Carbon;
use PDF;

use App\Models\Student;
use App\Models\ClassList;
use App\Models\ClassAttendee;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentService
{
    use SoftDeletes;

    const STUDENT_DETAILS_TITLE = '学生の詳細';
    const CLASS_LIST_TITLE = 'クラスリスト';

    public function view($id)
    {
        return Student::find($id);
    }

    public function registrationForm()
    {
        $data = [
            'title' => 'Escholar Student Registration Form',
            'date' => date('m/d/Y')
        ];
          
        $pdf = PDF::loadView('pdf.registration', $data);
    
        return $pdf->download('registration-form.pdf');
    }

    public function create($data)
    {
        $login = Carbon::now();
        $randPassword = Str::random(15);

        $student = new Student([
            "school_id" => $data['schooId'],
            "class_list_id" => $data['schoolClass'],
            "attendance_id" => $data['addtl'],
            "firstname" => $data['fname'],
            "lastname" => $data['lname'],
            "kana_firstname" => $data['kfname'],
            "kana_lastname" => $data['klname'],
            "nickname" => $data['nname'],
            "password" => Hash::make($randPassword),
            "course_id" => $data['course'],
            "year_id" => $data['schoolYear'],
            "last_login_in" => $login,
            "updated_by" => $data['update']
        ]);

        return $student->save();
    }

    public function updated($data)
    {
        $student = Student::where('id', $data['id'])
            ->update([
                "class_list_id"  => $data['class'],
                "attendance_id" => $data['attendanceNumber'],
                "firstname" => $data['name'],
                "lastname" => $data['surname'],
                "kana_firstname" => $data['kanaName'],
                "kana_lastname" => $data['kanaSurname'],
                "nickname" => $data['nickname'],
                "course_id" => $data['course'],
                "year_id" => $data['schoolYear'],
            ]);

        if (!$student) {
            return false;
        }
        
        return true;
    }

    public function delete($id)
    {
        return Student::where('id', $id)->delete();
    }

    public function registeredClass()
    {
        return ClassList::with(['course', 'year', 'class'])->get();
    }

    public function registerCondition($schoolId, $classId)
    {
        return Student::where("school_id", $schoolId)->where("class_list_id", $classId)->get();
    }
    
    public function createWithClassAttendee($schoolId, $createClass, $rarejobStudent, $password)
    {
        $student = new Student([
            "school_id" => $schoolId,
            "class_list_id" => $createClass->id,
            "password" =>  $password,
            "rarejob_student_id" => $rarejobStudent->student->id
        ]);
        $student->save();

        $attendee = new ClassAttendee([
            "class_list_id" => $createClass->id,
            "student_id" => $student->id
        ]);
        $attendee->save();            
    }
    
    public function generatePdfDetails($id)
    {
        $students = Student::select('students.*',  'mc.name AS course')
                        ->leftJoin('mst_courses AS mc', 'mc.id', '=', 'students.course_id')
                        ->whereIn('students.id', $id)
                        ->get();

        $data = [
            'title' => self::STUDENT_DETAILS_TITLE,
            'students' => $students
        ];
          
        $pdf = PDF::loadView('pdf.student-details', $data);
    
        return $pdf->stream('student-details.pdf');
    }

    public function generatePdfByClassListId($id)
    {
        $students = Student::leftJoin('mst_courses AS mc', 'mc.id', '=', 'students.course_id')
                            ->where('students.class_list_id', $id)
                            ->get();

        $data = [
            'title' => self::CLASS_LIST_TITLE,
            'students' => $students
        ];
          
        $pdf = PDF::loadView('pdf.class-list', $data);
    
        return $pdf->stream('class-list.pdf');
    }
}