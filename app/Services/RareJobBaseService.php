<?php

namespace App\Services;

use GuzzleHttp\Client;

class RareJobBaseService
{
    const CATEGORY_TUTOR = '02';
    const CATEGORY_STUDENT = '01';
    const GROUP_ID = '00';
    const PRODUCT_ID = '002';
    const ROLE_STUDENT = 'student';
    const ROLE_TUTOR = 'tutor';

    protected $client;
    protected $base_url;
    protected $options = [];

    protected $api_generate_token = "/token";
    protected $api_auth = "/auth";

    public function __construct()
    {
        $this->client = new Client();
        $this->options['headers'] = [
            'X-Api-Key' => env('RAREJOB_API_KEY'),
            'X-Transaction-ID' => env('RAREJOB_TRANSACTION_ID')
        ];
    }

    protected function generateToken($actor_id, $category)
    {
        // create auth
        $role = '';
        if ($category == self::CATEGORY_STUDENT) {
            $role = self::ROLE_STUDENT;
        } else if ($category == self::CATEGORY_TUTOR) {
            $role = self::ROLE_TUTOR;
        }

        $uuid = $category . '-' . self::GROUP_ID . '-' . $actor_id;
        $this->options['json'] = ['uuid' => $uuid, 'role' => $role];
        $authUri = env('RAREJOB_AUTH_BASE_URL') . $this->api_auth;
        $res = $this->client->request('POST', $authUri, $this->options);

        if ($res->getStatusCode() != 201) {
            return false;
        }

        // generate token
        $this->options['json'] = [
            "product_id" => self::PRODUCT_ID,
            "actor_user_category_id" => $category,
            "actor_user_group_id" => self::GROUP_ID,
            "actor_user_id" => "$actor_id"
        ];

        $generateTokenUri = env('RAREJOB_AUTH_BASE_URL') . $this->api_generate_token;
        $res = $this->client->request('POST', $generateTokenUri, $this->options);
        $res = json_decode($res->getBody());

        return $res->token;
    }

    protected function setBaseUrl($base_url)
    {
        $this->base_url = $base_url;
    }

    protected function request($method, $api)
    {
        $res = $this->client->request($method, $this->base_url . $api, $this->options);

        return json_decode($res->getBody());
    }
}