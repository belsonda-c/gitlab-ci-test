<?php

namespace App\Services;

class RareJobStudentService extends RareJobBaseService
{
    private $api_get_student = "/students/student/{student_id}";
    private $api_create_student = "/students/signup";
    private $api_provisional_reg = "/students/provisional_registration";
    private $api_provisional_reg_verify = "/students/provisional_registration/verification";
    private $api_subscribe_student = "/students/student/{student_id}/subscribe";

    public function __construct()
    {
        $this->setBaseUrl(env('RAREJOB_STUDENT_BASE_URL'));
        parent::__construct();
    }

    public function find($student_id)
    {
        $token = $this->generateToken($student_id, RareJobBaseService::CATEGORY_STUDENT);
        $this->options['headers']['Authorization'] = 'Bearer ' . $token;

        // remove json params for generate of token
        unset($this->options['json']);

        return $this->request('GET', str_replace("{student_id}", $student_id, $this->api_get_student));
    }

    public function provisional_registration($data)
    {
        $this->options['json'] = [
            'category_id' => self::CATEGORY_STUDENT,
            'product_id' => self::PRODUCT_ID,
            'group_id' => self::GROUP_ID,
            'email' => $data['email'],
            'password' => $data['password'],
            'name' => $data['name']
        ];

        return $this->request('POST', $this->api_provisional_reg);
    }

    public function provisional_registration_verify($verifyToken)
    {
        $this->options['headers']['X-Provisional-Token'] = $verifyToken;

        return $this->request('POST', $this->api_provisional_reg_verify);
    }

    public function create($email, $password)
    {
        $this->options['json'] = [
            'category_id' => self::CATEGORY_STUDENT,
            'group_id' => self::GROUP_ID,
            'email' => $email,
            'password' => $password
        ];

        return $this->request('POST', $this->api_create_student);
    }

    public function subscribe($student_id, $product_id)
    {
        $token = $this->generateToken($student_id, RareJobBaseService::CATEGORY_STUDENT);
        $this->options['headers']['Authorization'] = 'Bearer ' . $token;

        // remove json params for generate of token
        unset($this->options['json']);

        // subscribe api params
        $this->options['json'] = ['product_id' => $product_id];

        $subscribeApi = str_replace('{student_id}', $student_id, $this->api_subscribe_student);
        return $this->request('POST', $subscribeApi);
    }
}