<?php

namespace App\Services;

use App\Models\SchoolReservationSlot;
use App\Models\LessonReservation;
use App\Models\ClassAttendee;
use App\Models\ClassList;
use App\Models\MstCourse;
use App\Models\MstYear;
use App\Models\MstClass;
use App\Models\Student;
use App\Models\MstTextbook;

class LessonDetailService
{
    public function courseList()
    {
        return MstCourse::all();
    }

    public function yearList()
    {
        return MstYear::all();
    }

    public function classList()
    {
        return MstClass::all();
    }

    public function studentList()
    {
        return Student::all();
    }

    public function textBookList()
    {
        return MstTextbook::all();
    }

    public function classAttendees($classId)
    {
        return ClassAttendee::where('class_id', '=', $classId)->get();
    }

    public function lessonDetails($slot, $class)
    {
        $slot = SchoolReservationSlot::where('id', $slot)->first()->toArray();
        $lessonBookings = LessonReservation::select('id')->where('class_list_id', $class)->get()->toArray();
        $studentArray = LessonReservation::select('student_id')->where('class_list_id', $class)->get()->toArray();
        $attendees = ClassAttendee::select('student_id')
                    ->where('class_id', $class)
                    ->whereNotIn('student_id', $studentArray)
                    ->get()
                    ->toArray();
        $classList = ClassList::where('id', $class)->first()->toArray();
        $studentCandidate = Student::whereIn('id', $attendees)->get()->toArray();
        $bookedStudent = Student::whereIn('id', $studentArray)->get()->toArray();

        $bookingIds = [];
        foreach($lessonBookings as $booking) {
            $bookingIds[] = $booking['id'];
        }

        return [
            "slotInfo" => $slot,
            "course" => $classList['mst_course_id'],
            "year" => $classList['mst_year_id'],
            "class" => $classList['mst_class_id'],
            "booking" => implode(',', $bookingIds),
            "candidate" => $studentCandidate,
            "students" => $bookedStudent
        ];
    }
}