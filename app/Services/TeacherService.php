<?php

namespace App\Services;

use App\Models\Teacher;
use App\Models\ClassList;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\Mail;
use App\Mail\TeacherRegistrationMail;

class TeacherService
{
    use SoftDeletes;

    public function sendRegistrationEmail($receiver, $details)
    {       
        Mail::to($receiver)->send(new TeacherRegistrationMail($details));        
    }

    public function newsNotif()
    {
        $news = [
            "label" => "2021/11/30",
            "subject" => "ようこそ先生",
            "content" => "伊椅子太郎が新規先生として登録されました。"
        ];

        if(!empty($news)) {
            return $news['label']." ".$news['content'];
        } else {
            return "新着のお知らせはありません。";
        }
    }

    public function listAll()
    {
        return Teacher::all();
    }

    public function classList()
    {
        return ClassList::all();
    }

    public function find($id)
    {
        return Teacher::find($id);
    }

    public function create($data)
    {
        $receiver = $data['email'];
        $randPassword = Str::random(15);

        $teacher = new Teacher([
            "school_id" => $data['schooId'],
            "first_name" => $data['name'],
            "last_name" => $data['surname'],
            "first_name_kana" => $data['surnameKana'],
            "last_name_kana" => $data['nameKana'],
            "email" => $receiver,
            "password" => Hash::make($randPassword),
            "role" => $data['authority']
        ]);

        $details = [
            'content' => '先生の新規登録',
            'password' => $randPassword
        ];

        if($teacher->save()) {
            $this->sendRegistrationEmail($receiver, $details);
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $teacher = Teacher::where('id', $data['id'])
            ->update([
                "school_id" => $data['schooId'],
                "first_name" => $data['name'],
                "last_name" => $data['surname'],
                "first_name_kana" => $data['surnameKana'],
                "last_name_kana" => $data['nameKana'],
                "email" => $data['email'],
                "role" => $data['authority'],
                "updated_by" => $data['updateBy']
            ]);

        if (!$teacher) {
            return false;
        }
        
        return true;
    }

    public function delete($id)
    {
        return Teacher::where('id', $id)->delete();
    }
}