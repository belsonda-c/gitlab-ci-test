<?php

namespace App\Services;

use App\Models\LessonReservation;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialService
{
    use SoftDeletes;

    public function update($data)
    {
        $material = LessonReservation::where('student_id', $data['studentID'])
                    ->update([
                        "mst_textbook_id" => $data['materialID'],
                        "updated_by" => $data['updateBy']
                    ]);

        if (!$material) {
            return false;
        }
        
        return true;
    }
}