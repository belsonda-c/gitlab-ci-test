<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Teacher;
use App\Models\Student;
use App\Models\LessonReservation;
use App\Models\ClassList;
use App\Models\MstClass;
use App\Models\MstCourse;
use App\Models\MstYear;
use App\Models\SchoolReservationSlot;
use App\Models\LessonConditionLog;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;

class LessonReservationService
{
    const STATUS_CONFIRMED = '1';
    const STATUS_ON_GOING = '2';
    const STATUS_CANCELLED = '3';
    const STATUS_PENDING = '4';
    const NO_REPEAT = '1';
    const WEEKLY = '2';
    const MONTHLY = '3';
    const NO_DATA_FOUND = 'データが見つかりません';

    const STATUS_CONFIRMED = 1;
    const STATUS_ONGOING = 2;
    const STATUS_CANCELLED = 3;
    const STATUS_PENDING = 4;

    private $validIncludeProps = [
        "schoolReservationSlot",
        "classList",
        "lessonConditionLogs"
    ];

    function getDateForSpecificDayBetweenDates($startDate, $endDate, $day_number) 
    {
        $day = config('constants.'.$day_number);
        $endDate = strtotime($endDate);
        
        for($i = strtotime($day, strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i)) {
            $date_array[] = date('Y-m-d',$i);
        }
        
        return $date_array;
    }
    
    public function reservationDates($type, $fromDate, $toDate)
    {
        $dayInWord = Carbon::createFromFormat('Y-m-d', $fromDate)->format('l');

        if($type == self::NO_REPEAT) {

            return array($toDate);

        } else if($type == self::WEEKLY) {

            $dayInWord = strtoupper($dayInWord);
            $reserveDate = [];
            $day = config('constants.'.$dayInWord);
            $startDate = Carbon::parse($fromDate)->next($day);
            $endDate = Carbon::parse($toDate);
        
            for ($date = $startDate; $date->lte($endDate); $date->addWeek()) {
                $reserveDate[] = $date->format('Y-m-d');
            }
    
            return $reserveDate;

        } else if($type == self::MONTHLY) {

            $weekPosition = Carbon::createFromFormat('Y-m-d', $fromDate)->weekOfMonth;
            $addNthDay = $this->getDateForSpecificDayBetweenDates($fromDate, $toDate, 4);
            $reserveDate = [];

            foreach($addNthDay as $date) {
                $weekIncluded = Carbon::createFromFormat('Y-m-d', $date)->weekOfMonth;
                if($weekPosition == $weekIncluded) {
                    $reserveDate[] = $date;
                }
            }

            return $reserveDate;

        } 
    }

    public function checkClass($course, $year, $class) 
    {
        return ClassList::where('mst_course_id', '=', $course)
            ->where('mst_year_id', '=', $year)
            ->where('mst_class_id', '=', $class)
            ->first();
    }

    public function reservationDtls($id) 
    {
        $slot = SchoolReservationSlot::with('lessonReservation')->where('id', $id)->first();
        $classList = [];

        foreach($slot->lessonReservation as $lessonInfo) {
            $classList[] = $lessonInfo->class_list_id;
        }

        $classListIds = array_unique($classList);

        $class = ClassList::where('id', $classListIds[0])->first();

        return [
            "slot" => $slot,
            "class" => $class,
            "count" => count($classList)
        ];
    }

    public function before($data)
    {
        $success = false;

        DB::beginTransaction();
        try {
            $slotData = SchoolReservationSlot::where('id', $data['resSlot'])->first();
            $slotId = $slotData->id;
            $slotsAvailable = $slotData->slot_count;
            $students = $data['resStudent'][0];
            $numberOfStudent = count($students);
            $bookingIds = explode(",", $data['resBook']);
            $resType = $data['resType'];
            $resDate = $data['resDate'];
            $resDeadline = $data['resDeadline'];
            $reservationDates = $this->reservationDates($resType, $resDate, $resDeadline);  
            $reservationDatesRef = $this->reservationDates($resType, $resDate, $resDeadline);
    
            $numberOfReservation = count($reservationDates);
            $allowedResPerStud = (int)floor($slotsAvailable / $numberOfStudent);
            $dateToDeny = (int)$numberOfReservation - $allowedResPerStud;
    
            array_splice($reservationDates, $allowedResPerStud, $dateToDeny);           
    
            foreach($students as $stud) {
                $todayDate = Carbon::now();
                foreach($reservationDates as $reservationDate) {
                    $bookings[] = [
                        "lesson_date" => $reservationDate,
                        "school_reservation_slot_id" => $slotId,
                        "duration" => $data['resDuration'],
                        "student_id" => $stud,
                        "class_list_id" => $data['resClass'],
                        "tutor_id" => $data['resTutor'],
                        "mst_textbook_id" => $data['resMaterial'],
                        "memo" => $data['resMemo'],
                        "status" => $data['resStatus'],
                        "updated_by" => $data['resUser'],
                        "created_at" => $todayDate,
                        "updated_at" => $todayDate
                    ];
                }
            }
    
            LessonReservation::insert($bookings);
            LessonReservation::where("school_reservation_slot_id", $slotId)->whereIn("id", $bookingIds)->forceDelete();
            
            $success = true;
            if ($success) {
                DB::commit();  
                return response()->json([
                    "status" => true,
                    "data" => array(
                        "Allowed" => array_splice($reservationDates, 0, $allowedResPerStud),
                        "Denied" => array_splice($reservationDatesRef, $allowedResPerStud, $dateToDeny)
                    )
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function after($data)
    {
        $lesson = LessonReservation::where('id', $data['id'])
            ->update([
                "mst_textbook_id" => $data['resMaterial'],
                "memo" => $data['resMemo']
            ]);

        return $lesson;
    }

    public function reserve($data)
    {
        $success = false;

        DB::beginTransaction();
        try {
            $slotId = $data['resSlot'];
            $slotData = SchoolReservationSlot::where('id', $slotId)->first();

            $slotsAvailable = $slotData->slot_count;
            $students = $data['resStudent'][0];
            $numberOfStudent = count($students);

            $resType = $data['resType'];
            $resDate = $data['resDate'];
            $resDeadline = $data['resDeadline'];
            $reservationDates = $this->reservationDates($resType, $resDate, $resDeadline);
            $reservationDatesRef = $this->reservationDates($resType, $resDate, $resDeadline);

            $numberOfReservation = count($reservationDates);
            $allowedResPerStud = (int)floor($slotsAvailable / $numberOfStudent);
            $dateToDeny = (int)$numberOfReservation - $allowedResPerStud;

            array_splice($reservationDates, $allowedResPerStud, $dateToDeny);

            foreach($students as $stud) {
                $todayDate = Carbon::now();
                foreach($reservationDates as $reservationDate) {
                    $bookings[] = [
                        "lesson_date" => $reservationDate,
                        "school_reservation_slot_id" => $slotData->id,
                        "duration" => $data['resDuration'],
                        "student_id" => $stud,
                        "class_list_id" => $data['resClass'],
                        "tutor_id" => $data['resTutor'],
                        "mst_textbook_id" => $data['resMaterial'],
                        "memo" => $data['resMemo'],
                        "status" => $data['resStatus'],
                        "updated_by" => $data['resUser'],
                        "created_at" => $todayDate,
                        "updated_at" => $todayDate
                    ];
                }
            }
    
            LessonReservation::insert($bookings);
    
            $success = true;
            if ($success) {
                DB::commit();  
                return response()->json([
                    "status" => true,
                    "data" => array(
                        "Allowed" => array_splice($reservationDates, 0, $allowedResPerStud),
                        "Denied" => array_splice($reservationDatesRef, $allowedResPerStud, $dateToDeny)
                    )
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function deleteByReservationSlotId($id)
    {
        LessonReservation::where('id', $id)
        ->update([
            "status" => self::STATUS_CANCELLED
        ]);

        return LessonReservation::where('school_reservation_slot_id', $id)->delete();
    }

    public function create($data)
    {
        $lesson = new LessonReservation([
            "lesson_start_time" => $data['lesson_start_time'],
            "lesson_end_time" => $data['lesson_end_time'],
        ]);

        return $lesson->save();
    }

    public function exists($data)
    {
        return LessonReservation::where('lesson_start_time', '<', $data["lesson_end_time"])
        ->where('lesson_start_time', '>', $data["lesson_start_time"])
        ->where(DB::raw("(STR_TO_DATE(lesson_start_time,'%Y-%m-%d'))"), Carbon::parse($data["lesson_end_time"])->format('Y-m-d'))
        ->exists();
    }

    public function delete($id)
    {
        return LessonReservation::where('id', $id)->delete();
    }

    public function getAll()
    {
        return LessonReservation::all();
    }

    public function getById($id, $request, $include = null)
    {
        $lessonReservation = LessonReservation::find($id);
        $includeProperty = [];

        if(!$lessonReservation) {
            return response()->json([
                'success' => false,
                'message' => self::NO_DATA_FOUND
            ]);

        } 
        
        if($lessonReservation && ($request && $request->include) ||  $include) {
            $include = explode(",", $include ? $include : $request->include);

            foreach($this->validIncludeProps as $props) {
                if(in_array($props, $include)) {
                    array_push($includeProperty, $props);
                }
            }
        }

        return $lessonReservation->load($includeProperty);
    }

    function joinQuery()
    {
        return DB::table("lesson_reservations")
        ->join("school_reservation_slots","lesson_reservations.school_reservation_slot_id","=","school_reservation_slots.id")
        ->join("class_lists","lesson_reservations.class_list_id","=","class_lists.id")
        ->join("mst_textbooks","lesson_reservations.mst_textbook_id","=","mst_textbooks.id")
        ->join("mst_classes","class_lists.mst_class_id","=","mst_classes.id")
        ->join("mst_courses","class_lists.mst_course_id","=","mst_courses.id")
        ->join("mst_years","class_lists.mst_year_id","=","mst_years.id")
        ->leftjoin("students","class_lists.id","=","students.class_list_id");
    }
    
    public function show()
    {
        $result = self::joinQuery()
        ->select("lesson_reservations.*","school_reservation_slots.reservation_slot_date","school_reservation_slots.end_time","mst_years.name as yearName","mst_textbooks.name as textbookName","lesson_reservations.created_at as createdDate","lesson_reservations.updated_at as updatedDate","mst_courses.name as course_name","mst_classes.name as class_name","class_lists.*",DB::raw("count(students.id) as student_count"))
        ->groupBy("lesson_reservations.id")
        ->get();
       
        return $result;
    }

    public function refineSearch($request)
    {
        $course = $request->get('fld-course');
        $schoolYear = $request->get('fld-schoolYear');
        $class = $request->get('fld-class');
        $status = $request->get('fld-status');
        $reservationId = $request->get('fld-id');
        $person = $request->get('fld-person');
        $dateRange = $request->get('daterange');
        
        $string = explode('-',$dateRange);
        $date1 = explode('/',$string[0]);
        $date2 = explode('/',$string[1]);
        $finalDate1 = $date1[2].'-'.$date1[0].'-'.$date1[1];
        $finalDate2 = $date2[2].'-'.$date1[0].'-'.$date2[1];
        
        $startDate=date( "Y-m-d", strtotime($finalDate1));
        $endDate=date( "Y-m-d", strtotime($finalDate2));
        
        $result = $result = self::joinQuery()
        ->select("lesson_reservations.*","school_reservation_slots.reservation_slot_date","school_reservation_slots.end_time","mst_years.name as yearName","mst_textbooks.name as textbookName","lesson_reservations.created_at as createdDate","lesson_reservations.updated_at as updatedDate","mst_courses.name as course_name","mst_classes.name as class_name","class_lists.*",DB::raw("count(students.id) as student_count"))
        ->where('mst_courses.id', '=', $course)
        ->orWhere('mst_years.id', '=', $schoolYear)
        ->orWhere('mst_classes.id', '=', $class)
        ->orWhere('lesson_reservations.status', '=', $status)
        ->orWhere('lesson_reservations.id', '=', $reservationId)
        ->orWhere('lesson_reservations.tutor_id', '=', $person)
        ->orWhereBetween('school_reservation_slots.reservation_slot_date', [$startDate, $endDate])
        ->groupBy("lesson_reservations.id","mst_courses.id")
        ->get();

        return $result;
        
    }

    public function getReservationsToMatch()
    {
        $targetReservationDate = date(
            'Y-m-d',
            strtotime('+' . env('DAYS_BEFORE_LESSON_MATCHING') . ' days')
        );

        $reservationSlots = SchoolReservationSlot::where('reservation_slot_date', '<=', $targetReservationDate)->get();
        if ($reservationSlots->isEmpty()) {
            return [];
        }

        $slotIds = [];
        foreach ($reservationSlots as $reservationSlot) {
            $slotIds[] = $reservationSlot->id;
        }

        return LessonReservation::whereIn('school_reservation_slot_id', $slotIds)
            ->where('tutor_id', 0)
            ->get();
    }

    public function getReservationsToReassign()
    {
        $gracePeriodMinutes = (int) env('MINUTES_BEFORE_TUTOR_REASSIGNMENT');
        $gracePeriodSec = time() + ($gracePeriodMinutes * 60);
        $gracePeriodDate = date('Y-m-d H:i:s', $gracePeriodSec);
        $explodedDateTime = explode(' ', $gracePeriodDate);

        $slots = SchoolReservationSlot::where('reservation_slot_date', $explodedDateTime[0])
            ->where('start_time' >= $explodedDateTime[1])
            ->get();

        if ($slots->isEmpty()) {
            return [];
        }

        $slotIds = [];
        foreach ($slots as $slot) {
            $slotIds[] = $slot->id;
        }

        return LessonReservation::whereIn('school_reservation_slot_id', $slotIds)->get();
    }
}