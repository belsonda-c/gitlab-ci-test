<?php

namespace App\Services;

use App\Models\LessonConditionLog;

class LessonMonitoringService {

    const ACTION_TYPE_JOIN = 1;
    const ACTION_TYPE_UNJOIN = 2;
    const ACTOR_TYPE_STUDENT = 1;
    const ACTOR_TYPE_TEACHER = 2;
    const ACTION_JOIN = 'join';
    const ACTOR_STUDENT = 'student';

    public function getAll()
    {
        return LessonConditionLog::all();
    }

    public function findById($id)
    {
        return LessonConditionLog::find($id);
    }   

    public function findByLessonReservationId($id)
    {
        return LessonConditionLog::where("lesson_reservation_id", $id);
    }

    public function create($lessonReservationId, $actor_id, $action, $actor_type,  $action_datetime)
    {
        if ($action == self::ACTION_JOIN) {
            $action=self::ACTION_TYPE_JOIN;
        } else {
            $action=self::ACTION_TYPE_UNJOIN;
        }
        if ($actor_type == self::ACTOR_STUDENT) {
            $actor_type=self::ACTOR_TYPE_STUDENT;
        } else {
            $actor_type=self::ACTOR_TYPE_TEACHER;
        }
        $logSave = new LessonConditionLog([
            "lesson_reservation_id" => $lessonReservationId,
            "actor_id" => $actor_id,
            "actor_type" => $actor_type,
            "action" => $action,
            "action_datetime" => $action_datetime
        ]);
        if (!$logSave->save()) {
            return false;
        }
        return $logSave;
    }
}