<?php

namespace App\Services;

use App\Models\MstYear;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use DB;

class MstYearService
{
    
    public function showMstYear()
    {
        return MstYear::all();
    }

}