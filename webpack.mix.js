const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .css('resources/css/app.css', 'public/css/')
    .sass("resources/sass/app.scss", "public/css/")
    .scripts("resources/js/lesson-monitoring.js", "public/js/lesson-monitoring.js")
    .copy("resources/js/class-register.js", "public/js/class-register.js")
    .copy("resources/js/input-number.js", "public/js/input-number.js")
    .copy("resources/js/lesson-booking.js", "public/js/lesson-booking.js")
    .copy("resources/js/lesson-edit.js", "public/js/lesson-edit.js")
    .copy("resources/js/lesson-monitoring.js", "public/js/lesson-monitoring.js")
    .copy("resources/js/list-item.js", "public/js/list-item.js")
    .copy("resources/js/material-change.js", "public/js/material-change.js")
    .copy("resources/js/reservation.js", "public/js/reservation.js")
    .copy("resources/js/student-register.js", "public/js/student-register.js")
    .copy("resources/js/student-update.js", "public/js/student-update.js")
    .copy("resources/js/table-sort.js", "public/js/table-sort.js")
    .copy("resources/js/teacher-register.js", "public/js/teacher-register.js")
    .copy("resources/js/teacher-update.js", "public/js/teacher-update.js")
    .copy("resources/js/toasts.js", "public/js/toasts.js")
    .sourceMaps();
