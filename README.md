<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Prerequisites

You should have Docker installed in your machine.

Install Docker for Mac [here](https://docs.docker.com/docker-for-mac/install/) <br>  
and Docker for Windows [here](https://docs.docker.com/docker-for-windows/install/)

## Steps to setup the project

1. `git clone git@gitlab.com:westacton/backend_dev/rarejob/eu-school.git`
2. `cd eu-school`
3. `docker-compose up -d --build`
   - we create and start our containers in docker-compose.yml with the docker-compose up command
   - --build flag to build images before starting its docker containers
   - and -d flag (detached mode) to run everything in the background
   - optional : remove `-d` flag to see realtime logs
4. `docker-compose exec app_eu_school composer install` OR `composer install` (if you have composer installed)
5. `docker-compose exec app_eu_school composer dump-autoload`
6. `cp .env.example .env`
7. open `.env` with an IDE or a text editor then change the value of keys with comments
   - to check your local IP address, open your cmd line interface and type `ifconfig` if on mac and `ipconfig` if on windows
8. change the value of `DB_USERNAME` and `DB_PASSWORD` to your mysql user which has all privileges granted (%)
      - note that it should NOT be `root` user
9. `docker-compose exec app php artisan key:generate`

## Steps to setup the project on Windows Machine

1. `git clone git@git.rarejob.com:rpl/eu-school.git`
2. `cd eu-school`
3. copy `WindowsDockerfile` content into `Dockerfile`
4. `docker-compose up -d --build`
5. `docker build -t app_eu_school ./docker/nginx_php_amazonlinux/`
6. `docker exec -it app_eu_school /bin/bash`
7. `php-fpm`
8. `nginx`
9. run `ps aux` to check if php and nginx is running
10. `composer install`
11. `composer dump-autoload`
12. `cp .env.example .env`
13. open `.env` with an IDE or a text editor then change the value of keys with comments
   - to check your local IP address, open your cmd line interface and type `ifconfig` if on mac and `ipconfig` if on windows
14. change the value of `DB_USERNAME` and `DB_PASSWORD` to your mysql user which has all privileges granted (%)
      - note that it should NOT be `root` user
15. `php artisan key:generate`
16. `php artisan optimize`

## to access the project
`http://{YOUR LOCAL IP}:8080/`


## Laravel mix set-up

1. Download and install nodejs on this site: https://nodejs.org/en/ .
2. You must have to require UI `composer require laravel/ui`.
3. Install bootstrap css via npm command `npm i bootstrap`.
4. Bootstrap icon `npm i bootstrap-icons `.
5. Add this line on resources/sass/app.scss to import bootstrap.
   `@import '~bootstrap/scss/bootstrap';`
   `@import '~bootstrap-icons/font/bootstrap-icons';`
6. Add this line on resources/js/app.js to add jquery.
   `require('./bootstrap');`
7. Add links on header tag.
   `<script src="{{ asset('js/app.js') }}" defer></script>`
   `<link href="{{ asset('css/app.css') }}" rel="stylesheet">`
8. Add this line in webpack.mis.js and needs for compilling assets.
   `mix.js('resources/js/app.js', 'public/js')`
   ` .sass('resources/sass/app.scss', 'public/css')`
   ` .sourceMaps();`
9. after that you need to write `npm install && npm run dev`




