<?php
use Carbon\Carbon;

return [
    '1' => 'Monday',
    '2' => 'Tuesday', 
    '3' => 'Wednesday',
    '4' => 'Thursday',
    '5' => 'Friday',
    '6' => 'Saturday',
    '7' => 'Sunday',
    'MONDAY' => Carbon::MONDAY,
    'TUESDAY' => Carbon::TUESDAY,
    'WEDNESDAY' => Carbon::WEDNESDAY,
    'THURSDAY' => Carbon::THURSDAY,
    'FRIDAY' => Carbon::FRIDAY,
    'SATURDAY' => Carbon::SATURDAY,
    'SUNDAY' => Carbon::SUNDAY
];