$(document).ready(function()
{
    loadLogs()
    
    $("#refresh-logs").click(function()
    {
        $(".activity-list-container").empty()
        $(".activity-list-container").append("<div id='loader''></div>");

        loadLogs()
    })

    function loadLogs()
    {
        var lessonReservationId = $("input#lessonReservationId").val();
        var url = window.location.origin
        $.ajax({
            url: `${url}/api/lesson-reservation/${lessonReservationId}?include=schoolReservationSlot,lessonConditionLogs`,
            method: "get",
            success: function(result) {
                var content = '';

                result.lesson_condition_logs.forEach(activity => {
                    content += `
                        <div class="col-12 row activity-container">
                            <div class='col-2'>
                                <span>${activity.id}</span>
                            </div>
                            <div class='col-2'>
                                <span>佐藤　よしこ</span>
                            </div>
                            <div class='col-4'>
                                <span>日常英会話　レベル3</span>
                            </div>
                            <div class='col-2'>
                                <div class="badge bg-light text-dark">
                                    <span>${activity.action_datetime}</span>
                                </div>
                            </div>
                            <div class='col-2'>
                                <button class="btn btn-primary btn-radius">
                                    <span>未入室</span>
                                </button>
                            </div>
                        </div>
                    `;
                })

                $(".activity-list-container").empty()
                $(".activity-list-container").append(content);
        }})
    }
})