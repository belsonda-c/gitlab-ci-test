function errorMessage(className, msg, state) {
    $('.'+className).text(msg);
    if(state == 1) {
        $('.'+className+'-req').removeClass('hide-window');
        $('.'+className).removeClass('hide-window');
    } else {
        $('.'+className+'-req').addClass('hide-window');
        $('.'+className).addClass('hide-window');
    }
}


$(document).ready(function() {
    $('#btn-confirm').click(function() {
        $('.win-one').addClass('hide-window');
        $('.win-two').removeClass('hide-window');

        $('#breadcrumbs-one').removeClass('btn-primary');
        $('#breadcrumbs-one').removeClass('pointer');
        $('#breadcrumbs-one').addClass('btn-outline-primary');
        $('#breadcrumbs-one').addClass('pointer-standby');

        $('#breadcrumbs-two').removeClass('btn-outline-primary');
        $('#breadcrumbs-two').removeClass('pointer-standby');
        $('#breadcrumbs-two').addClass('btn-primary');
        $('#breadcrumbs-two').addClass('pointer');

        var varSurname      = $('#fld-surname').val();
        var varName      = $('#fld-name').val();
        var varKanaSurname   = $('#fld-kanaSurname').val();
        var varKanaName      = $('#fld-kanaName').val();
        var varNickname      = $('#fld-nickname').val();
        var varCourse       = $('#fld-course').val();
        var varSchoolYear   = $('#fld-schoolYear').val();
        var varClass       = $('#fld-class').val();
        var varAttendanceNumber  = $('#fld-attendanceNumber').val();

        $('#txt-surname').text(varSurname);
        $('#txt-name').text(varName);
        $('#txt-kanaSurname').text(varKanaSurname);
        $('#txt-kanaName').text(varKanaName);
        $('#txt-nickname').text(varNickname);
        $('#txt-course').text(varCourse);
        $('#txt-schoolYear').text(varSchoolYear );
        $('#txt-class').text(varClass);
        $('#txt-attendanceNumber').text(varAttendanceNumber);
    
    });

    $('#btn-return').click(function() {
        $('.win-one').removeClass('hide-window');
        $('.win-two').addClass('hide-window');

        $('#breadcrumbs-one').addClass('btn-primary');
        $('#breadcrumbs-one').addClass('pointer');
        $('#breadcrumbs-one').removeClass('btn-outline-primary');
        $('#breadcrumbs-one').removeClass('pointer-standby');

        $('#breadcrumbs-two').addClass('btn-outline-primary');
        $('#breadcrumbs-two').addClass('pointer-standby');
        $('#breadcrumbs-two').removeClass('btn-primary');
        $('#breadcrumbs-two').removeClass('pointer');
    });

    $('#btn-save').click(function() {
        var _token      = $('meta[name="csrf-token"]').attr('content');
        var sid             = $('#fld-id').val();
        var saveSurname      = $('#fld-surname').val();
        var saveName      = $('#fld-name').val();
        var saveKanaSurname   = $('#fld-kanaSurname').val();
        var saveKanaName      = $('#fld-kanaName').val();
        var saveNickname      = $('#fld-nickname').val();
        var saveCourse       = $('#fld-course').val();
        var saveSchoolYear   = $('#fld-schoolYear').val();
        var saveClass       = $('#fld-class').val();
        var saveAttendanceNumber  = $('#fld-attendanceNumber').val();

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/student/updated/'+sid,
            data: {
                id: sid,
                surname: saveSurname,
                name: saveName,
                kanaSurname: saveKanaSurname,
                kanaName: saveKanaName,
                nickname: saveNickname,
                course: saveCourse,
                schoolYear: saveSchoolYear,
                class: saveClass,
                attendanceNumber: saveAttendanceNumber
            }, success: function (response) {
                $('.win-one').addClass('hide-window');
                $('.win-two').addClass('hide-window');
                $('.win-three').removeClass('hide-window');
        
                $('#breadcrumbs-two').addClass('btn-outline-primary');
                $('#breadcrumbs-two').addClass('pointer-standby');
                $('#breadcrumbs-two').removeClass('btn-primary');
                $('#breadcrumbs-two').removeClass('pointer');
        
                $('#breadcrumbs-three').removeClass('btn-outline-primary');
                $('#breadcrumbs-three').removeClass('pointer-last');
                $('#breadcrumbs-three').addClass('btn-primary');
                $('#breadcrumbs-three').addClass('pointer');
            },
            error: function(jqXHR) {
                var fldArr = {};
                var errorsMsg = jqXHR.responseJSON.errors;
                $.each(errorsMsg, function (key, msg) {
                    fldArr[key] = msg[0];
                });
                if(fldArr.hasOwnProperty("surname")) {
                    errorMessage('err-surname', fldArr.surname, 1);
                } else {
                    errorMessage('err-surname', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("name")) {
                    errorMessage('err-name', fldArr.name, 1);
                } else {
                    errorMessage('err-name', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("kanaSurname")) {
                    errorMessage('err-kanaSurname', fldArr.kanaSurname, 1);
                } else {
                    errorMessage('err-kanaSurname', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("kanaName")) {
                    errorMessage('err-kanaName', fldArr.kanaName, 1);
                } else {
                    errorMessage('err-kanaName', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("nickname")) {
                    errorMessage('err-nickname', fldArr.nickname, 1);
                } else {
                    errorMessage('err-nickname', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("course")) {
                    errorMessage('err-course', fldArr.course, 1);
                } else {
                    errorMessage('err-course', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("schoolYear")) {
                    errorMessage('err-schoolYear', fldArr.schoolYear, 1);
                } else {
                    errorMessage('err-schoolYear', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("class")) {
                    errorMessage('err-class', fldArr.class, 1);
                } else {
                    errorMessage('err-class', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("attendanceNumber")) {
                    errorMessage('err-attendanceNumber', fldArr.attendanceNumber, 1);
                } else {
                    errorMessage('err-attendanceNumber', 'valid', 0);
                }

               
            }
        });
    });   
});