function errorMessage(className, msg, state) {
    $('.'+className).text(msg);
    if(state == 1) {
        $('.'+className+'-req').removeClass('hide-window');
        $('.'+className).removeClass('hide-window');
    } else {
        $('.'+className+'-req').addClass('hide-window');
        $('.'+className).addClass('hide-window');
    }
}

$(document).ready(function() {
    var _token = $('meta[name="csrf-token"]').attr('content');

    $("#fld-res-course").change(function() {
        var resCourse   = $('#fld-res-course').val();
        var resYear     = $('#fld-res-year').val();
        var resClass    = $('#fld-res-class').val();

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/reservation/api/checkClass',
            data: {
                course: resCourse,
                year: resYear,
                class: resClass,
            },
            success: function (response) {
                if(response.class == 0) {
                    alert('利用可能なクラス');
                } else {
                    $("#fld-res-class-list").val(response.class);
                    $.each(response.studentList, function(key, element) {
                        $('#fld-res-candidate').append('<option value="'+element.student_id+'">'+element.student_id+'</option>');
                    });
                }
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    });

    $("#fld-res-year").change(function() {
        var resCourse   = $('#fld-res-course').val();
        var resYear     = $('#fld-res-year').val();
        var resClass    = $('#fld-res-class').val();

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/reservation/api/checkClass',
            data: {
                course: resCourse,
                year: resYear,
                class: resClass,
            },
            success: function (response) {
                if(response.class == 0) {
                    alert('利用可能なクラス');
                } else {
                    $("#fld-res-class-list").val(response.class);
                    $.each(response.studentList, function(key, element) {
                        $('#fld-res-candidate').append('<option value="'+element.student_id+'">'+element.student_id+'</option>');
                    });
                }
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    });

    $("#fld-res-class").change(function() {
        var resCourse   = $('#fld-res-course').val();
        var resYear     = $('#fld-res-year').val();
        var resClass    = $('#fld-res-class').val();

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/reservation/api/checkClass',
            data: {
                course: resCourse,
                year: resYear,
                class: resClass,
            },
            success: function (response) {
                if(response.class == 0) {
                    alert('利用可能なクラス');
                } else {
                    $("#fld-res-class-list").val(response.class);
                    $.each(response.studentList, function(key, element) {
                        $('#fld-res-candidate').append('<option value="'+element.student_id+'">'+element.student_id+'</option>');
                    });
                }
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    });

    $('#btn-win-two').click(function() {
        var participantCount    = $("#fld-res-parti").val();
        var studentCount        = $("#fld-res-student option").length;

        if(participantCount == studentCount) {
            $('.win-one').addClass('hide-window');
            $('.win-two').removeClass('hide-window');

            $('#breadcrumbs-one').removeClass('btn-primary');
            $('#breadcrumbs-one').removeClass('pointer');
            $('#breadcrumbs-one').addClass('btn-outline-primary');
            $('#breadcrumbs-one').addClass('pointer-standby');

            $('#breadcrumbs-two').removeClass('btn-outline-primary');
            $('#breadcrumbs-two').removeClass('pointer-standby');
            $('#breadcrumbs-two').addClass('btn-primary');
            $('#breadcrumbs-two').addClass('pointer');

            var resDate         = $('#fld-res-date').val();
            var resTime         = $('#fld-res-time').val();
            var resType         = $('#fld-res-type').val();
            var resDeadline     = $('#fld-res-deadline').val();
            var resCourse       = $('#fld-res-course').val();
            var resYear         = $('#fld-res-year').val();
            var resClass        = $('#fld-res-class').val();
            var resParti        = $('#fld-res-parti').val();
            var resCandidate    = $('#fld-res-candidate').val();
            var resStudent      = $('#fld-res-student').val();
            var resPeriod       = $('#fld-res-period').val();
            var resCat          = $('#fld-res-cat').val();
            var resUnit         = $('#fld-res-unit').val();
            var resMemo         = $('#fld-res-memo').val();

            $('#txt-res-date').text(resDate);
            $('#txt-res-time').text(resTime);
            $('#txt-res-type').text(resType);
            $('#txt-res-deadline').text(resDeadline);
            $('#txt-res-course').text(resCourse);
            $('#txt-res-year').text(resYear);
            $('#txt-res-class').text(resClass);
            $('#txt-res-parti').text(resParti);
            $('#txt-res-candidate').text(resCandidate);
            $('#txt-res-student').text(resStudent);
            $('#txt-res-period').text(resPeriod);
            $('#txt-res-cat').text(resCat);
            $('#txt-res-unit').text(resUnit);
            $('#txt-res-memo').text(resMemo);
        } else {
            alert('Participant / Student List Did not match!');
        }
    });

    $('#btn-win-one').click(function() {
        $('.win-one').removeClass('hide-window');
        $('.win-two').addClass('hide-window');

        $('#breadcrumbs-one').addClass('btn-primary');
        $('#breadcrumbs-one').addClass('pointer');
        $('#breadcrumbs-one').removeClass('btn-outline-primary');
        $('#breadcrumbs-one').removeClass('pointer-standby');

        $('#breadcrumbs-two').addClass('btn-outline-primary');
        $('#breadcrumbs-two').addClass('pointer-standby');
        $('#breadcrumbs-two').removeClass('btn-primary');
        $('#breadcrumbs-two').removeClass('pointer');
    });

    $('#btn-win-three').click(function() {
        var resSlot         = $('#reservation-slot-id').val();
        var resDate         = $('#fld-res-date').val();
        var resTime         = $('#fld-res-time').val();
        var resType         = $('#fld-res-type').val();
        var resDeadline     = $('#fld-res-deadline').val();
        var resClass        = $('#fld-res-class-list').val();
        var resParti        = $('#fld-res-parti').val();
        var resPeriod       = $('#fld-res-period').val();
        var resCat          = $('#fld-res-cat').val();
        var resUnit         = $('#fld-res-unit').val();
        var resMemo         = $('#fld-res-memo').val();
        var studentCount    = $("#fld-res-student option").length;
        
        var studList = [];
        $('#fld-res-student').each(function() {
            studList.push($(this).val());
        });

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/reservation/reserve',
            data: {
                resSlot: resSlot,
                resDate: resDate,
                resTime: resTime,
                resType: resType,
                resDeadline: resDeadline,
                resPart: resParti,
                resDuration: resPeriod,
                resStudent: studList,
                resClass: resClass,
                resTutor: 888,
                resMaterial: resCat,
                resMemo: resMemo,
                resStatus: 4
            },
            success: function (data, textStatus, xhr) {
                var respCode    = xhr.status;

                $('.win-one').addClass('hide-window');
                $('.win-two').addClass('hide-window');
                $('.win-three').removeClass('hide-window');
        
                $('#breadcrumbs-two').addClass('btn-outline-primary');
                $('#breadcrumbs-two').addClass('pointer-standby');
                $('#breadcrumbs-two').removeClass('btn-primary');
                $('#breadcrumbs-two').removeClass('pointer');
        
                $('#breadcrumbs-three').removeClass('btn-outline-primary');
                $('#breadcrumbs-three').addClass('last-tab');
                
                if(respCode == 201) {
                    if(studentCount <=1) {
                        $('.single-reg').removeClass('hide-window');
                    } else {
                        $('.multiple-reg').removeClass('hide-window');

                        $.each(data.data.Denied , function(index, val) {
                            $('#list-failed').append('<li>'+val+'</li>');
                        });

                        $.each(data.data.Allowed , function(index, val) {
                            $('#list-booked').append('<li>'+val+'</li>');
                        });
                    }
                } else {
                    $('.failed-reg').removeClass('hide-window');
                }
            },
            error: function(jqXHR) {
                var fldArr = {};
                var errorsMsg = jqXHR.responseJSON.errors;

                if(jqXHR.status == 500) {
                    $('.win-one').addClass('hide-window');
                    $('.win-two').addClass('hide-window');
                    $('.win-three').removeClass('hide-window');
            
                    $('#breadcrumbs-two').addClass('btn-outline-primary');
                    $('#breadcrumbs-two').addClass('pointer-standby');
                    $('#breadcrumbs-two').removeClass('btn-primary');
                    $('#breadcrumbs-two').removeClass('pointer');
            
                    $('#breadcrumbs-three').removeClass('btn-outline-primary');
                    $('#breadcrumbs-three').addClass('last-tab');

                    $('.failed-reg').removeClass('hide-window');
                } else {
                    $.each(errorsMsg, function (key, msg) {
                        fldArr[key] = msg[0];
                    });
    
                    if(fldArr.hasOwnProperty("resDate")) {
                        errorMessage('err-res-date', fldArr.resDate, 1);
                    } else {
                        errorMessage('err-res-date', 'valid', 0);
                    }
    
                    if(fldArr.hasOwnProperty("resTime")) {
                        errorMessage('err-res-time', fldArr.resTime, 1);
                    } else {
                        errorMessage('err-res-time', 'valid', 0);
                    }
    
                    if(fldArr.hasOwnProperty("resPart")) {
                        errorMessage('err-res-parti', fldArr.resPart, 1);
                    } else {
                        errorMessage('err-res-parti', 'valid', 0);
                    }
                }
            }
        });
    });   
});