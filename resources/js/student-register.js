function errorMessage(className, msg, state) {
    $('.'+className).text(msg);
    if(state == 1) {
        $('.'+className+'-req').removeClass('hide-window');
        $('.'+className).removeClass('hide-window');
    } else {
        $('.'+className+'-req').addClass('hide-window');
        $('.'+className).addClass('hide-window');
    }
}

$(document).ready(function() {
    $('#btn-win-two').click(function() {
        $('.win-one').addClass('hide-window');
        $('.win-two').removeClass('hide-window');

        $('#breadcrumbs-one').removeClass('btn-primary');
        $('#breadcrumbs-one').removeClass('pointer');
        $('#breadcrumbs-one').addClass('btn-outline-primary');
        $('#breadcrumbs-one').addClass('pointer-standby');

        $('#breadcrumbs-two').removeClass('btn-outline-primary');
        $('#breadcrumbs-two').removeClass('pointer-standby');
        $('#breadcrumbs-two').addClass('btn-primary');
        $('#breadcrumbs-two').addClass('pointer');

        var course  = $('#fld-course').val();
        var schoolYear  = $('#fld-year').val();
        var schoolClass = $('#fld-class').val();
        var addtl   = $('#fld-addtl').val();

        $('#txt-course').text(course);
        $('#txt-year').text(schoolYear);
        $('#txt-class').text(schoolClass);
        $('#txt-addtl').text(addtl);
    });

    $('#btn-win-one').click(function() {
        $('.win-one').removeClass('hide-window');
        $('.win-two').addClass('hide-window');

        $('#breadcrumbs-one').addClass('btn-primary');
        $('#breadcrumbs-one').addClass('pointer');
        $('#breadcrumbs-one').removeClass('btn-outline-primary');
        $('#breadcrumbs-one').removeClass('pointer-standby');

        $('#breadcrumbs-two').addClass('btn-outline-primary');
        $('#breadcrumbs-two').addClass('pointer-standby');
        $('#breadcrumbs-two').removeClass('btn-primary');
        $('#breadcrumbs-two').removeClass('pointer');
    });

    $('#btn-win-three').click(function() {
        var _token  = $('meta[name="csrf-token"]').attr('content');
        var course  = $('#fld-course').val();
        var schoolYear   = $('#fld-year').val();
        var schoolClass  = $('#fld-class').val();
        var addtl   = $('#fld-addtl').val();

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/student/register',
            data: {
                course: course,
                schoolYear: schoolYear,
                schoolClass: schoolClass,
                addtl: addtl
            },
            success: function (response) {
                $('.win-one').addClass('hide-window');
                $('.win-two').addClass('hide-window');
                $('.win-three').removeClass('hide-window');
        
                $('#breadcrumbs-two').addClass('btn-outline-primary');
                $('#breadcrumbs-two').addClass('pointer-standby');
                $('#breadcrumbs-two').removeClass('btn-primary');
                $('#breadcrumbs-two').removeClass('pointer');
        
                $('#breadcrumbs-three').removeClass('btn-outline-primary');
                $('#breadcrumbs-three').addClass('last-tab');
            },
            error: function(jqXHR) {
                var fldArr = {};
                var errorsMsg = jqXHR.responseJSON.errors;
                $.each(errorsMsg, function (key, msg) {
                    fldArr[key] = msg[0];
                });

                if(fldArr.hasOwnProperty("course")) {
                    errorMessage('err-course', fldArr.course, 1);
                } else {
                    errorMessage('err-course', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("schoolYear")) {
                    errorMessage('err-year', fldArr.schoolYear, 1);
                } else {
                    errorMessage('err-year', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("schoolClass")) {
                    errorMessage('err-class', fldArr.schoolClass, 1);
                } else {
                    errorMessage('err-class', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("addtl")) { 
                    errorMessage('err-addtl', fldArr.addtl, 1);
                } else {
                    errorMessage('err-addtl', 'valid', 0);
                }
            }
        });
    });   
});