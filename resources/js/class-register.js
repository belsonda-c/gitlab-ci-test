function errorMessage(className, msg, state) {
    $('.'+className).text(msg);
    if(state == 1) {
        $('.'+className+'-req').removeClass('hide-window');
        $('.'+className).removeClass('hide-window');
    } else {
        $('.'+className+'-req').addClass('hide-window');
        $('.'+className).addClass('hide-window');
    }
}


$(document).ready(function() {
    $('#btn-confirm').click(function() {
        $('.win-one').addClass('hide-window');
        $('.win-two').removeClass('hide-window');

        $('#breadcrumbs-one').removeClass('btn-primary');
        $('#breadcrumbs-one').removeClass('pointer');
        $('#breadcrumbs-one').addClass('btn-outline-primary');
        $('#breadcrumbs-one').addClass('pointer-standby');

        $('#breadcrumbs-two').removeClass('btn-outline-primary');
        $('#breadcrumbs-two').removeClass('pointer-standby');
        $('#breadcrumbs-two').addClass('btn-primary');
        $('#breadcrumbs-two').addClass('pointer');

        var varCourse       = $('#fld-course').val();
        var varSchoolYear   = $('#fld-schoolYear').val();
        var varClass       = $('#fld-class').val();
        var varStudentCount  = $('#fld-studentCount').val();

        $('#txt-course').text(varCourse);
        $('#txt-schoolYear').text(varSchoolYear );
        $('#txt-class').text(varClass);
        $('#txt-studentCount').text(varStudentCount);
    
    });

    $('#btn-return').click(function() {
        $('.win-one').removeClass('hide-window');
        $('.win-two').addClass('hide-window');

        $('#breadcrumbs-one').addClass('btn-primary');
        $('#breadcrumbs-one').addClass('pointer');
        $('#breadcrumbs-one').removeClass('btn-outline-primary');
        $('#breadcrumbs-one').removeClass('pointer-standby');

        $('#breadcrumbs-two').addClass('btn-outline-primary');
        $('#breadcrumbs-two').addClass('pointer-standby');
        $('#breadcrumbs-two').removeClass('btn-primary');
        $('#breadcrumbs-two').removeClass('pointer');
    });

    $('#btn-save').click(function() {
        var _token      = $('meta[name="csrf-token"]').attr('content');
        var saveCourse      = $('#fld-course').val();
        var saveSchoolYear     = $('#fld-schoolYear').val();
        var saveClass        = $('#fld-class').val();
        var saveStudentCount       = $('#fld-studentCount').val();
 
        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/class/register',
            data: {
                course: saveCourse,
                schoolYear: saveSchoolYear,
                class: saveClass,
                studentCount: saveStudentCount
            }, success: function (response) {
                $('.win-one').addClass('hide-window');
                $('.win-two').addClass('hide-window');
                $('.win-three').removeClass('hide-window');
        
                $('#breadcrumbs-two').addClass('btn-outline-primary');
                $('#breadcrumbs-two').addClass('pointer-standby');
                $('#breadcrumbs-two').removeClass('btn-primary');
                $('#breadcrumbs-two').removeClass('pointer');
        
                $('#breadcrumbs-three').removeClass('btn-outline-primary');
                $('#breadcrumbs-three').removeClass('pointer-last');
                $('#breadcrumbs-three').addClass('btn-primary');
                $('#breadcrumbs-three').addClass('pointer');
            },
            error: function(jqXHR) {
                var fldArr = {};
                var errorsMsg = jqXHR.responseJSON.errors;
                $.each(errorsMsg, function (key, msg) {
                    fldArr[key] = msg[0];
                });

                if(fldArr.hasOwnProperty("course")) {
                    errorMessage('err-course', fldArr.course, 1);
                } else {
                    errorMessage('err-course', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("schoolYear")) {
                    errorMessage('err-schoolYear', fldArr.schoolYear, 1);
                } else {
                    errorMessage('err-schoolYear', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("class")) {
                    errorMessage('err-class', fldArr.class, 1);
                } else {
                    errorMessage('err-class', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("studentCount")) {
                    errorMessage('err-studentCount', fldArr.studentCount, 1);
                } else {
                    errorMessage('err-studentCount', 'valid', 0);
                }

               
            }
        });
    });   
});