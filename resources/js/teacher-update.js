function errorMessage(className, msg, state) {
    $('.'+className).text(msg);
    if(state == 1) {
        $('.'+className+'-req').removeClass('hide-window');
        $('.'+className).removeClass('hide-window');
    } else {
        $('.'+className+'-req').addClass('hide-window');
        $('.'+className).addClass('hide-window');
    }
}

$(document).ready(function() {
    $('#btn-win-two').click(function(){
        $('.win-one').addClass('hide-window');
        $('.win-two').removeClass('hide-window');

        $('#breadcrumbs-one').removeClass('btn-primary');
        $('#breadcrumbs-one').removeClass('pointer');
        $('#breadcrumbs-one').addClass('btn-outline-primary');
        $('#breadcrumbs-one').addClass('pointer-standby');

        $('#breadcrumbs-two').removeClass('btn-outline-primary');
        $('#breadcrumbs-two').removeClass('pointer-standby');
        $('#breadcrumbs-two').addClass('btn-primary');
        $('#breadcrumbs-two').addClass('pointer');

        var surname     = $('#fld-surname').val();
        var name        = $('#fld-name').val();
        var surnameKana = $('#fld-surname-kana').val();
        var nameKana    = $('#fld-name-kana').val();
        var email       = $('#fld-email').val();
        var authority   = $('#fld-authority').val();

        $('#txt-surname').text(surname);
        $('#txt-name').text(name);
        $('#txt-surname-kana').text(surnameKana);
        $('#txt-name-kana').text(nameKana);
        $('#txt-email').text(email);
        $('#txt-authority').text(authority);
    });

    $('#btn-win-one').click(function(){
        $('.win-one').removeClass('hide-window');
        $('.win-two').addClass('hide-window');

        $('#breadcrumbs-one').addClass('btn-primary');
        $('#breadcrumbs-one').addClass('pointer');
        $('#breadcrumbs-one').removeClass('btn-outline-primary');
        $('#breadcrumbs-one').removeClass('pointer-standby');

        $('#breadcrumbs-two').addClass('btn-outline-primary');
        $('#breadcrumbs-two').addClass('pointer-standby');
        $('#breadcrumbs-two').removeClass('btn-primary');
        $('#breadcrumbs-two').removeClass('pointer');
    });

    $('#btn-win-three').click(function(){
        var _token      = $('meta[name="csrf-token"]').attr('content');
        var tid         = $("#fld-id").val();
        var surname     = $('#fld-surname').val();
        var name        = $('#fld-name').val();
        var surnameKana = $('#fld-surname-kana').val();
        var nameKana    = $('#fld-name-kana').val();
        var email       = $('#fld-email').val();
        var authority   = $('#fld-authority').val();

        $.ajax({ 
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/teacher/update/'+tid,
            data: {
                id: tid,
                surname: surname,
                name: name,
                surnameKana: surnameKana,
                nameKana: nameKana,
                email: email,
                authority: authority
            },
            success: function (response) {
                $('.win-one').addClass('hide-window');
                $('.win-two').addClass('hide-window');
                $('.win-three').removeClass('hide-window');
        
                $('#breadcrumbs-two').addClass('btn-outline-primary');
                $('#breadcrumbs-two').addClass('pointer-standby');
                $('#breadcrumbs-two').removeClass('btn-primary');
                $('#breadcrumbs-two').removeClass('pointer');
        
                $('#breadcrumbs-three').removeClass('btn-outline-primary');
                $('#breadcrumbs-three').addClass('last-tab');
            },
            error: function(jqXHR) {
                var fldArr = {};
                var errorsMsg = jqXHR.responseJSON.errors;
                $.each(errorsMsg, function (key, msg) {
                    fldArr[key] = msg[0];
                });

                if(fldArr.hasOwnProperty("surname")) {
                    errorMessage('err-surname', fldArr.surname, 1);
                } else {
                    errorMessage('err-surname', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("name")) {
                    errorMessage('err-name', fldArr.name, 1);
                } else {
                    errorMessage('err-name', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("surnameKana")) {
                    errorMessage('err-surname-kana', fldArr.surnameKana, 1);
                } else {
                    errorMessage('err-surname-kana', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("nameKana")) {
                    errorMessage('err-name-kana', fldArr.nameKana, 1);
                } else {
                    errorMessage('err-name-kana', 'valid', 0);
                }

                if(fldArr.hasOwnProperty("email")) {
                    errorMessage('err-email', fldArr.email, 1);
                } else {
                    errorMessage('err-email', 'valid', 0);
                }
            }
        });
    });   
});