function errorMessage(className, msg, state) {
    $('.'+className).text(msg);
    if(state == 1) {
        $('.'+className+'-req').removeClass('hide-window');
        $('.'+className).removeClass('hide-window');
    } else {
        $('.'+className+'-req').addClass('hide-window');
        $('.'+className).addClass('hide-window');
    }
}

$(document).ready(function() {
    $('#btn-win-two').click(function() {
        $('.win-one').addClass('hide-window');
        $('.win-two').removeClass('hide-window');

        $('#breadcrumbs-one').removeClass('btn-primary');
        $('#breadcrumbs-one').removeClass('pointer');
        $('#breadcrumbs-one').addClass('btn-outline-primary');
        $('#breadcrumbs-one').addClass('pointer-standby');

        $('#breadcrumbs-two').removeClass('btn-outline-primary');
        $('#breadcrumbs-two').removeClass('pointer-standby');
        $('#breadcrumbs-two').addClass('btn-primary');
        $('#breadcrumbs-two').addClass('pointer');

        var category = $('#fld-category').val();
        var unit = $('#fld-unit').val();

        $('#txt-material').text(category);
        $('#txt-unit').text(unit);
    });

    $('#btn-win-one').click(function() {
        $('.win-one').removeClass('hide-window');
        $('.win-two').addClass('hide-window');

        $('#breadcrumbs-one').addClass('btn-primary');
        $('#breadcrumbs-one').addClass('pointer');
        $('#breadcrumbs-one').removeClass('btn-outline-primary');
        $('#breadcrumbs-one').removeClass('pointer-standby');

        $('#breadcrumbs-two').addClass('btn-outline-primary');
        $('#breadcrumbs-two').addClass('pointer-standby');
        $('#breadcrumbs-two').removeClass('btn-primary');
        $('#breadcrumbs-two').removeClass('pointer');
    });

    $('#btn-win-three').click(function() {
        var _token      = $('meta[name="csrf-token"]').attr('content');
        var mid         = $('#fld-student').val();
        var category    = $('#fld-category').val();

        $.ajax({
            type: 'post',
            headers: {'X-CSRF-TOKEN': _token},
            url: '/material/update/'+mid,
            data: {
                studentID: mid,
                materialID: category
            },
            success: function (response) {
                $('.win-one').addClass('hide-window');
                $('.win-two').addClass('hide-window');
                $('.win-three').removeClass('hide-window');
        
                $('#breadcrumbs-two').addClass('btn-outline-primary');
                $('#breadcrumbs-two').addClass('pointer-standby');
                $('#breadcrumbs-two').removeClass('btn-primary');
                $('#breadcrumbs-two').removeClass('pointer');
        
                $('#breadcrumbs-three').removeClass('btn-outline-primary');
                $('#breadcrumbs-three').addClass('last-tab');
            },
            error: function(jqXHR) {
                var fldArr = {};
                var errorsMsg = jqXHR.responseJSON.errors;
                $.each(errorsMsg, function (key, msg) {
                    fldArr[key] = msg[0];
                });

                if(fldArr.hasOwnProperty("materialID")) {
                    errorMessage('err-category', fldArr.materialID, 1);
                } else {
                    errorMessage('err-category', 'valid', 0);
                }
            }
        });
    });   
});