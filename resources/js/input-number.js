function incrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal)) {
    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

function decrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal) && currentVal > 0) {
    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

$('.input-group').on('click', '.button-plus', function(e) {
  incrementValue(e);
});

$('.input-group').on('click', '.button-minus', function(e) {
  decrementValue(e);
});

var $input = $("#fld-res-parti");

$input.val(0);

$(".list-manage").click(function() {
    var sub = $("#fld-res-candidate :selected").length;
    var add = $("#fld-res-student :selected").length;
    if ($(this).hasClass('increase')) {
      $input.val(parseInt($input.val())+sub);
    } else if ($input.val()>=1) {
      $input.val(parseInt($input.val())-add);
    }

    if($(this).hasClass('stud-reset')) {
      $input.val(0);
    }
});