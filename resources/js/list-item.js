function moveItems(origin, dest) {
    $(origin).find(':selected').appendTo(dest);
}

function moveAllItems(origin, dest) {
    $(origin).children().appendTo(dest);
}

$('#left').click(function () {
    moveItems('#fld-res-student', '#fld-res-candidate');
});

$('#right').on('click', function () {
    moveItems('#fld-res-candidate', '#fld-res-student');
});

$('#leftall').on('click', function () {
    moveAllItems('#fld-res-student', '#fld-res-candidate');
});