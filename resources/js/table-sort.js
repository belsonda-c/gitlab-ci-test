let f_sl = 1;
let f_nm = 1;

function sort(tblID, f, n) {
    let rows = $('#'+tblID+' tbody  tr').get();

    rows.sort(function(a, b) {
        let A = getVal(a);
        let B = getVal(b);

        if(A < B) {
            return -1*f;
        }
        if(A > B) {
            return 1*f;
        }

        return 0;
    });

    function getVal(elm){
        let v = $(elm).children('td').eq(n).text().toUpperCase();

        if($.isNumeric(v)){
            v = parseFloat(v,10);
        }

        return v;
    }

    $.each(rows, function(index, row) {
        $('#'+tblID).children('tbody').append(row);
    });
}

function sortable(tblID) {
    $('#'+ tblID +' th').click(function(){
        f_sl *= -1
        let n = $(this).prevAll().length
        sort(tblID, f_sl ,n)
        if(f_sl === -1)
            $(this).text($(this).text().slice(0, -1) + " ▼")
        else
            $(this).text($(this).text().slice(0, -1) + " ▲")
    });
}