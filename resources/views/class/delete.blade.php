@extends('app')

@section('content')
<div style=' padding-right:171px; padding-left: ;' class="container-fluid">
    <div class="row justify-content-center text-start pt-5">
        <div class='col-lg-8'><span style="font-size: 34px; font-weight: 600;">登録クラス削除  </span></div>   
        <div class='col-lg'></div>
        <div class='col-lg'></div>
    </div>
    <div class="row justify-content-center text-start pt-0 pb-3">  
        <div class='col-lg-7'><span style="font-size: 27px; font-weight: 200;">  </span></div>
        <div class='col-lg-3'></div>
        <div class='col-lg-2'> </a></div>
    </div>
</div>

<div style=' padding-right:230px; padding-left: ; ' class="container-fluid">       
    <div style='border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
        <div class='text-center h2 pt-5'>選択したクラスを削除します </div>
        <div class='text-center h5 pt-3'>よろしければ削除ボタンを押してください </div>
        <div class="col-12">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 pt-3 pb-3">
                    <a href="/class/view/{{ $id }}" style='padding: 8px 54px;' class="btn btn-secondary float-end" role="button">戻る</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid pt-3 pb-3">
                    <form method="POST" action="/class/confirm/{{ $id }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" style='padding: 8px 49px;' class="btn btn-primary">削除する</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection