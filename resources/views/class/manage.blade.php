@extends('app')

@section('content')
<div style=' padding-right:10px; padding-left: ;' class="container-fluid">
    <div class="row justify-content-center text-start pt-5">
        <div class='col-lg-8';><span style="font-size: 34px; font-weight: 600;">クラス管理  </span></div>   
        <div class='col-lg';><a href='#'class='btn btn-outline-primary btn-inverse rounded-pill float-right' >翌年度用クラス複製</a></div>
        <div class='col-lg';><a href="{{route('/registration')}}" class='btn btn-primary rounded-pill float-right' >クラス新規登録</a></div>
    </div>
    @if (count($data) > 0)
    <div class="row justify-content-center text-start pt-0 pb-3">  
        <div class='col-lg-7';><span style="font-size: 27px; font-weight: 200;">対象クラス選択  </span></div>
        <div class='col-lg-3';></div>
        <div class='col-lg-2';> </a></div>
    </div>
</div>
<form method='get' action="{{route('/search')}}">
@csrf
      <div style=' padding-right:; padding-left: ;' class="container-fluid">    
      <div style='border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
      <div class="row justify-content-center text-start">
      <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
           <div style=' padding-top:31px;' class="col-lg-6 mb-3">
            <label style='padding-left:16px; padding-right:108px;'>年度</label>
             <select name='year_now' class="form-select" style=" width:270px; display:inline-block;">
             <option style='text-align: right;' value="1">2021</option>
             <option style='text-aign: right;' value="2">2020</option>
             <option style='text-align: right;' value="3">2019</option>
             <option style='text-align: right;' value="4">2018</option>
             <option style='text-align: right;' value="5">2017</option>
             <option style='text-align: right;' value="6">2016</option>
             </select>
           </div>          
           <div style=' padding-top:31px;' class="col-lg-6 mb-3">
             <label style='padding-left:16px; padding-right:108px;'>コース</label>
             <select name='course' class="form-select" style="width:270px; display:inline-block;">
             <option style='text-align: right;' value=""></option>
             </select>
           </div>
         </div>           
         <div class="row justify-content-center text-start">
            <div class="col-md-6 mb-3">
             <label style='padding-left:16px; padding-right:108px;'>学年</label>
             <select name='fiscal_year' class="form-select" style="width:270px; display:inline-block;">
             <option style='text-align: right;' value="1">2021</option>
             <option style='text-align: right;' value="2">2020</option>
             <option style='text-align: right;' value="3">2019</option>
             <option style='text-align: right;' value="4">2018</option>
             <option style='text-align: right;' value="5">2017</option>
             <option style='text-align: right;' value="6">2016</option>
             </select>
          </div>
          <div class="col-md-6 mb-3">
             <label style='padding-left:16px; padding-right:108px;'>クラス</label>
             <select name='group' class="form-select" style="width:270px; display:inline-block;">
             <option style='text-align: right;' value=""></option>
             </select>
            </div>
            </div>
            <div class="row justify-content-center text-start">
            <div style='margin-bottom:32px;margin-top:32px; text-align: center;' class="col-md-12 mb-6">
            <input  type='submit' style='padding:  12px 60px;' class='btn btn-primary rounded-pill float-right' value='検索' /> 
            </div>
            </div>
          </div>
             </form>
    <div class="row justify-content-center text-start pt-5">
        <div class='col-lg-8';><span style="font-size: 34px; font-weight: 600;">登録済クラス  </span></div>   
        <div class='col-lg';></div>
        <div class='col-lg';></div>
    </div> 
    <div style='border-radius: 15px; background: #FfFfFf; border: 1px solid #f8f8f8; padding: 20px; width: 100%; height: 55px;' class="row justify-content-center text-center pt-3">
       <div class='col-lg-2' style='display: inline-block;'><b>年度</b></div>
       <div class='col-lg-2' style='display: inline-block;'><b>コース</b></div>
       <div class='col-lg-2' style='display: inline-block;'><b>学年</b></div>
       <div class='col-lg-2' style='display: inline-block;'><b>クラス</b></div>
       <div class='col-lg-1' style='display: inline-block;'><b>人数</b></div>
       <div class='col-lg-3' style='display: inline-block;'></div>
    </div>
@if (isset($class_search))
     @foreach($class_search as $classlist)
    <div style='border-radius: 15px; background: #F4F4F4; border: 1px solid #f8f8f8; padding: 20px; width: 100%; height: 55px;' class="row justify-content-center text-center mb-3 pt-2">
       <div class='col-lg-2' style='display: inline-block;'>{{$classlist->mst_year_id}}</div>
       <div class='col-lg-2' style='display: inline-block;'>{{$classlist->mst_course_id}}</div>
       <div class='col-lg-2' style='display: inline-block;'>{{$classlist->fiscal_year}}</div>
       <div class='col-lg-2' style='display: inline-block;'>{{$classlist->mst_class_id}}</div>
       <div class='col-lg-1' style='display: inline-block;'>{{$classlist->student_count}}人</div>
       <div class='col-lg-3' style='display: inline-block;'>
         <a href="{{ route('/view', ['id' => $classlist->id]) }}" style='padding: 6px 30px;' class='btn btn-outline-primary rounded-pill float-right' > 用紙出力</a>
       </div>
    </div>
    @endforeach
@else
    @foreach($data as $classlist)
    <div style='border-radius: 15px; background: #F4F4F4; border: 1px solid #f8f8f8; padding: 20px; width: 100%; height: 55px;' class="row justify-content-center text-center mb-1 pt-2">
      <div class='col-lg-2' style='display: inline-block;'>{{$classlist->mst_year_id}}</div>
      <div class='col-lg-2' style='display: inline-block;'>{{$classlist->mst_course_id}}</div>
      <div class='col-lg-2' style='display: inline-block;'>{{$classlist->fiscal_year}}</div>
      <div class='col-lg-2' style='display: inline-block;'>{{$classlist->mst_class_id}}</div>
      <div class='col-lg-1' style='display: inline-block;'>{{$classlist->student_count}}人</div>
      <div class='col-lg-3' style='display: inline-block;'>
         <a href="/student/class-list/{{ $classlist->id }}" target="_blank" style='padding: 6px 30px;' class='btn btn-outline-primary rounded-pill float-right' > 用紙出力</a>
         <a href="{{ route('/view', ['id' => $classlist->id]) }}" ><i class="fa fa-chevron-right float-end arrow-pointer"  aria-hidden="true"></i></a>
     </div>
    </div>
  @endforeach
@endif
</div>
@else
<br>
<div class="row justify-content-center text-start pt-0 pb-3">  
      <div class='col-lg-7';><span style="font-size: 20px; font-weight: 200;">画面上部の「クラス新規登録」ボタンよりご利用されるクラスを登録ください。  </span></div>
      <div class='col-lg-3';></div>
      <div class='col-lg-2';> </a></div>
  </div>
</div>
@endif
@endsection