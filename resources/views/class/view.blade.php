@extends('app')

@section('content')
<div style=' padding-right:171px; padding-left: ;' class="container-fluid">

    <div class="row justify-content-center text-start pt-5">
        <div class='col-lg-8'><span style="font-size: 34px; font-weight: 600;">クラス詳細  </span></div>   
        <div class='col-lg'></div>
        <div class='col-lg'><a href="{{route('/registration')}}" class='btn btn-primary rounded-pill float-right' >クラス新規登録</a></div>
    </div>
    <div class="row justify-content-center text-start pt-0 pb-3">  
        <div class='col-lg-7'><span style="font-size: 27px; font-weight: 200;">  </span></div>
        <div class='col-lg-3'></div>
        <div class='col-lg-2'> </a></div>
    </div>
</div>

<div style=' padding-right:230px; padding-left: ; ' class="container-fluid">       
   <div style='border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
      <div class="row text-start ">
             <div style=' align-content: center; padding-top: 46px;' class='justify-content-center text-start';>
                  <a href="{{route('/registration')}}" style='margin-left: 16px; background: #162B44; border-radius:5px'class='btn btn-primary btn-md'>学年/クラス </a>&nbsp;
                  <a  style='display:inline; padding-left: 16px; font-size: 24px;  border-radius:5px'>2年1組</a>
                  <a style='display:inline; margin-left: 32px;' href='#'class='btn btn-outline-primary btn-inverse rounded-pill' >登録用紙出力</a>
                 <a style='display:inline; padding-right: 32px; font-size: 18px;  float:right;' href='#'class='' >登録用紙出力</a>
             </div>

            <div style=' align-content: center; padding-top: 30px;' class='justify-content-center text-start';> 
             <div style='margin:15px; height:143px; border-radius: 15px; background:#FfFfFf' class="jumbotron jumbotron-fluid border">
                  <div class='row'>
                      <div style='border-right: 1px solid;' class='col-lg-4 justify-content-center text-end ';>
                          <span style="font-size: 18px; font-weight: 200;"> コース </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">  人数 </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">  年度 </span><br>
                      </div>

                      <div class='col-lg-8'>
                          <span style="font-size: 18px; font-weight: 200;"> {{$viewClass->course}} </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;"> {{$viewClass->student_count}} </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;"> {{$viewClass->fiscal_year}} </span> <br>
                     </div>

                 </div>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-center text-center pt-4 ">
        <div class='col-lg-5'>
            <button onclick="window.location.href='{{route('/manage')}}'" style='display:inline; width:380px;' href='#'class='btn btn-lg btn-block btn-secondary ' >一覧へ戻る</button>
        </div> 
        <div class='col-lg-7'>
            <button onclick="window.location.href='{{route('/delete', ['id' => $viewClass->id]) }}'" style='display:inline;  width:380px' href='#'class='btn btn-block btn-lg btn-secondary ' >クラス情報削除</button>
        </div>   
</div>   


@endsection