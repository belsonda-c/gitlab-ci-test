
@extends('app')

@section('content')

<div style=' padding-right:171px; padding-left: ;' class="container-fluid">


    <div class="row justify-content-center text-start ">
        <div class='col-lg-8'><span style="font-size: 34px; font-weight: 600;">クラス管理  </span></div>   
        <div class='col-lg'></div>
        <div class='col-lg'></div>
    </div>
</div>


<div style=' padding-right:171px; padding-left: ;' class="container-fluid">

<div class="row px-4 py-4">
    <div class="btn-group">
        <a href="#" id="breadcrumbs-one"   class="btn btn-primary pointer                 noHover" style="z-index: 3;">情報入力</a>
        <a href="#" id="breadcrumbs-two"   class="btn btn-outline-primary pointer-standby noHover" style="z-index: 2;">内容確認</a>
        <a href="#" id="breadcrumbs-three" class="btn btn-outline-primary pointer-last    noHover" style="z-index: 1;">登録完了</a>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 py-4 round-corner win-one">
        <div class="mb-3">
            <label for="fld-course" class="form-label float-start">コース</label>
            <span class="hide-window err-course-req err-msg float-start">必須</span>
            <span class="hide-window err-course err-msg float-end"></span>
            <input type="text" class="form-control" id="fld-course" placeholder="スタンダード">
        </div>
        <div class="row align-items-center g-3">
            <div class="col">
                <label class="fld-schoolYear float-start" for="fld-schoolYear">学年</label>
                <span class="hide-window err-schoolYear-req err-msg float-start">必須</span>
                <span class="hide-window err-schoolYear err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-schoolYear" placeholder="学年">
            </div>
            <div class="col">
                <label class="fld-class" for="">クラス</label>
                <span class="hide-window err-class err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-class" placeholder="クラス">
            </div>
        </div>
        <div class="mb-3 pt-2">
            <label for="fld-studentCount" class="form-label float-start">人数</label>
            <span class="hide-window err-studentCount-req err-msg float-start">必須</span>
            <span class="hide-window err-studentCount err-msg float-end"></span>
            <input type="number" value='' class="form-control" id="fld-studentCount" placeholder="人数">
        </div>
        <div class="d-grid gap-2">
            <button id="btn-confirm" class="btn btn-primary" type="button">確認</button>
        </div>
    </div>
    <div class="col-12 py-4 round-corner win-two hide-window">
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">コース</label>
                <span class="hide-window err-course-req err-msg float-start">必須</span>
                <span class="hide-window err-course err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-course">コース</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label class="form-label float-start">学年</label>
                <span class="hide-window err-schoolYear-req err-msg float-start">必須</span>
                <span class="hide-window err-schoolYear err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6">
                <label class="form-label float-start">クラス</label>
                <span class="hide-window err-class-req err-msg float-start">必須</span>
                <span class="hide-window err-class err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6">
                <p id="txt-schoolYear">学年</p>
            </div>
            <div class="col-6">
            <p id="txt-class">クラス</p>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">人数</label>
                <span class="hide-window err-studentCount-req err-msg float-start">必須</span>
                <span class="hide-window err-studentCount err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-studentCount">人数</p>
            </div>
        </div>
        <div class="row">
            <div class="col d-grid gap-2"><button id="btn-return" class="btn btn-secondary" type="button">戻る</button></div>
            <div class="col d-grid gap-2"><button id="btn-save" class="btn btn-primary" type="button">登録する</button></div>
        </div>
    </div>
    <div class="col-12 py-4 round-corner win-three hide-window">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <h1>クラスの新規登録が完了しました</h1>
        </div>
        <div class="col-12 d-flex align-items-center justify-content-center">
            <a href="/class/manage" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
        </div>
    </div>
</div>

</div>

<script src="{{ asset('js/class-register.js') }}"></script>
     
@endsection