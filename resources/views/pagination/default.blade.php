@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li style=" padding-right:8px;" class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span style="
           width: 80px;
            height: 50px;
            border-radius: 25px;
            font-size: 13px;
            text-align: center;
            
" class="page-link" aria-hidden="true">< 前へ</span>
                </li>
            @else
                <li  style=" padding-right:8px;" class="page-item">
                    <a style="
           width: 80px;
            height: 50px;
            border-radius: 25px;
            font-size: 13px;
            text-align: center;
" class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">< 前へ</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li style=" padding-right:8px;" class="page-item disabled" aria-disabled="true"><span  class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li style=" padding-right:8px;" class="page-item active "  aria-current="page"><span style="
           width: 50px;
            height: 50px;
            border-radius: 25px;
            font-size: 10px;
            text-align: center;
" class="page-link">{{ $page }}</span></li>
                        @else
                            <li style=" padding-right:8px;" class="page-item"><a style="
          width: 50px;
            height: 50px;
            border-radius: 25px;
            font-size: 10px;
            text-align: center;
        " class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" style="
           width: 80px;
            height: 50px;
            border-radius: 25px;
            font-size: 13px;
            text-align: center;
            display: inline-block;
            
" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">次へ ></a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span style="
           width: 80px;
            height: 50px;
            border-radius: 25px;
            font-size: 13px;
            text-align: center;
" class="page-link" aria-hidden="true">次へ ></span>
                </li>
            @endif
        </ul>
    </nav>
@endif
