@extends('auth')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div style="border-radius: 6px; background-color: #ffffff; padding: 20px 50px 20px 50px; margin: 50px;">
            <div class="row justify-content-center text-center pt-4 pb-3">
                <span style="font-size: 34px; font-weight: 600;">教職員様ログイン画面</span>
            </div>
            <div class="row justify-content-center">
                <form method="POST" action="{{ url('loginProcess') }}">
                    @csrf
                    @if (\Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Success!</strong> {!! \Session::get('success') !!}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if (\Session::has('error'))
                        <div class="alert alert-danger" role="alert">
                            {!! \Session::get('error') !!}
                        </div>
                    @endif
                    <div class="form-group mb-3">
                        <label for="school_id" class="mb-2"><b>学校ID</b></label>
                        @if ($errors->has('school'))
                            <span class="err-msg float-end">{{ $errors->first('school') }}</span>
                        @endif
                        <input type="text" placeholder="" id="school_id" class="form-control" name="school_id">
                    </div>

                    <div class="form-group mb-3">
                        <label for="email" class="mb-2"><b>メールアドレス</b></label>
                        @if ($errors->has('email'))
                            <span class="err-msg float-end">{{ $errors->first('email') }}</span>
                        @endif
                        <input type="text" placeholder="mail@example.com" id="email" class="form-control" name="email" required autofocus>
                    </div>
        
                    <div class="form-group mb-3">
                        <label for="password" class="mb-2"><b>パスワード</b></label>
                        @if ($errors->has('password'))
                            <span class="err-msg float-end">{{ $errors->first('password') }}</span>
                        @endif
                        <input type="password" placeholder="Password" id="password" class="form-control" name="password" required>
                    </div>
        
                    <div class="d-grid mx-auto">
                        <button type="submit" class="btn btn-primary btn-block">ログイン</button>
                    </div>
                </form>
            </div>
            <div class="row justify-content-center text-center pt-2 pb-4">
                <a href="password/forget">パスワードを忘れた方</a>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
</div>
@endsection