@extends('auth')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div style="border-radius: 6px; background-color: #ffffff; padding: 20px 50px 20px 50px; margin: 50px;">
            <div class="row justify-content-center text-center pt-4 pb-3">
                <span style="font-size: 34px; font-weight: 600;">パスワード変更</span>
                <p>初期パスワードを変更してください</p>
            </div>
            <div class="row justify-content-center">
                <form method="POST" action="{{ url('password/reset') }}">
                    @csrf
                    @if(!empty($success))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Success!</strong> {{ $success }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if(!empty($error))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error!</strong> {{ $error }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <div class="form-group mb-3">
                        <label for="fld-new-pass" class="mb-2"><b>パスワード</b></label>
                        @if ($errors->has('password'))
                            <span class="err-email err-msg float-end">{{ $errors->first('password') }}</span>
                        @endif
                        <input type="hidden" id="fld-token" class="form-control" name="token" value="{{ $token }}">
                        <input type="password" placeholder="New Password" id="fld-new-pass" class="form-control" name="password" autofocus>
                    </div>
        
                    <div class="form-group mb-3">
                        <label for="password-confirmation" class="mb-2"><b>パスワード再入力</b></label>
                        @if ($errors->has('password_confirmation'))
                            <span class="err-email err-msg float-end">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                        <input type="password" placeholder="Confirm New Password" id="password-confirmation" class="form-control" name="password_confirmation">
                    </div>
        
                    <div class="d-grid mx-auto">
                        <button type="submit" class="btn btn-primary btn-block">変更</button>
                    </div>
                </form>
            </div>
            <div class="row justify-content-center text-center pt-2 pb-4">
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
</div>
@endsection