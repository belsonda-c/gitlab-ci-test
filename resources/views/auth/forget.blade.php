@extends('auth')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div style="border-radius: 6px; background-color: #ffffff; padding: 20px 50px 20px 50px; margin: 50px;">
            <div class="row justify-content-center text-center pt-4 pb-3">
                <span style="font-size: 34px; font-weight: 600;">パスワード再設定</span>
                <p>メールアドレスを入力してください</p>
            </div>
            <div class="row justify-content-center">
                <form method="POST" action="{{ url('password/reset/link') }}">
                    @csrf
                    @if (\Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            {!! \Session::get('success') !!}
                        </div>
                    @endif
                    <div class="form-group mb-3">
                        <label for="email" class="mb-2"><b>メールアドレス</b></label>
                        @if ($errors->has('email'))
                            <span class="err-email err-msg float-end">{{ $errors->first('email') }}</span>
                        @endif
                        <input type="text" placeholder="mail@example.com" id="email" class="form-control" name="email" autofocus>
                    </div>
                    <div class="d-grid mx-auto">
                        <button type="submit" class="btn btn-primary btn-block">送信</button>
                    </div>
                </form>
            </div>
            <div class="row justify-content-center text-center pt-2 pb-4">
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
</div>
@endsection