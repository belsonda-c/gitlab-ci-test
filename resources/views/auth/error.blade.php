@extends('auth')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
        <div style="border-radius: 6px; background-color: #ffffff; padding: 20px 50px 20px 50px; margin: 50px;">
            <div class="row justify-content-center">
                @if(!empty($error))
                    <p class="error-mgs">{{ $error }} <a href="/login">ここをクリック</a></p>
                @endif
            </div>
            <div class="row justify-content-center text-center pt-2 pb-4">
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3"></div>
</div>
@endsection