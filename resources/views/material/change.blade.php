@extends('app')

@section('content')
<div class="row px-4 my-4">
    <span class="heading-bold">教材変更</span>
    <div class="btn-group">
        <a href="#" id="breadcrumbs-one" class="btn btn-primary pointer noHover" style="z-index: 3;">情報入力</a>
        <a href="#" id="breadcrumbs-two" class="btn btn-outline-primary pointer-standby noHover" style="z-index: 2;">内容確認</a>
        <a href="#" id="breadcrumbs-three" class="btn btn-outline-primary pointer-last noHover" style="z-index: 1;">登録完了</a>
    </div>
</div>
<div class="row px-4">
    <div class="col-12 win-one">
        <div class="row round-corner py-3">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 my-3">
                <button type="button" class="btn btn-dark float-start">レッスン予約日時</button>
                <p class="heading-content float-start px-3">{{ $reservationDetails->reservation_slot_date }}（月） {{ $reservationDetails->start_time }} - {{ $reservationDetails->end_time }}</p>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 px-4">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3" style="background-color: #fff;">
                        <table style="font-size: 18px; font-weight: 400;">
                            <tbody>
                                <tr><td class="text-end">教材</td></tr>
                                <tr><td class="text-end">&nbsp;</td></tr>
                                <tr><td class="text-end">生徒名</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 col-xxl-9" style="background-color: #fff;">
                        <table style="font-size: 18px; font-weight: 400;">
                            <tbody>
                                <tr><td class="text-start">カテゴリ：日常英会話</td></tr>
                                <tr><td class="text-start">ユニット：レベル３</td></tr>
                                <tr><td class="text-start">伊椅子　太郎</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row round-corner mt-4">
            <div class="mb-3 pt-2">
                <label class="fld-category float-start" for="fld-year">カテゴリ</label>
                <span class="hide-window err-category-req err-msg float-start">必須</span>
                <span class="hide-window err-category err-msg float-end"></span>
                <select class="form-control" id="fld-category">
                    @forelse($textBooks as $textBook)
                        <option value="{{ $textBook->id }}">{{ $textBook->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            <div class="mb-3 pt-2">
                <label class="fld-unit float-start" for="fld-year">ユニット</label>
                <span class="hide-window err-unit-req err-msg float-start">必須</span>
                <span class="hide-window err-unit err-msg float-end"></span>
                <select class="form-control" id="fld-unit">
                    <option value="レベル３">レベル３</option>
                </select>
            </div>
            <div class="d-grid gap-2 py-5">
                <button id="btn-win-two" class="btn btn-primary" type="button">確認</button>
            </div>
        </div>
    </div>

    <div class="col-12 py-4 round-corner win-two hide-window">
        <div class="row">
            <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
                <h1>選択した教材へ変更します</h1>
            </div>
        </div>
        <div class="row py-3 my-1">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <button type="button" class="btn btn-dark float-start">レッスン予約日時</button>
                <p class="heading-content float-start px-3">2021/12/06（月）　10:00 - 10:30</p>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 mt-2 px-4">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3" style="background-color: #fff;">
                        <table style="font-size: 18px; font-weight: 400;">
                            <tbody>
                                <tr><td class="text-end">教材</td></tr>
                                <tr><td class="text-end">&nbsp;</td></tr>
                                <tr><td class="text-end">生徒名</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 col-xxl-9" style="background-color: #fff;">
                        <table style="font-size: 18px; font-weight: 400;">
                            <tbody>
                                <tr><td class="text-start" id="txt-material">カテゴリ：日常英会話</td></tr>
                                <tr><td class="text-start" id="txt-unit">ユニット：レベル３</td></tr>
                                <tr><td class="text-start" id="txt-student">伊椅子　太郎</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
                <p>よろしければ変更ボタンを押してください</p>
            </div>
        </div>
        <div class="row">
            <div class="col d-grid gap-2"><button id="btn-win-one" class="btn btn-secondary" type="button">戻る</button></div>
            <div class="col d-grid gap-2"><button id="btn-win-three" class="btn btn-primary" type="button">変更する</button></div>
        </div>
    </div>

    <div class="col-12 py-4 round-corner win-three hide-window">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <h1>教材の変更が完了しました</h1>
        </div>
        <div class="col-12 d-flex align-items-center justify-content-center">
            <a href="/teacher/list" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/material-change.js') }}"></script>
@endsection