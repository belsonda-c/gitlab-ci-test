<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Class List</title>
    <style>
        @font-face {
            font-family: Meiryo;
            font-weight: normal;
            font-weight: 400; /* fallback */
            font-style: normal;
            src: url("{{ storage_path('fonts/Meiryo.ttf') }}") format("truetype");
        }
        @font-face {
            font-family: Meiryo;
            font-weight: bold;
            font-weight: 700; /* fallback */
            font-style: normal;
            src: url("{{ storage_path('fonts/Meiryo-Bold.ttf') }}") format("truetype");
        }
        @page {
            margin: 30px 45px 25px;
        }

        body {
            font-family: Meiryo, sans-serif;
            max-width: 700px;
            min-height: 900px;
            font-size: 14px;
        }

        span {
            display: block;
            line-height: 0.95;
        }
        header {
            text-align: center;
            font-size: 30px;
            color: #5bbaf6;
            line-height: 0.95;
            font-weight: bold;
        }

        header img {
            width: 350px;
        }

        footer img {
            width: 150px;
        }
        footer {
            text-align: center;
            margin-top: 55px;
        }

        .container {
            text-align: center;
            padding: 10px 5px;
        }

        .content {
            margin: 15px auto 5px auto;
            padding-top: 8px;
            text-align: left;
            max-width: 630px;
            height: 780px;
        }
        
        .qr-code{
            width: 150px; 
            height: 150px;
            margin: 0;
        }

        .scan-me{
            margin:0;
            margin-left: 45px;
        }

    </style>
</head>
<body>
    <div class="container">
        {{-- TODO: investigate if view can be converted to components, almost same view with student pdf details --}}
        @php
            $i = 0;
            $count = 0;
    
            if ($students){
                if ($students instanceof Illuminate\Support\Collection) {
                    $count = $students->count();
                }
            }
        @endphp
        
    
            @if ($count > 0)
            @foreach ($students as $student) 
                @php($i++)
    
                <header>
                    <img src="{{ public_path('images/header/logo1.png') }}">
                </header>
    
                <div class="content">
    
                <h3>学生の詳細</h3>
    
                <hr>
            
                <div style="clear:both; position:relative;">
                    <div style="position:absolute; left:0pt; width:192pt;">
                        <p>学生証: {{ $student->id }}</p>
                        <p>ファーストネーム: {{ $student->firstname }}</p> 
                        <p>苗字: {{ $student->lastname }}</p> 
                        <p>かな名: {{ $student->kana_firstname }}</p>
                        <p>かな姓: {{ $student->kana_lastname }}</p>
                        <p>コース: {{ $student->course }}</p>
                        <p>年:{{ $student->year_id }}</p>
                    </div>
                    <div style="margin-left:200pt;">
                        <img class="qr-code" src="{{ public_path('images/student-qrcode.png') }}" alt="qr code">
                        <p class="scan-me">スキャンミー</p>
                    </div>
                </div>
                
                </div>
                <footer>
                    <img src="{{ public_path('images/logo_footer.png') }}">
                </footer>
    
                @if ($i < $count)
                    <div style="page-break-before: always;"></div>
                @endif
            @endforeach
        @endif
    </div>
    
    </body>
    </html>