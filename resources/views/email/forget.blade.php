<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Password Reset Link</title>
</head>
<body>
    <p>Please click <a href="http://localhost:8080/password/reset/{{ $details['token'] }}">here</a> to reset your password.</p>
</body>
</html>