<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>学校管理者</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">


</head>
<body style="background-color: #ffffff;">

<nav class="navbar navbar-expand-lg mb-4" style="background: #f8f8f8;">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><img src="{{ asset('images/logo.png') }}" alt="" class="img-fluid"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mob-navbar" aria-label="Toggle">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="mob-navbar">
            <ul class="navbar-nav mb-2 mb-lg-0 mx-5">
                <li class="nav-item dropdown">
                    <a class="nav-link active dropdown-toggle" aria-current="page" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-user-o" aria-hidden="true"></i> {{ Auth::guard('teacher')->user()->last_name }} {{ Auth::guard('teacher')->user()->first_name }}</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/signout">Signout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid vh-100">
    <div class="row justify-content-center">
        <div class="col-2" style="height: 870px; background-color: #fff;">

            @include('includes.sidebar')
        </div>
        <div class="col-8" style="height: 870px; background-color: #fff;">
            @yield('content')
        </div>
    </div>
    <div class="row fixed-bottom" style="background: #f8f8f8;">
        <div class="container-fluid my-auto">
            <div class="row py-2 px-2">
                <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 pt-1">
                    <a href="#" class="float-start"><img src="{{ asset('images/logo_footer.png') }}" alt="" class="img-fluid"></a>
                    <a href="" class="float-start footer-nav mt-2 ms-2">株式会社エンビジョン</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 d-flex align-items-center justify-content-center">
                    <a href="" class="mt-1 footer-nav">利用規約</a>
                    <a href="" class="mt-1 ms-5 footer-nav">個人情報保護方針</a>
                    <a href="" class="mt-1 ms-5 footer-nav">よくあるご質問</a>
                    <a href="" class="mt-1 ms-5 footer-nav">お問い合わせ</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 pt-1">
                    <span class="footer-nav float-end">Copyright ©2020 Envizion, Inc. All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
@yield('script')
</body>
</html>