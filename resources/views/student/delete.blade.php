@extends('app')

@section('content')
<div style=' padding-right:171px; padding-left: ;' class="container-fluid">

    <div class="row justify-content-center text-start">
        <div class='col-lg-8'><span style="font-size: 34px; font-weight: 600;">生徒登録削除  </span></div>   
        <div class='col-lg'></div>
        <div class='col-lg'></div>
    </div>
    <div class="row justify-content-center text-start pt-0 pb-3">  
        <div class='col-lg-7'><span style="font-size: 27px; font-weight: 200;">  </span></div>
        <div class='col-lg-3'></div>
        <div class='col-lg-2'> </a></div>
    </div>


<div style=' padding-right:230px; padding-left: ; ' class="container-fluid">       
    <div style='border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
        <div class='text-center h2 pt-5'>選択した生徒の登録を削除します </div>
        
    <div style=' align-content: center; padding-top: 30px;' class='justify-content-center text-start';> 
            <div style='margin-right:50px; margin-left:50px;  height:320px; border-radius: 15px; background:#FfFfFf' class="jumbotron jumbotron-fluid border">
                  <div class='row pt-4'>
                      <div style='border-right: 1px solid;' class='col-lg-6 justify-content-center text-end ';>
                          <span style="font-size: 18px; font-weight: 200;"> コース </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">  学年 </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">  クラス </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;"> 出席番号 </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">  氏名 </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">  氏名カナ </span><br>
                      </div>
                      <div class='col-lg-6'>
                          <span style="font-size: 18px; font-weight: 200;">{{$viewStudent->course_id}} </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">{{$viewStudent->year_id}} </span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">{{$viewStudent->class_list_id}}</span> <br><br>
                          <span style="font-size: 18px; font-weight: 200;">{{$viewStudent->attendance_id}}</span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">{{$viewStudent->lastname}} {{$viewStudent->firstname}}</span><br><br>
                          <span style="font-size: 18px; font-weight: 200;">{{$viewStudent->kana_lastname}} {{$viewStudent->kana_firstname}}</span> <br>
                     </div>
            </div>
    </div>
        <div class="col-12">
            <div class="row">
                <div class='text-center h5 pt-2'>よろしければ削除ボタンを押してください </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 pt-3 pb-3">
                    <a href="" style='padding: 8px 54px;' class="btn btn-secondary float-end" role="button">戻る</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid pt-3 pb-3">
                    <form method="POST" action="/student/delete-confirm/{{$viewStudent->id}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" style='padding: 8px 49px;' class="btn btn-primary">削除する</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection