@extends('app')

@section('content')

<div style=' padding-right:171px; padding-left: ;' class="container-fluid">

    <div class="row justify-content-center text-start pt-3">
        <div class='col-lg-8'><span style="font-size: 34px; font-weight: 600;">生徒情報編集  </span></div>   
        <div class='col-lg'></div>
        <div class='col-lg'></div>
    </div>
</div>


<div style=' padding-right:171px; padding-left: ;' class="container-fluid">

<div class="row px-4 py-4">
    <div class="btn-group">
        <a href="#" id="breadcrumbs-one"   class="btn btn-primary pointer                 noHover" style="z-index: 3;">情報入力</a>
        <a href="#" id="breadcrumbs-two"   class="btn btn-outline-primary pointer-standby noHover" style="z-index: 2;">内容確認</a>
        <a href="#" id="breadcrumbs-three" class="btn btn-outline-primary pointer-last    noHover" style="z-index: 1;">登録完了</a>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 py-4 round-corner win-one">
        <div class="row align-items-center g-3">
            <div class="col">
                 <label class="fld-surname float-start" for="fld-surname">氏名</label>
                <span class="hide-window err-surname-req err-msg float-start">必須</span>
                <span class="hide-window err-surname err-msg float-end"></span>
                <input type="hidden" class="form-control" id="fld-id" value="{{ $editStudent->id }}">
                <input type="text" class="form-control" value="{{$editStudent->lastname}}" id="fld-surname" placeholder="">
            </div>
            <div class="col pt-4">             
                <span class="hide-window err-name err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-name" value="{{$editStudent->firstname}}" placeholder="">
            </div>
        </div>
        <div class="row align-items-center g-3">
            <div class="col">
                <label class="fld-kanaSurname float-start" for="fld-kanaSurname">氏名（カナ）</label>
                <span class="hide-window err-kanaSurname-req err-msg float-start">必須</span>
                <span class="hide-window err-kanaSurname err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-kanaSurname"  value="{{$editStudent->kana_lastname}}" placeholder="">
            </div>
            <div class="col pt-4">
                <span class="hide-window err-kanaName err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-kanaName" value="{{$editStudent->kana_firstname}}" placeholder="">
            </div>
        </div>
        <div class="mb-3 pt-2">
            <label for="fld-nickname" class="form-label float-nickname">ニックネーム</label>
            <span class="hide-window err-nickname-req err-msg float-start">必須</span>
            <span class="hide-window err-nickname err-msg float-end"></span>
            <input type="text" class="form-control" id="fld-nickname"  value="{{$editStudent->nickname}}" placeholder="">
        </div>
        <div class="mb-3 pt-2">
            <label for="fld-course" class="form-label float-course">コース</label>
            <span class="hide-window err-course-req err-msg float-start">必須</span>
            <span class="hide-window err-course err-msg float-end"></span>
            <select id="fld-course" class="form-control">
                @forelse($courses as $course)
                    <option value="{{ $course->id }}" {{ ($editStudent->course_id == $course->id) ? 'selected' : '' }}>{{ $course->name }}</option>
                @empty
                    <option value="">スタンダード</option>
                @endforelse
            </select>
        </div>
        <div class="row align-items-center g-3">
            <div class="col">
                <label class="fld-schoolYear float-start" for="fld-schoolYear">学年</label>
                <span class="hide-window err-schoolYear-req err-msg float-start">必須</span>
                <span class="hide-window err-schoolYear err-msg float-end"></span>
                <select class="form-control form-select" id="fld-schoolYear">
                    <option>1</option>
                    <option>2</option>
                </select>
            </div>
            <div class="col">
                <label class="fld-class" for="">クラス</label>
                <span class="hide-window err-class err-msg float-end"></span>
                <select class="form-control form-select" id="fld-class">
                    @forelse($classes as $class)
                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>
        <div class="mb-3 pt-2">
            <label for="fld-attendanceNumber" class="form-label float-attendanceNumber">出席番号</label>
            <span class="hide-window err-attendanceNumber-req err-msg float-start">必須</span>
            <span class="hide-window err-attendanceNumber err-msg float-end"></span>
            <input type="number" class="form-control" value="{{$editStudent->attendance_id}}" id="fld-attendanceNumber" placeholder="">
        </div>
        <div class="d-grid gap-2">
            <button id="btn-confirm" class="btn btn-primary" type="button">確認</button>
        </div>
    </div>
    <div class="col-12 py-4 round-corner win-two hide-window">
        <div class="row">
        <div class="row">
            <div class="col-6">
                <label class="form-label float-surname">氏名</label>
                <span class="hide-window err-surname-req err-msg float-start">必須</span>
                <span class="hide-window err-surname err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6 pt-4">
                <span class="hide-window err-name-req err-msg float-start">必須</span>
                <span class="hide-window err-name err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6">
                <p id="txt-surname">学年</p>
            </div>
            <div class="col-6">
            <p id="txt-name">クラス</p>
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label class="form-label float-start">氏名（カナ）</label>
                <span class="hide-window err-kanaSurname-req err-msg float-start">必須</span>
                <span class="hide-window err-KanaSurname err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6 pt-4">
                <span class="hide-window err-kanaName-req err-msg float-start">必須</span>
                <span class="hide-window err-kanaName err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6">
                <p id="txt-kanaSurname">学年</p>
            </div>
            <div class="col-6">
            <p id="txt-kanaName">クラス</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">ニックネーム</label>
                <span class="hide-window err-nickname-req err-msg float-start">必須</span>
                <span class="hide-window err-nickname err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-nickname">人数</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">コース</label>
                <span class="hide-window err-course-req err-msg float-start">必須</span>
                <span class="hide-window err-course err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-course">人数</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label class="form-label float-start">学年</label>
                <span class="hide-window err-schoolYear-req err-msg float-start">必須</span>
                <span class="hide-window err-schoolYear err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6">
                <label class="form-label float-start">クラス</label>
                <span class="hide-window err-class-req err-msg float-start">必須</span>
                <span class="hide-window err-class err-msg float-end"></span>
                <label class=""></label>
            </div>
            <div class="col-6">
                <p id="txt-schoolYear">学年</p>
            </div>
            <div class="col-6">
            <p id="txt-class">クラス</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">出席番号</label>
                <span class="hide-window err-attendanceNumber-req err-msg float-start">必須</span>
                <span class="hide-window err-attendanceNumber err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-attendanceNumber">人数</p>
            </div>
        </div>
        <div class="row">
            <div class="col d-grid gap-2"><button id="btn-return" class="btn btn-secondary" type="button">戻る</button></div>
            <div class="col d-grid gap-2"><button id="btn-save" class="btn btn-primary" type="button">登録する</button></div>
        </div>
    </div>
    <div class="col-12 py-4 round-corner win-three hide-window">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <h1>生徒情報の編集が完了しました</h1>
        </div>
        <div class="col-12 d-flex align-items-center justify-content-center">
            <a href="" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
        </div>
    </div>
</div>

</div>

<script src="{{ asset('js/student-update.js') }}"></script>
     
@endsection