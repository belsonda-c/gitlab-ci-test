@extends('app')

@section('content')
<div class="row px-4 py-4">
    <span class="heading-bold">生徒新規登録</span>
    <div class="btn-group">
        <a href="#" id="breadcrumbs-one"   class="btn btn-primary pointer                 noHover" role="button" style="z-index: 3;">情報入力</a>
        <a href="#" id="breadcrumbs-two"   class="btn btn-outline-primary pointer-standby noHover" role="button" style="z-index: 2;">内容確認</a>
        <a href="#" id="breadcrumbs-three" class="btn btn-outline-primary pointer-last    noHover" role="button" style="z-index: 1;">登録完了</a>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 py-4 round-corner win-one">
        <div class="mb-3">
            <label for="fld-course" class="form-label float-start">コース</label>
            <span class="hide-window err-course-req err-msg float-start">必須</span>
            <span class="hide-window err-course err-msg float-end"></span>
            <select class="form-control" id="fld-course">
                @forelse($courses as $course)
                    <option value="{{ $course->id }}">{{ $course->name }}</option>
                @empty
                    <option value="">スタンダード</option>
                @endforelse
            </select>
        </div>
        <div class="row align-items-center g-3">
            <div class="col">
                <label class="fld-year float-start" for="fld-year">学年</label>
                <span class="hide-window err-year-req err-msg float-start">必須</span>
                <span class="hide-window err-year err-msg float-end"></span>
                <select class="form-control" id="fld-year">
                    <option value="1">小学5年</option>
                    <option value="2">小学6年</option>
                    <option value="3">中学1年</option>
                    <option value="4">中学2年</option>
                    <option value="5">中学3年</option>
                    <option value="6">高校1年</option>
                    <option value="7">高校2年</option>
                    <option value="8">高校3年</option>
                    <option value="9">その他</option>
                </select>
            </div>
            <div class="col">
                <label class="fld-class" for="">クラス</label>
                <span class="hide-window err-class err-msg float-end"></span>
                <select class="form-control" id="fld-class">
                    @forelse($classes as $class)
                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>
        <div class="mb-3 pt-2">
            <label for="fld-addtl" class="form-label float-start">追加人数</label>
            <span class="hide-window err-addtl-req err-msg float-start">必須</span>
            <span class="hide-window err-addtl err-msg float-end"></span>
            <div class="input-group mb-3">
                <input type="text" name="quantity" id="fld-addtl" class="form-control quantity-field" value="1" step="1" min="1">
                <button class="input-group-text button-minus" data-field="quantity">-</button>
                <button class="input-group-text button-plus" data-field="quantity">+</button>
            </div>
        </div>
        <div class="d-grid gap-2">
            <button id="btn-win-two" class="btn btn-primary" type="button">確認</button>
        </div>
    </div>

    <div class="col-12 py-4 round-corner win-two hide-window">
        <div class="row">
            <div class="col-12">
                <label class="form-label">所属</label>
                <span class="hide-window err-course-req err-msg float-start">必須</span>
            </div>
            <div class="col-12">
                <p id="txt-course">スタンダード</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label class="form-label float-start">学年</label>
                <span class="hide-window err-year-req err-msg float-start">必須</span>
            </div>
            <div class="col-6">
                <label class="">クラス</label>
            </div>
            <div class="col-6">
                <p id="txt-year">2</p>
            </div>
            <div class="col-6">
            <p id="txt-class">4</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">追加人数</label>
                <span class="hide-window err-addtl-req err-msg float-start">必須</span>
            </div>
            <div class="col-12">
                <p id="txt-addtl">1</p>
            </div>
        </div>
        <div class="row">
            <div class="col d-grid gap-2"><button id="btn-win-one" class="btn btn-secondary" type="button">戻る</button></div>
            <div class="col d-grid gap-2"><button id="btn-win-three" class="btn btn-primary" type="button">登録する</button></div>
        </div>
    </div>

    <div class="col-12 py-4 round-corner win-three hide-window">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <h1>生徒の新規登録が完了しました</h1>
        </div>
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <p>３名分の生徒IDが発行されました</p>
        </div>
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <ul>
                <li>112233</li>
                <li>112234</li>
                <li>112235</li>
            </ul>
        </div>
        <div class="col-12 d-flex align-items-center justify-content-center">
            <a href="/teacher/list" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/student-register.js') }}"></script>
    <script src="{{ asset('js/input-number.js') }}"></script>
@endsection