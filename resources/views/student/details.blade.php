@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-2">
        <span class="heading-bold float-start">生徒詳細</span>
        <a href="/student/update/{{ $student->id }}" class="btn btn-primary btn-lg rounded-pill float-end">編集</a>
    </div>
</div>
<div class="row px-4"> 
    <div class="col-12 px-4 py-4 round-corner" style="background-color: #f8f8f8;">
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <button type="button" class="btn btn-dark float-start">出席番号</button>
                <p class="heading-content float-start px-3">{{ $student->attendance_id }}</p>
                <a href="/student/record" class="btn btn-outline-primary rounded-pill btn-bold-outlined float-start" role="button">登録用紙出力</a>
                <a href="/student/status" class="float-end">生徒再選択</a>
            </div>
        </div>
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3 px-4 py-4" style="background-color: #fff;">
                <table style="font-size: 18px; font-weight: 400;">
                    <tbody>
                        <tr><td class="text-end">氏名</td></tr>
                        <tr><td class="text-end">氏名カナ</td></tr>
                        <tr><td class="text-end">ニックネーム</td></tr>
                        <tr><td class="text-end">生徒ID</td></tr>
                        <tr><td class="text-end">コース</td></tr>
                        <tr><td class="text-end">学年/クラス</td></tr>
                        <tr><td class="text-end">通算受講回数</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 col-xxl-9 px-4 py-4" style="background-color: #fff;">
                <table style="font-size: 18px; font-weight: 400;">
                    <tbody>
                        <tr><td class="text-start">{{ $student->lastname }} {{ $student->firstname }}</td></tr>
                        <tr><td class="text-start">{{ $student->kana_lastname }} {{ $student->kana_firstname }}</td></tr>
                        <tr><td class="text-start">{{ $student->nickname }}</td></tr>
                        <tr><td class="text-start">{{ $student->id }}</td></tr>
                        <tr><td class="text-start">{{ $student->course_id }}</td></tr>
                        <tr><td class="text-start">{{ $student->year_id }} 年 {{ $student->class_list_id }} 組</td></tr>
                        <tr><td class="text-start">{{ $student->attendance_id }} 回</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
        <h3 class="heading-module">次回予約</h3>
    </div>
    <div class="col-12 round-corner" style="background-color: #f8f8f8;">
        <div class="row">
            <div class="col-8">
                <p class="dtls-bold">2021/11/31(水) 12:00 - 12:30 </p>
                <p class="dtls-subject">実用英会話 レベル3</p>
            </div>
            <div class="col-4">
                <a href="/material/change/{{ $student->id }}" class="btn btn-outline-primary rounded-pill float-end mt-2 btn-bold-outlined" role="button">教材変更</a>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
        <h3 class="heading-module">受講履歴</h3>
    </div>
    <div class="col-12 round-corner" style="background-color: #f8f8f8;">
        <div class="row">
            <div class="col-5">
                <p class="dtls-bold">2021/11/31(水) 12:00 - 12:30</p>
                <p class="dtls-subject">実用英会話 レベル3</p>
            </div>
            <div class="col-1">
                <div class="dtls-score-card">
                    <p class="score-card-label">Score</p>
                    <p class="score-card-grade">A</p>
                </div>
            </div>
            <div class="col-6">
                <a href="#" class="btn custom-btn-rounded float-end mx-3 my-3"><i class="fa fa-sign-in" aria-hidden="true"></i> レッスンページ</a>
                <a href="#" class="btn custom-btn-rounded float-end mx-3 my-3"><i class="fa fa-commenting-o" aria-hidden="true"></i> フィードバック</a>
            </div>
        </div>
    </div>
    <div class="row px-4 py-4">
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
            <a href="/student/list" class="btn btn-white-rounded" role="button">一覧へ戻る</a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
            <a href="/student/delete/{{ $student->id }}" class="btn btn-white-rounded" role="button">生徒情報削除</a>
        </div>
    </div>
</div>
@endsection