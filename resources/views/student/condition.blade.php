@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
        <h3 class="heading-bold">生徒管理</h3>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 pt-3">
        <a href="/student/registration" class="btn btn-primary rounded-pill float-end" style="padding: 5px 15px 5px 15px;" role="button"> 生徒新規登録</a>
    </div>
    <div class="col-12 my-3 py-2 round-corner" style="background-color: #f8f8f8;">
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <button type="button" class="btn btn-dark float-start">学年/クラス</button>
                <p class="heading-content float-start px-3">2年 1組</p>
                <a href="/student/generate-pdf-class-list/{{ $classId }}" role="button" class="custom-btn-rounded float-start mx-3">用紙一括出力</a>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
        <ul class="nav nav-tabs" id="myTab">
            <li class="nav-item">
                <a href="#status" class="nav-link active" data-bs-toggle="tab">登録状況</a>
            </li>
            <li class="nav-item">
                <a href="#history" class="nav-link" data-bs-toggle="tab">受講履歴</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="status">
                <table id="dashboard-table" class="table table-borderless">
                    <thead>
                        <tr>
                            <th>出席番号 </th>
                            <th>生徒ID </th>
                            <th>氏名 </th>
                            <th>ログイン日時 </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($registerCondition as $registration)
                            <tr style="background-color: #f4f4f4;">
                                <td>{{ $registration->attendance_id }}</td>
                                <td>{{ $registration->id }}</td>
                                <td>{{ $registration->lastname }} {{ $registration->firstname }}</td>
                                <td>{{ $registration->last_login_in }}</td>
                                <td><i class="fa fa-chevron-right float-end arrow-pointer" aria-hidden="true"></i> <a href="/student/record" role="button" class="btn custom-btn-rounded float-end mx-3">用紙出力</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">レコードが見つかりません</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="history">
            <table id="dashboard-table" class="table table-borderless">
                    <thead>
                        <tr>
                            <th>生徒情報 </th>
                            <th>前回受講 </th>
                            <th>次回予約 </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="background-color: #f4f4f4;">
                            <td>
                                123456 <br>
                                伊椅子　太郎 <br>
                                通算123回
                            </td>
                            <td>
                                <div class="row mx-2 py-2" style="background-color: #ffffff; height: 100px;">
                                    <div class="col">
                                        2021/10/31(水) <br>
                                        12:00 - 12:30 <br>
                                        <button type="button" class="btn btn-outline-dark btn-sm">実施済み</button>
                                    </div>
                                    <div class="col">
                                        <div class="dtls-score-card">
                                            <p class="score-card-label">Score</p>
                                            <p class="score-card-grade">A</p>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i> レッスンページ</a> <br>
                                        <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>フィードバック</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="row mx-2 py-2" style="background-color: #ffffff; height: 100px;">
                                    <div class="col">
                                        2021/10/31(水) <br>
                                        12:00 - 12:30
                                    </div>
                                </div>
                            </td>
                            <td class="d-flex justify-content-center">
                                <a href="/student/details/1" class=""><i class="fa fa-chevron-right float-end arrow-pointer" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>

<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
        <a href="/student/status" role="button" class="btn btn-white-rounded">一覧へ戻る</a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
        <a href="#" role="button" class="btn btn-outline-primary rounded-pill float-end mt-2 btn-bold-outlined" role="button">受講履歴出力</a>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/table-sort.js') }}"></script>
<script>
$(document).ready(function() {
    sortable("dashboard-table");  
});
</script>
@endsection