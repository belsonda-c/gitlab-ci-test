@extends('app')

@push('styles')
<link href="{{ asset('/css/fullcalendar.css') }}" rel="stylesheet">
<link href="{{ asset('/css/fullcalendar-custom.css') }}" rel="stylesheet">
@endpush

<style>
   a.disabled {
        pointer-events: none; 
   }
   
</style>


@section('content')
<div class="row px-4">
    <div class="col-12 py-2 pe-5">
        <span class="heading-bold float-start">予約管理</span>
        <a href="/lesson/reservation" class="btn btn-primary btn-md rounded-pill float-end px-5">再読み込み</a>
        <a href="/reservation/list" class="btn btn-outline-primary btn-md rounded-pill float-end px-5 me-2">予約一覧表示</a>    
    </div>
</div>

<div class="row px-4 mt-4">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
        <div id='calendar'></div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 mt-3 px-3">
        <div class="float-end"> 
            <span class="dot dot-holidays"></span> <label class="dot-label">予約外・休校日</label>
            <span class="dot dot-reservable"></span> <label class="dot-label">予約可能枠</label>
            <span class="dot dot-reserved"></span> <label class="dot-label px-1">予約済枠</label> 
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" id="reservationModal" aria-labelledby="reservationModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header"> 
                <h5 class="modal-title">選択した時間帯の詳細</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="modalForm" action="#">
                <div class="modal-body">
                    <div class="row mt-2">
                        <p class="m-0">日付</p>
                        <p id="date" class="m-0">2021/09/29（水）</p>
                    </div>

                    <div class="row mt-4">
                        <div class="col-3">
                            <p class="m-0">時間</p>
                            <p id="time" class="m-0">14:00 -</p>
                        </div>
                        
                        <div class="col-6 mt-2">
                            <select id="minutesSelect" class="form-select" aria-label="Default select example">
                                <option value="" selected>分を選択してください</option>
                                <option value="1">15分</option>
                                <option value="2">25分</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-3">
                            <p class="m-0">状況</p>
                            <p id="vacant" class="m-0">空き41件</p>
                        </div>
                        
                        <div class="col-9 mt-4">
                            <p id="people" class="m-0">130人</p>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 text-center">
                        <a id="saveReservation" href="/reservation/1/booking/2022-01-07 14:00" class="btn btn-primary btn-md rounded-pill w-75 px-5 disabled">予約登録</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src='https://cdn.jsdelivr.net/npm/moment@2.27.0/min/moment.min.js'></script>
<script src="{{ asset('js/fullcalendar/main.js') }}"></script>
<script src="{{ asset('js/fullcalendar/locale/ja.js') }}"></script>
<script src='https://cdn.jsdelivr.net/npm/@fullcalendar/moment@5.5.0/main.global.min.js'></script>
<script src="{{ asset('js/toasts.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        var selectedStartTime = "";
        var startTime, endTime;
        var schoolReservationSlotId, reservationDatetime, slotCount;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            locale: 'ja',
            initialDate: moment(moment(moment(), "YYYY-MM-DD").add(14, 'days')).format('YYYY-MM-DD'),
            initialView: 'timeGridWeek',
            nowIndicator: true,
            timeZone: 'local',
            height: 720,
            headerToolbar: {
                left: 'prev',
                center: 'title',
                right: 'next',
                // right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            navLinks: true, // can click day/week names to navigate views
            allDaySlot: false,
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEvents: true, // allow "more" link when too many events
            slotMinTime: "08:30:00",
            slotMaxTime: "18:00:00",
            slotLabelInterval: "00:10:00",
            slotDuration: "00:10:00",
            slotLabelFormat: ["HH:mm"],
            scrollTime:"12:00:00",
            dayHeaderFormat: function(date) {
                let day = moment(date.date).format('ddd');
                return {
                    html: `<span style="font-weight:normal;">${day}</span>
                         <p style="margin-top:5px;margin:4px 0 0 0;font-size:16px;">${date.start.day}</p>
                        `
                }
            },
            eventColor: '#66c6cc',
            events: function(info, successCallback, failureCallback) {
                $.ajax({
                    url:`/reservation/slots`,
                    dataType: "json",
                    type:"GET",
                    success:function(response)
                    {
                        let events = [];
                        let numberOfPeople = parseInt($('#txtSearch').val());

                         $.map( response.school_reservation_slots, function(schoolReservationSlots) {

                            let color = '#66c6cc';
                            let title = `空き${schoolReservationSlots.slot_count}件`;


                            let day = moment(schoolReservationSlots.start_time).format('ddd')

                            if (schoolReservationSlots.slot_count <= 0 || schoolReservationSlots.slot_count < numberOfPeople){
                                color = '#cccccc';
                            }

                            // if (day == "reserved"){
                            //     color = '#fb9c57';
                            // }

                            events.push({
                                id: schoolReservationSlots.id,
                                title: title,
                                start: `${schoolReservationSlots.reservation_slot_date} ${schoolReservationSlots.start_time}`,
                                end: `${schoolReservationSlots.reservation_slot_date} ${schoolReservationSlots.end_time}`,
                                color: color,
                                slots: schoolReservationSlots.slot_count
                            });
                        });
                        
                        successCallback(events);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        $.snack('error','続行できません。もう一度やり直してください。', 5000);
                    }
                });
            },

            eventClick: function(info) {

                schoolReservationSlotId = info.event.id; 
                reservationDatetime = `${moment(info.event.start).format('YYYY-MM-DD HH:mm')}`;
                slotCount = info.event.extendedProps.slots;

                $("p#date").text(`${moment(info.event.start).format('YYYY/MM/DD')} （水）`); 
                $("p#time").text(`${moment(info.event.start).format('HH:mm')} -`); 
                $('p#vacant').text(`空き${info.event.extendedProps.slots}件`);
                $('p#people').text(`${30 - info.event.extendedProps.slots}人`);

                $("#modalForm")[0].reset();
                $('#saveReservation').addClass('disabled');
                $('#reservationModal').modal("show");
                
            }
        });

        calendar.render();


        $('#minutesSelect').change(function() {

            if (slotCount == 0){
                $.snack('error','別の時間帯を選択してください', 5000);
                return;
            }

            $("#saveReservation").removeClass("disabled");

            $("#saveReservation").attr("href", `/reservation/booking/${schoolReservationSlotId}/${reservationDatetime}`)
        });


        $(".fc-header-toolbar").append( `
            <div class="btn-toolbar ms-5" role="toolbar" aria-label="Toolbar">
                <div class="btn-group" role="group" aria-label="Header Buttons">
                    <button type="button" class="btn btn-primary tb-btn" style="width:100px;">
                        <i class="fa fa-user-friends"></i> 人数
                    </button>
                    <div style="background-color:#f8f8f8;">
                        <input id="txtSearch" type="text" class="input-search">
                    </div>
                    <button id="btnMinus" type="button" class="btn btn-primary tb-btn">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button id="btnPlus" type="button" class="btn btn-primary tb-btn">
                        <i class="fa fa-plus"></i>
                    </button>
                    <button id="btnSearch" type="button" class="btn btn-primary" onclick="">検索</button>
                </div>
            </div>
        ` );

        $("#btnMinus").click(function() {
            let numberOfPeople = parseInt($('#txtSearch').val());

            if (!numberOfPeople){
                return;
            }

            $('#txtSearch').val(numberOfPeople - 1);
        });

        $("#btnPlus").click(function() {
            let numberOfPeople = parseInt($('#txtSearch').val());

            if (!numberOfPeople){
                numberOfPeople = 0;
            }
            
            $('#txtSearch').val(numberOfPeople + 1);
        });

        $("#btnSearch").click(function() {
            calendar.refetchEvents();
        });

    });
</script>
@endsection