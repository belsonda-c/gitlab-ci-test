@extends('app')

@section('content')
<div id="class-implementation" class="row px-4">
    <div class="col-10 py-2">
        <span class="heading-bold float-start">レッスン実施状況</span>
    </div>
    <div class="col-2 refresh-container">
        <button id="refresh-logs" class="btn btn-primary btn-radius">
            <i class="bi bi-arrow-clockwise" aria-hidden="true"></i>
            <span>更新</span>
        </button>
    </div>
    <div class="col-12 class-info">
        <div class="subj">
            <span>学年/クラス</span>
        </div>
        <div class="year">
            <span>{{ $year }}</span>
        </div>
        <div class="sched">
            {{ $schedule }}
        </div>
    </div>

    <div class="col-12 activities-container">
        <div class="col-12 activity-header row">
            <div class='col-2'>
                <span>出席番号</span>
            </div>
            <div class='col-2'>
                <span>氏名</span>
            </div>
            <div class='col-4'>
                <span>教材</span>
            </div>
            <div class='col-2'>
                <span>講師入室状況</span>
            </div>
            <div class='col-2'>
                <span>生徒入室状況</span>
            </div>
        </div>
        <div class="col-12 activity-list-container">
            <div id="loader"></div>
            {{-- @if(count($activities) > 0)
                @forelse($lessonConditionLogs as $lessonConditionLog)
                    <div class="col-12 row activity-container">
                        <div class='col-2'>
                            <span>123456</span>
                        </div>
                        <div class='col-2'>
                            <span>佐藤　よしこ</span>
                        </div>
                        <div class='col-4'>
                            <span>日常英会話　レベル3</span>
                        </div>
                        <div class='col-2'>
                            <div class="badge bg-light text-dark">
                                <span>11:59:30</span>
                            </div>
                        </div>
                        <div class='col-2'>
                            <button class="btn btn-primary btn-radius">
                                <span>未入室</span>
                            </button>
                        </div>
                    </div>
                @empty
                    <p> No Data </p>
                @endforelse
            @endif --}}
        </div>
    </div>
</div>
<input type="hidden" id="lessonReservationId" value="{{ $lessonReservationId }}"/>
@endsection

@section('script')
<script src="{{ asset('js/lesson-monitoring.js') }}">
</script>
@endsection