@extends('app')

@section('content')
<div class="row px-4 py-4">
    <span class="heading-bold">教師登録を更新する</span>
    <div class="btn-group">
        <a href="#" id="breadcrumbs-one"   class="btn btn-primary pointer                 noHover" style="z-index: 3;">情報入力</a>
        <a href="#" id="breadcrumbs-two"   class="btn btn-outline-primary pointer-standby noHover" style="z-index: 2;">内容確認</a>
        <a href="#" id="breadcrumbs-three" class="btn btn-outline-primary pointer-last    noHover" style="z-index: 1;">登録完了</a>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 py-4 round-corner win-one">
        <div class="row align-items-center g-3">
            <div class="col">
                <label class="fld-surname float-start" for="fld-surname">氏名</label>
                <span class="hide-window err-surname-req err-msg float-start">必須</span>
                <span class="hide-window err-surname err-msg float-end"></span>
                <input type="hidden" class="form-control" id="fld-id" value="{{ $teacherData->id }}">
                <input type="text" class="form-control" id="fld-surname" placeholder="姓" value="{{ $teacherData->last_name }}">
            </div>
            <div class="col">
                <label class="fld-name" for=""></label>
                <span class="hide-window err-name err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-name" placeholder="名" value="{{ $teacherData->first_name }}">
            </div>
        </div>
        <div class="row align-items-center g-3">
            <div class="col">
                <label class="fld-surname float-start" for="fld-surname">氏名（カナ）</label>
                <span class="hide-window err-surname-kana-req err-msg float-start">必須</span>
                <span class="hide-window err-surname-kana err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-surname-kana" placeholder="姓" value="{{ $teacherData->last_name_kana }}">
            </div>
            <div class="col">
                <label class="fld-name" for=""></label>
                <span class="hide-window err-name-kana err-msg float-end"></span>
                <input type="text" class="form-control" id="fld-name-kana" placeholder="名" value="{{ $teacherData->first_name_kana }}">
            </div>
        </div>
        <div class="mb-3 pt-2">
            <label for="fld-email" class="form-label float-start">メールアドレス</label>
            <span class="hide-window err-email-req err-msg float-start">必須</span>
            <span class="hide-window err-email err-msg float-end"></span>
            <input type="email" class="form-control" id="fld-email" placeholder="mail@example.com" value="{{ $teacherData->email }}">
        </div>
        <div class="mb-3">
            <label for="fld-authority" class="form-label float-start">権限</label>
            <span class="hide-window err-auth-req err-msg float-start">必須</span>
            <span class="hide-window err-auth err-msg float-end"></span>
            <select name="" class="form-control" id="fld-authority">
                <option value="1" {{ ($teacherData->role == 1) ? 'selected' : '' }}>管理者</option>
                <option value="2" {{ ($teacherData->role == 2) ? 'selected' : '' }}>教職員</option>
                <option value="3" {{ ($teacherData->role == 3) ? 'selected' : '' }}>アシスタント</option>
            </select>
        </div>
        <div class="d-grid gap-2">
            <button id="btn-win-two" class="btn btn-primary" type="button">確認</button>
        </div>
    </div>
    <div class="col-12 py-4 round-corner win-two hide-window">
        <div class="row">
            <div class="col-6">
                <label class="form-label float-start">氏名</label>
                <span class="hide-window err-surname-req err-msg float-start">必須</span>
            </div>
            <div class="col-6">
                <label class=""></label>
            </div>
            <div class="col-6">
                <p id="txt-surname">姓</p>
            </div>
            <div class="col-6">
            <p id="txt-name">名</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <label class="form-label float-start">氏名（カナ）</label>
                <span class="hide-window err-surname-kana-req err-msg float-start">必須</span>
            </div>
            <div class="col-6">
                <label class=""></label>
            </div>
            <div class="col-6">
                <p id="txt-surname-kana">セイ</p>
            </div>
            <div class="col-6">
            <p id="txt-name-kana">メイ</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">メールアドレス</label>
                <span class="hide-window err-email-req err-msg float-start">必須</span>
            </div>
            <div class="col-12">
                <p id="txt-email">mail@example.com</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="form-label float-start">権限</label>
                <span class="hide-window err-auth-req err-msg float-start">必須</span>
            </div>
            <div class="col-12">
                <p id="txt-authority">管理者</p>
            </div>
        </div>
        <div class="row">
            <div class="col d-grid gap-2"><button id="btn-win-one" class="btn btn-secondary" type="button">前の画面へ戻る</button></div>
            <div class="col d-grid gap-2"><button id="btn-win-three" class="btn btn-primary" type="button">アップデート</button></div>
        </div>
    </div>
    <div class="col-12 py-4 round-corner win-three hide-window">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <h1>教師の更新登録が完了しました</h1>
        </div>
        <div class="col-12 d-flex align-items-center justify-content-center">
            <a href="/teacher/list" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/teacher-update.js') }}"></script>
@endsection