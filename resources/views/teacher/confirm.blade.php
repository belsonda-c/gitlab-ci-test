@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-2">
        <span class="heading-bold float-start">登録先生削除</span>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 px-4 py-4 round-corner" style="background-color: #f8f8f8;">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <p class="heading-bold">選択した先生の登録を削除します</p>
        </div>
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <p class="heading-content float-start px-3">よろしければ削除ボタンを押してください</p>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6">
                    <a href="/teacher/get/{{ $tid }}" class="btn btn-secondary float-end" role="button">前の画面へ戻る</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
                    <form method="POST" action="/teacher/delete/{{ $tid }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-primary">削除する</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection