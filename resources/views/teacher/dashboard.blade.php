@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-1">
        <span class="heading-bold">お知らせ</span>
    </div>
    <div class="col-12 py-2 round-corner" style="background-color: #f8f8f8;">
        <p class="heading-content">{{ $newDtls }}</p>
    </div>
    <div class="col-12 py-1">
        <span class="heading-bold">利用状況確認</span>
        <h3 class="heading-module">対象クラスを選択</h3>
    </div>
    <div class="col-12 px-4 py-4 round-corner" style="background-color: #f8f8f8;">
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 col-xxl-1"><label for="" class="form-label">年度</label></div>
            <div class="col-sm-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                <select name="" id="" class="form-control">
                    <option value="">2021</option>
                </select>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 col-xxl-1"><label for="" class="form-label">コース</label></div>
            <div class="col-sm-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                <select name="" id="" class="form-control">
                    <option value="">スタンダード</option>
                </select>
            </div>
        </div>
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 col-xxl-1"><label for="" class="form-label">学年</label></div>
            <div class="col-sm-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                <select name="" id="" class="form-control">
                    <option value="1">小学5年</option>
                    <option value="2">小学6年</option>
                    <option value="3">中学1年</option>
                    <option value="4">中学2年</option>
                    <option value="5">中学3年</option>
                    <option value="6">高校1年</option>
                    <option value="7">高校2年</option>
                    <option value="8">高校3年</option>
                    <option value="9">その他</option>
                </select>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-1 col-xl-1 col-xxl-1"><label for="" class="form-label">クラス</label></div>
            <div class="col-sm-12 col-md-12 col-lg-5 col-xl-5 col-xxl-5">
                <select name="" id="" class="form-control">
                    <option value="">4</option>
                </select>
            </div>
        </div>
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 d-flex align-items-center justify-content-center">
                <button type="button" class="btn btn-primary rounded-pill" style="padding: 5px 15px 5px 15px;"><i class="fa fa-search"></i> 検索</button>
            </div>
        </div>
    </div>
</div>

<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
        <h3 class="heading-module">登録済クラス</h3>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8 pt-3">
        <a href="/class/registration" class="btn btn-primary rounded-pill float-end" style="padding: 5px 15px 5px 15px;" role="button"> クラス新規登録</a>
        <button type="button" class="btn btn-outline-primary rounded-pill float-end mx-1" style="padding: 5px 15px 5px 15px;"> 翌年度用クラス複製</button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
        <p class="heading-content hide-window">「クラス新規登録」ボタンよりご利用されるクラスを登録ください。</p>
        <table id="dashboard-table" class="table table-borderless">
            <thead>
                <tr>
                    <th>年度 </th>
                    <th>学年 </th>
                    <th>クラス </th>
                    <th>コース </th>
                    <th>人数 </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($classList as $class)
                    <tr style="background-color: #f4f4f4;">
                    <td>{{ $class->fiscal_year }}年度</td>
                    <td>{{ $class->fiscal_year }} 年</td>
                    <td>{{ $class->group }} 組</td>
                    <td>{{ $class->course->name }}</td>
                    <td>{{ $class->student_count }}</td>
                    <td><i class="fa fa-chevron-right float-end arrow-pointer" aria-hidden="true"></i> <button type="button" class="custom-btn-rounded float-end mx-3">用紙出力</button></td>
                </tr>
                @empty
                    <p class="mt-4" style="font-size: 18px;">「クラス新規登録」ボタンよりご利用されるクラスを登録ください。</p>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
        <button type="button" class="btn-white-rounded">利用マニュアル</button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
        <button type="button" class="btn-white-rounded">お問い合わせ</button>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/table-sort.js') }}"></script>
<script>
$(document).ready(function() {
    sortable("dashboard-table");  
});
</script>
@endsection