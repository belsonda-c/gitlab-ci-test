@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-2">
        <span class="heading-bold float-start">先生詳細</span>
        <a href="/teacher/edit/{{ $teacher->id }}" class="btn btn-primary btn-lg rounded-pill float-end">編集</a>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 px-4 py-4 round-corner" style="background-color: #f8f8f8;">
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <button type="button" class="btn btn-dark float-start">氏名</button>
                <p class="heading-content float-start px-3">{{ $teacher->last_name }} {{ $teacher->first_name }}</p>
                <a href="#" class="float-end">先生再選択</a>
            </div>
        </div>
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3 px-4 py-4" style="background-color: #fff;">
                <table style="font-size: 18px; font-weight: 400;">
                    <tbody>
                        <tr><td class="text-end">所属</td></tr>
                        <tr><td class="text-end">メールアドレス</td></tr>
                        <tr><td class="text-end">権限</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 col-xxl-9 px-4 py-4" style="background-color: #fff;">
                <table style="font-size: 18px; font-weight: 400;">
                    <tbody>
                        <tr><td class="text-start">{{ $teacher->affiliate }}</td></tr>
                        <tr><td class="text-start">{{ $teacher->email }}</td></tr>
                        <tr><td class="text-start">{{ $teacher->role }}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
        <a href="/teacher/list" class="btn btn-light" role="button" style="font-size: 18px; font-weight: 700px;">一覧に戻る</a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xxl-6 d-grid">
        <a href="/teacher/confirm/{{ $teacher->id }}" class="btn btn-light" role="button" style="font-size: 18px; font-weight: 700px;">先生情報削除</a>
    </div>
</div>
@endsection