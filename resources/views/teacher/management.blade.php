@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-2">
        <span class="heading-bold float-start">先生管理</span>
        <a href="/teacher/registration" class="btn btn-primary btn-lg rounded-pill float-end">先生新規登録</a>
    </div>
</div>

<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
        <table id="teacher-table" class="table table-borderless">
            <thead>
                <tr>
                    <th>所属</th>
                    <th>氏名</th>
                    <th>メールアドレス</th>
                    <th>権限</th>
                    <th>登録日時</th>
                    <th>更新日時</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($teacherList as $teacher)
                    <tr style="background-color: #f4f4f4;">
                        <td>{{ $teacher->affiliate }}</td>
                        <td>{{ $teacher->last_name }} {{ $teacher->first_name }}</td>
                        <td>{{ $teacher->email }}</td>
                        <td>{{ $teacher->role }}</td>
                        <td>{{ $teacher->created_at }}</td>
                        <td>{{ $teacher->updated_at }}</td>
                        <td><a href="/teacher/get/{{ $teacher->id }}"><i class="fa fa-chevron-right float-end arrow-pointer" aria-hidden="true"></i></a></td>
                    </tr>
                @empty
                    <p class="heading-content">画面上部の「先生新規登録」ボタンより先生を登録ください。</p>
                @endforelse
            </tbody>
        </table> 

    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/table-sort.js') }}"></script>
<script>
$(document).ready(function() {
    sortable("teacher-table");  
});
</script>
@endsection