@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-2">
        <span class="heading-bold float-start">レッスン予約キャンセル</span>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 px-4 py-4 round-corner" style="background-color: #f8f8f8;">
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <p class="heading-bold">レッスン予約をキャンセルしました</p>
        </div>
        <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
            <a href="/reservation/list" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
        </div>
    </div>
</div>
@endsection