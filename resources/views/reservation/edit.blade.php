@extends('app')

@section('content')
<div class="row px-4 py-4">
    <span class="heading-bold">予約管理</span>
    <p>予約登録</p>
    <div class="btn-group">
        <a href="#" id="breadcrumbs-one"   class="btn btn-primary pointer                 noHover" role="button" style="z-index: 3;">情報入力</a>
        <a href="#" id="breadcrumbs-two"   class="btn btn-outline-primary pointer-standby noHover" role="button" style="z-index: 2;">内容確認</a>
        <a href="#" id="breadcrumbs-three" class="btn btn-outline-primary pointer-last    noHover" role="button" style="z-index: 1;">登録完了</a>
    </div>
</div>
<div class="row">
    <div class="col-12 py-5 round-corner win-one">
        <div class="row px-5">
            <div class="col">
                <input type="hidden" id="reservation-slot-id" value="{{ $lessonDetails['slotInfo']['id'] }}">
                <label class="float-start bold-label" for="fld-res-date">レッスン予約日</label>
                <span class="hide-window err-res-date-req err-msg float-start">必須</span>
                <span class="hide-window err-res-date err-msg float-end"></span>
                
                <input type="hidden" name="" id="fld-res-booking" class="form-control" value="{{ $lessonDetails['booking'] }}">
                <input type="hidden" name="" id="fld-res-id" class="form-control" value="{{ $class }}">
                <input type="date" name="" id="fld-res-date" class="form-control" value="{{ $lessonDetails['slotInfo']['reservation_slot_date'] }}">
            </div>
            <div class="col">
                <label class="float-start bold-label" for="fld-res-time">開始時間</label>
                <span class="hide-window err-res-time-req err-msg float-start">必須</span>
                <span class="hide-window err-res-time err-msg float-end"></span>
                <input type="time" name="" id="fld-res-time" class="form-control" value="{{ $lessonDetails['slotInfo']['start_time'] }}">
            </div>
        </div>

        <div class="row px-5 pt-3">
            <div class="col">
                <label class="float-start bold-label" for="fld-res-type">繰り返し予約</label>
                <span class="hide-window err-res-type-req err-msg float-start">必須</span>
                <span class="hide-window err-res-type err-msg float-end"></span>
                <select class="form-control" id="fld-res-type">
                    <option value="1">繰り返さない</option>
                    <option value="2">毎週</option>
                    <option value="3">毎月（同週同曜日</option>
                </select>
            </div>
            <div class="col">
                <label class="fld-class bold-label" for="fld-res-deadline">繰り返し期限</label>
                <span class="hide-window err-class err-msg float-end"></span>
                <input type="date" name="" id="fld-res-deadline" class="form-control">
            </div>
        </div>

        <div class="row px-5 pt-3">
            <div class="col">
                <label for="fld-res-course" class="form-label float-start bold-label">コース</label>
                <span class="hide-window err-res-course-req err-msg float-start">必須</span>
                <span class="hide-window err-res-course err-msg float-end"></span>
                <select name="" id="fld-res-course" class="form-control">
                    @forelse ($courses as $course)
                        <option value="{{ $course->id }}" {{ ($lessonDetails['course'] == $course->id) ? 'selected' : '' }}>{{ $course->name }}</option>
                    @empty
                        <option value="">レコードが見つかりません</option>
                    @endforelse
                </select>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <div class="col">
                <label class="fld-year float-start bold-label" for="fld-res-year">学年</label>
                <span class="hide-window err-res-year-req err-msg float-start">必須</span>
                <span class="hide-window err-res-year err-msg float-end"></span>
                <select class="form-control" id="fld-res-year">
                    @forelse ($years as $year)
                        <option value="{{ $year->id }}" {{ ($lessonDetails['year'] == $year->id) ? 'selected' : '' }}>{{ $year->name }}</option>
                    @empty
                        <option value="">レコードが見つかりません</option>
                    @endforelse
                </select>
            </div>
            <div class="col">
                <label class="fld-res-class bold-label" for="">クラス</label>
                <span class="hide-window err-res-class err-msg float-end"></span>
                <input type="hidden" name="" id="fld-res-class-list" class="form-control" value={{ $class }}>
                <select name="" id="fld-res-class" class="form-control">
                    @forelse ($classes as $class)
                        <option value="{{ $class->id }}" {{ ($lessonDetails['class'] == $class->id) ? 'selected' : '' }}>{{ $class->name }}</option>
                    @empty
                        <option value="">レコードが見つかりません</option>
                    @endforelse
                </select>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <div class="col">
                <label for="fld-res-parti" class="form-label float-start bold-label">受講人数</label>
                <span class="hide-window err-res-parti-req err-msg float-start">必須</span>
                <span class="hide-window err-res-parti err-msg float-end"></span>
                <div class="input-group mb-3">
                    <input type="text" name="quantity" id="fld-res-parti" class="form-control quantity-field" value="1" readonly>
                    <button class="input-group-text button-minus" data-field="quantity">-</button>
                    <button class="input-group-text button-plus" data-field="quantity">+</button>
                </div>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <hr>
            <h5>受講生徒指定</h5>
            <hr>
            <p>受講する生徒を右側へ移動させてください</p>
            <div class="col-5">
                <label class="float-start bold-label" for="fld-res-candidate">候補一覧</label>
                <select id="fld-res-candidate" class="form-control mb-2" multiple="multiple">
                    @forelse ($lessonDetails['candidate'] as $student)
                        <option value="{{ $student['id'] }}">{{ $student['lastname'] }} {{ $student['firstname'] }}</option>
                    @empty
                    @endforelse
                </select>
                <a href="#" id="leftall" class="list-manage stud-reset" role="button">リセットする</a>
            </div>
            <div class="d-grid gap-2 col-2">
                <button id="left" class="btn btn-secondary btn-sm list-manage decrease"><<</button>
                <button id="right" class="btn btn-secondary btn-sm list-manage increase">>></button>
            </div>
            <div class="col-5">
                <label class="fld-class" for="fld-res-student">受講生徒</label>
                <select id="fld-res-student" class="form-control" multiple="multiple">
                    @forelse ($lessonDetails['students'] as $student)
                        <option value="{{ $student['id'] }}">{{ $student['lastname'] }} {{ $student['firstname'] }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div> 

        <div class="row px-5 pt-3">
            <hr>
            <div class="col">
                <h5>レッスン時間</h5>
                <span class="hide-window err-res-cat-req err-msg float-start">必須</span>
                <span class="hide-window err-res-cat err-msg float-end"></span>
                <div class="form-check">
                    <input class="form-check-input bold-label" type="radio" id="fld-res-period" value="25" checked>
                    <label class="form-check-label" for="fld-res-period">
                        25分
                    </label>
                </div>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <hr>
            <div class="col">
                <h5>教材選択</h5>
                <span class="hide-window err-res-cat-req err-msg float-start">必須</span>
                <span class="hide-window err-res-cat err-msg float-end"></span>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <hr>
            <div class="col">
                <label for="fld-res-cat" class="form-label float-start bold-label">カテゴリ</label>
                <span class="hide-window err-res-cat-req err-msg float-start">必須</span>
                <span class="hide-window err-res-cat err-msg float-end"></span>
                <select name="" id="fld-res-cat" class="form-control">
                    @forelse($textBooks as $textBook)
                        <option value="{{ $textBook->id }}">{{ $textBook->name }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <div class="col">
                <label for="fld-res-unit" class="form-label float-start bold-label">ユニット</label>
                <span class="hide-window err-res-unit-req err-msg float-start">必須</span>
                <span class="hide-window err-res-unit err-msg float-end"></span>
                <select name="" id="fld-res-unit" class="form-control mb-2">
                    <option value="レベル３">レベル３</option>
                </select>
                <a href="/material/change/{{ $slot }}" target="_blank">選択された教材を確認する</a>
            </div>
        </div>

        <div class="row px-5 pt-3">
            <div class="col">
                <label for="fld-res-memo" class="form-label float-start bold-label">メモ</label>
                <span class="hide-window err-res-memo-req err-msg float-start">必須</span>
                <span class="hide-window err-res-memo err-msg float-end"></span>
                <input type="text" name="" id="fld-res-memo" class="form-control">
            </div>
        </div>

        <div class="row px-5 pt-3">
                <div class="col d-grid gap-2"><a href="/reservation/list" class="btn btn-secondary" role="button">戻る</a></div>
                <div class="col d-grid gap-2"><a href="/reservation/cancel/{{ $lessonDetails['slotInfo']['id'] }}" class="btn btn-primary" role="button">予約をキャンセルする</a></div>
                <div class="col d-grid gap-2"><button id="btn-win-two" class="btn btn-primary" type="button">確認</button></div>
        </div>
    </div>

    <div class="col-12 py-4 round-corner win-two hide-window">
        <div class="row">
            <div class="col-6">
                <label class="float-start bold-label" for="fld-res-date">レッスン予約日</label>
                <span class="hide-window err-res-date-req err-msg float-start">必須</span>
            </div>
            <div class="col-6">
                <label class="float-start bold-label" for="fld-res-time">開始時間</label>
                <span class="hide-window err-res-time-req err-msg float-start">必須</span>
            </div>
            <div class="col-6">
                <p id="txt-res-date"></p>
            </div>
            <div class="col-6">
                <p id="txt-res-time"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <label class="float-start bold-label" for="fld-res-type">繰り返し予約</label>
                <span class="hide-window err-res-type-req err-msg float-start">必須</span>
                <span class="hide-window err-res-type err-msg float-end"></span>
            </div>
            <div class="col-6">
                <label class="fld-class bold-label" for="fld-res-deadline">繰り返し期限</label>
                <span class="hide-window err-class err-msg float-end"></span>
            </div>
            <div class="col-6">
                <p id="txt-res-type"></p>
            </div>
            <div class="col-6">
                <p id="txt-res-deadline"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label for="fld-res-course" class="form-label float-start bold-label">コース</label>
                <span class="hide-window err-res-course-req err-msg float-start">必須</span>
                <span class="hide-window err-res-course err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-res-course"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <label class="fld-year float-start bold-label" for="fld-res-year">学年</label>
                <span class="hide-window err-res-year-req err-msg float-start">必須</span>
                <span class="hide-window err-res-year err-msg float-end"></span>
            </div>
            <div class="col-6">
                <label class="fld-res-class bold-label" for="">クラス</label>
                <span class="hide-window err-res-class err-msg float-end"></span>
            </div>
            <div class="col-6">
                <p id="txt-res-year"></p>
            </div>
            <div class="col-6">
                <p id="txt-res-class"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label for="fld-res-parti bold-label" class="form-label float-start">受講人数</label>
                <span class="hide-window err-res-parti-req err-msg float-start">必須</span>
            </div>
            <div class="col-12">
                <p id="txt-res-parti"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h5>受講生徒指定</h5>
                <label class="fld-class bold-label" for="fld-res-student">受講生徒</label>
            </div>
            <div class="col-12">
                <p id="txt-res-student"></p>
            </div>
        </div>      

        <div class="row">
            <div class="col-12">
                <h5>レッスン時間</h5>
                <span class="hide-window err-res-cat-req err-msg float-start">必須</span>
                <span class="hide-window err-res-cat err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-res-period"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label for="fld-res-cat" class="form-label float-start bold-label">カテゴリ</label>
                <span class="hide-window err-res-cat-req err-msg float-start">必須</span>
                <span class="hide-window err-res-cat err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-res-cat"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label for="fld-res-unit" class="form-label float-start bold-label">ユニット</label>
                <span class="hide-window err-res-unit-req err-msg float-start">必須</span>
                <span class="hide-window err-res-unit err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-res-unit"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label for="fld-res-memo" class="form-label float-start bold-label">メモ</label>
                <span class="hide-window err-res-memo-req err-msg float-start">必須</span>
                <span class="hide-window err-res-memo err-msg float-end"></span>
            </div>
            <div class="col-12">
                <p id="txt-res-memo"></p>
            </div>
        </div>

        <div class="row">
            <div class="col d-grid gap-2"><button id="btn-win-one" class="btn btn-secondary" type="button">戻る</button></div>
            <div class="col d-grid gap-2"><button id="btn-win-three" class="btn btn-primary" type="button">上記の内容で予約する</button></div>
        </div>
    </div>

    <div class="col-12 py-4 round-corner win-three hide-window">
        <div class="row single-reg hide-window">
            <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
                    <h1 class="mb-5">予約が完了しました</h1>
                </div>
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <a href="/reservation/list" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
                </div>
            </div>

            <div class="row multiple-reg hide-window">
                <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
                    <h1 class="mb-5">予約結果</h1>
                </div>
                <div class="row">
                    <div class="col-3">
                    </div>
                    <div class="col-3">
                        <h5>予約不可</h5>
                        <ul id="list-failed" class="booked-summary">
                        </ul>
                    </div>
                    <div class="col-3">
                        <h5>予約完了</h5>
                        <ul id="list-booked" class="booked-summary">
                        </ul>
                    </div>
                    <div class="col-3">
                    </div>
                </div>
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <a href="/reservation/list" class="btn btn-secondary" type="button">一覧画面へ戻る</a>
                </div>
            </div>

            <div class="row failed-reg hide-window">
                <div class="col-12 d-grid mx-auto align-items-center justify-content-center">
                    <h1 class="mb-5">予約が完了できませんでした</h1>
                </div>
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <a href="#" class="btn btn-secondary" type="button">予約画面へ戻る</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/lesson-edit.js') }}"></script>
    <script src="{{ asset('js/input-number.js') }}"></script>
    <script src="{{ asset('js/list-item.js') }}"></script>
@endsection
