@extends('app')

@section('content')
<div class="row px-4">
    <div class="col-12 py-2">
        <span class="heading-bold float-start">レッスン予約キャンセル</span>
    </div>
</div>
<div class="row px-4 py-4">
    <div class="col-12 px-4 py-4 round-corner" style="background-color: #f8f8f8;">
        <div class="row px-2 py-2">
            <h2>レッスン予約をキャンセルします</h2>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
                <button type="button" class="btn btn-dark float-start">レッスン予約日時</button>
                <p class="heading-content float-start px-3">{{ $reservationDtls['slot']->date }}（月）　{{ $reservationDtls['slot']->start_time }} - {{ $reservationDtls['slot']->end_time }}</p>
            </div>
        </div>
        <div class="row px-2 py-2">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 col-xxl-3 px-4 py-4" style="background-color: #fff;">
                <table style="font-size: 18px; font-weight: 400;">
                    <tbody>
                        <tr><td class="text-end">形式</td></tr>
                        <tr><td class="text-end">教材</td></tr>
                        <tr><td class="text-end">予約ID</td></tr>
                        <tr><td class="text-end">学校名</td></tr>
                        <tr><td class="text-end">コース</td></tr>
                        <tr><td class="text-end">学年/クラス</td></tr>
                        <tr><td class="text-end">人数</td></tr>
                        <tr><td class="text-end">予約者ID</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9 col-xl-9 col-xxl-9 px-4 py-4" style="background-color: #fff;">
                <table style="font-size: 18px; font-weight: 400;">
                    <tbody>
                        <tr><td class="text-start">一斉レッスン</td></tr>
                        <tr>
                            <td class="text-start">
                                カテゴリ： ユニット：
                            </td>
                        </tr>
                        <tr><td class="text-start">{{ $reservationDtls['slot']->id }}</td></tr>
                        <tr><td class="text-start">--</td></tr>
                        <tr><td class="text-start">{{ $reservationDtls['class']->mst_course_id }}</td></tr>
                        <tr><td class="text-start">{{ $reservationDtls['class']->mst_class_id }}</td></tr>
                        <tr><td class="text-start">{{ $reservationDtls['count'] }}</td></tr>
                        <tr><td class="text-start">{{ $reservationDtls['slot']->created_by }}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row px-4">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 d-grid text-center">
        <p>選択したレッスン予約をキャンセルしますよろしければ確定ボタンを押してください</p>
    </div>
</div>
<div class="row px-4">
    <div class="col d-grid gap-2"><a href="/reservation/edit/{{ $reservationDtls['slot']->id }}/1" class="btn btn-secondary" role="button">戻る</a></div>
    <div class="col d-grid gap-2"><a href="/reservation/deleteReservation/{{ $reservationDtls['slot']->id }}" class="btn btn-primary" role="button">キャンセルする</a></div>
</div>
@endsection