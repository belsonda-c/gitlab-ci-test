@extends('app')

@section('content')
<div style=' padding-right:; padding-left: ;' class="container-fluid">
    <div class="row justify-content-center text-start ">
        <div class='col-lg-8'><span style="font-size: 34px; font-weight: 600;">予約管理  </span></div>   
        <div class='col-lg-2'><a style="float:right" href="{{route('/list')}}" class='btn btn-outline-primary rounded-pill float-right' >カレンダー表示</a></div>
        <div class='col-lg-2'><a  style="float:right" href="{{route('/list')}}" class='btn btn-primary rounded-pill float-right' >再読み込み</a></div>
    </div>
    <div class="row justify-content-center text-start pt-1 pb-1">  
        <div class='col-lg-7'><span style="font-size: 27px; font-weight: 200;"> 絞り込み検索</span></div>
        <div class='col-lg-3'></div>
        <div class='col-lg-2'> </a></div>
    </div>
</div>

<div style=' padding-right:; padding-left: ; ' class="container-fluid">       
    <div style='border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
          <div class="row text-start ">
              <div style=' align-content: center;  padding-top: 46px;' class='col-lg-4 justify-content-center text-end';>
              <form method='get' action="{{route('/search')}}">
                  <label style='padding-left:16px; padding-right:10px;'>コース</label>
                  <select name='fld-course' class="form-select" style=" width:270px; display:inline-block;">
                       @foreach($dataMstCourse as $list)
                       <option style='text-align: right;' value="{{$list->id}}">{{$list->name}}</option>
                       @endforeach
                  </select>
              </div>
              <div style=' align-content: center; padding-top: 46px;' class='col-lg-4 justify-content-center text-end';>
                  <label style='padding-left:16px; padding-right:10px;'>学年</label>
                  <select name='fld-schoolYear' class="form-select" style=" width:270px; display:inline-block;">
                  @foreach($dataMstYear as $list)
                       <option style='text-align: right;' value="{{$list->id}}">{{$list->name}}</option>
                       @endforeach
                  </select>
              </div>
              <div style=' align-content: center; padding-top: 46px;' class=' col-lg-4 justify-content-center text-start';>
                  <label style='padding-left:16px; padding-right:10px;'>クラス</label>
                  <select name='fld-class' class="form-select" style=" width:270px; display:inline-block;">
                  @foreach($dataMstClass as $list)
                       <option style='text-align: right;' value="{{$list->id}}">{{$list->name}}</option>
                       @endforeach
                  </select>
              </div>
          </div>
          <div class="row text-start ">
               <div style=' align-content: center;  padding-top: 16px;' class='col-lg-4 justify-content-center text-end';>
                  <label style='padding-left:16px; padding-right:10px;'>ステータス</label>
                  <select name='fld-status' class="form-select" style=" width:270px; display:inline-block;">
                  @foreach($data as $list)
                       <option style='text-align: right;' value="{{$list->status}}">{{$list->status}}</option>
                       @endforeach
                  </select>
              </div>
              <div style=' align-content: center; padding-top: 16px;' class='col-lg-4 justify-content-center text-end';>
                  <label style='padding-left:16px; padding-right:10px;'>予約ID</label>
                  <input type="text" style=" width:270px;" class="" name="fld-id" value="" placeholder="">
              </div>
              <div style=' align-content: center; padding-top: 16px;' class=' col-lg-4 justify-content-center text-start';>
                  <label style='padding-left:16px; padding-right:10px;'>予約者</label>
                  <input type="text" class="" style=" width:270px;" name="fld-person" value="" placeholder="">
              </div>
          </div>
          <div class="row text-start ">
               <div style=' align-content: center; padding-top: 16px;' class='col-lg-8 justify-content-center text-center';>
                  <label style='padding-left:16px; padding-right:10px;'>レッスン日</label>
                  <input type="text" name="daterange" class="" style="text-align: center; width:678px;" value="{{date('m/d/Y')}}-{{date('m/d/Y')}}" placeholder="">
              </div>
              <div style=' align-content: center;padding-left:90px; padding-top: 16px;padding-bottom: 32px' class=' col-lg-4 justify-content-center text-start';>
                  <input type="submit" class="btn btn-primary" style=" width:270px;" id="fld-name" value="検索" placeholder="">
              </div>
            </form>
          </div>
    </div>
</div>

@if (isset($paginationDataSearch))
<div style='padding-top:20px;   padding-right:; padding-left: ; ' class="container-fluid">{{$paginationDataSearch->total()}}件中 {{$paginationDataSearch->count()}} 件表示</div>
@foreach($paginationDataSearch as $reservationList)
<div style='padding-top:10px; padding-right:; padding-left: ; ' class="container-fluid">       
    <div style=' height:205px; border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
        <div class='row'>
            <div style=' align-content: center;  padding-top: 15px;' class='col-lg-6'>
                <div class='row'>
                    <div style='padding-left: 55px;'  class='col-lg-3'>
                    @if($reservationList->status==1)
                    <input type="submit" class="btn btn-primary" style="background: #60D764; border-radius: 5px; width:70px;" value="確定済" placeholder=""> 
                    @elseif ($reservationList->status==2)
                    <a href="/lesson-monitoring/logs/{{ $reservationList->id }}" class="btn btn-primary" style="background: #F9617A; border-radius: 5px; width:70px;">実施中</a>
                    @else
                    <input type="submit" class="btn btn-primary" style="background: #A7A7A7; border-radius: 5px; width:100px;" value="キャンセル" placeholder=""> 
                    @endif
                    </div>
                    <div class='text-start col-lg-9'>
                      
                    <div>{{$reservationList->reservation_slot_date}}</div>
                        <div>{{$reservationList->memo}}</div>
                        <div>{{$reservationList->textbookName}}</div>
                        <div>登録：{{$reservationList->createdDate}}</div>
                        <div>更新：{{$reservationList->updatedDate}}</div>
                    
                    </div>
                </div>
            </div>

            <div style=' align-content: center; padding-bottom: 10px; padding-top: 15px;' class='text-start col-lg-4';> 
                <div class="jumbotron jumbotron-fluid border text-start" style='margin-right:10px; float:right;  margin-left:0px; width:600px; height:180px; border-radius: 15px; background:#FfFfFf' >
                      <div class='row pt-4'>
                             <div style='border-right: 1px solid;' class='col-lg-5 justify-content-center text-end ';>
                                  <span style="font-size: 12px; font-weight: 200;"> 予約ID </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  学校 </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  コース </span><br>
                                  <span style="font-size: 12px; font-weight: 200;"> 学年/クラス </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  人数 </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  予約者ID </span><br>
                              </div>
                              <div class='col-lg-7'>
                                  <span style="font-size: 12px; font-weight: 200;"> {{$reservationList->id}} </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  {{$reservationList->school_id}} </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  {{$reservationList->course_name}}  </span><br>
                                  <span style="font-size: 12px; font-weight: 200;"> {{$reservationList->yearName}} / {{$reservationList->class_name}}  </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  {{$reservationList->student_count}}  </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  vacant </span><br>
                              </div>
                      </div>
                 </div>
            </div>
            <div style=' align-content: center;  padding-top: 60px;' class='col-lg-2'>
                <div class='row'>
                    <div style='padding-left: 55px;'  class='col-lg-2'>
                    <input type="submit" class="btn btn-primary" style=" width:70px;" id="fld-name" value="検索" placeholder=""> 
                    </div>
                    
                </div>
            </div>

        </div>

    </div>
</div>
   
@endforeach
<div style='padding-top:20px; margin-bottom:100px; height: 100px;  ' class="container-fluid"><div class="d-flex justify-content-center">
{{$paginationDataSearch->links('pagination.default')}}</div></div>

@else
<div style='padding-top:10px;   padding-right:; padding-left: ; ' class="container-fluid">
{{$paginationData->total()}}件中 {{$paginationData->count()}} 件表示</div>
@foreach($paginationData as $reservationList)

<div style='padding-top:10px; padding-right:; padding-left: ; ' class="container-fluid">       
    <div style=' height:205px; border-radius: 15px; background:#F4F4F4' class="jumbotron jumbotron-fluid border">
        <div class='row'>
            <div style=' align-content: center;  padding-top: 15px;' class='col-lg-6'>
                <div class='row'>
                    <div style='padding-left: 45px;'  class='col-lg-3'>
                    @if($reservationList->status==1)
                    <input type="submit" class="btn btn-primary" style="background: #60D764; border-radius: 5px; width:70px;" value="確定済" placeholder=""> 
                    @elseif ($reservationList->status==2)
                    <a href="/lesson-monitoring/logs/{{ $reservationList->id }}" class="btn btn-primary" style="background: #F9617A; border-radius: 5px; width:70px;">実施中</a>
                    @else
                    <input type="submit" class="btn btn-primary" style="background: #A7A7A7; border-radius: 5px; width:100px;" value="キャンセル" placeholder=""> 
                    @endif
                    </div>
                    <div class='text-start col-lg-9'>
                      
                        <div>{{$reservationList->reservation_slot_date}}</div>
                        <div>{{$reservationList->memo}}</div>
                        <div>{{$reservationList->textbookName}}</div>
                        <div>登録：{{$reservationList->createdDate}}</div>
                        <div>更新：{{$reservationList->updatedDate}}</div>
                    
                    </div>
                </div>
            </div>

            <div style=' align-content: center; padding-bottom: 10px; padding-top: 15px;' class='text-start col-lg-4';> 
                <div class="jumbotron jumbotron-fluid border text-start" style='margin-right:10px; float:right;  margin-left:0px; width:600px; height:180px; border-radius: 15px; background:#FfFfFf' >
                      <div class='row pt-4'>
                             <div style='border-right: 1px solid;' class='col-lg-5 justify-content-center text-end ';>
                                  <span style="font-size: 12px; font-weight: 200;"> 予約ID </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  学校 </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  コース </span><br>
                                  <span style="font-size: 12px; font-weight: 200;"> 学年/クラス </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  人数 </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  予約者ID </span><br>
                              </div>
                              <div class='col-lg-7'>
                                  <span style="font-size: 12px; font-weight: 200;"> {{$reservationList->id}} </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  {{$reservationList->school_id}} </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  {{$reservationList->course_name}}  </span><br>
                                  <span style="font-size: 12px; font-weight: 200;"> {{$reservationList->yearName}} / {{$reservationList->class_name}}  </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  {{$reservationList->student_count}}  </span><br>
                                  <span style="font-size: 12px; font-weight: 200;">  vacant </span><br>
                              </div>
                      </div>
                 </div>
            </div>
            <div style=' align-content: center;  padding-top: 60px;' class='col-lg-2'>
                <div class='row'>
                    <div style='padding-left: 55px;'  class='col-lg-2'>
                    <input type="submit" class="btn btn-primary" style=" width:70px;" id="fld-name" value="検索" placeholder=""> 
                    </div>
                    
                </div>
            </div>

        </div>

    </div>
</div> 
@endforeach
<div style='padding-top:10px; margin-bottom:100px; height: 100px; ' class="container-fluid"><div class="d-flex justify-content-center">
{{$paginationData->links('pagination.default')}}</div></div>

@endif

<script src="{{ asset('js/reservation.js') }}"></script>
@endsection