<!DOCTYPE html>
<html>
<head>
    <title>学校管理者</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body style="background-color: #e5e5e5;">
    <nav class="navbar navbar-light navbar-expand-lg">
        <div class="container-fluid">
            <a class="navbar-brand mr-auto" href="#"><img src="{{ asset('images/logo.png') }}" alt="" class="img-fluid"></a>
            @guest

            @else
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            @endguest
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    @guest

                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('signout') }}">Logout</a>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        @yield('content')
    </div>
    <footer class="footer fixed-bottom">
        <div class="container-fluid my-auto">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 pt-1">
                    <a href="#" class="float-start"><img src="{{ asset('images/logo_footer.png') }}" alt="" class="img-fluid"></a>
                    <a href="" class="float-start footer-nav mt-2 ms-2">株式会社エンビジョン</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 d-flex align-items-center justify-content-center">
                    <a href="" class="mt-1 footer-nav">利用規約</a>
                    <a href="" class="mt-1 ms-5 footer-nav">個人情報保護方針</a>
                    <a href="" class="mt-1 ms-5 footer-nav">よくあるご質問</a>
                    <a href="" class="mt-1 ms-5 footer-nav">お問い合わせ</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 pt-1">
                    <span class="footer-nav float-end">Copyright ©2020 Envizion, Inc. All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('script')
</body>

</html>