<style>
.dropdown-text{
    position:  relative;
}
.dropdown-text[data-bs-toggle].collapsed:after {
    font-family: "Font Awesome 5 Free"; 
    font-weight: 600;
    content: "\f106"; 
    color:  #a7a7a7;
    font-size: 20px;
    position: absolute;
    right: 10px;
}
.dropdown-text[data-bs-toggle]:not(.collapsed):after {
    font-family: "Font Awesome 5 Free"; 
    font-weight: 600;
    content: "\f105"; 
    color:  #a7a7a7;
    font-size: 20px;
    position: absolute;
    right: 10px;
}

.sidebar-header{
    width: 230px; 
    height:130px;
    background-color:#f8f8f8;
    margin-bottom:5px;
    margin-left:17px;
    border-radius:15px;
    position: absolute;
}

</style>





<style>
.dropdown-text{
    position:  relative;
}
.dropdown-text[data-bs-toggle].collapsed:after {
    font-family: "Font Awesome 5 Free"; 
    font-weight: 600;
    content: "\f106"; 
    color:  #a7a7a7;
    font-size: 20px;
    position: absolute;
    right: 10px;
}
.dropdown-text[data-bs-toggle]:not(.collapsed):after {
    font-family: "Font Awesome 5 Free"; 
    font-weight: 600;
    content: "\f105"; 
    color:  #a7a7a7;
    font-size: 20px;
    position: absolute;
    right: 10px;
}

.sidebar-header{
    width: 230px; 
    height:130px;
    background-color:#f8f8f8;
    margin-bottom:5px;
    margin-left:17px;
    border-radius:15px;
    position: absolute;
}

</style>

<div class="wrapper">

    {{-- <div class="d-grid gap-2 d-md-block breadcrumb">
        <button class="btn btn-primary escholar" type="button">Escholar</button>
        <i class="bi bi-chevron-right" aria-hidden="true"></i>
        <button class="btn btn-primary current" type="button">カレント</button>
    </div> --}}

    <div class="sidebar-header">
        <div style="padding:15px 15px 0 15px;">
            <h6 class="fw-bold">エンビジョン学園</h6>
        </div>
        <hr class="solid">
        <div style="padding:0 15px 0 15px;">
            <p class="m-0" style="font-size:12px;font-weight:bold;">受講コース</p>
            <p class="m-0" style="font-size:12px;">一斉/個別</p>
        </div>
    </div>

    <!-- Sidebar -->
    <nav id="sidebar" style="margin:120px 0 0 15px;">
        <ul class="list-unstyled components">
            <li class="active">
                <a href="#schoolManagement" data-bs-toggle="collapse" aria-expanded="false" class="dropdown-text">学校管理
                    <i class="bi bi-chevron-right" aria-hidden="true"></i>
                    <i class="bi bi-chevron-up" aria-hidden="true"></i>
                </a>
                <ul class="collapse list-unstyled sub-list" id="schoolManagement"> 
                    <li>
                        <a href="#"> 学校一覧 </a>
                    </li>
                    <li>
                        <a href="#"> 学校新規登録 </a>
                    </li>
                </ul>
            </li>

            <hr class="solid">
            <li class="active">
                <a href="#management" data-bs-toggle="collapse" aria-expanded="false" class="dropdown-text">先生管理
                    <i class="bi bi-chevron-right" aria-hidden="true"></i>
                    <i class="bi bi-chevron-up" aria-hidden="true"></i>
                </a>
                <ul class="collapse list-unstyled sub-list" id="management">
                    <li>
                        <a href="/teacher/list"> 先生一覧 </a>
                    </li>
                    <li>
                        <a href="/teacher/registration"> 先生新規登録 </a>
                    </li>
                </ul>
            </li>            
    
            <hr class="solid">
            <li class="active">
                <a href="#classManagement" data-bs-toggle="collapse" aria-expanded="false" class="dropdown-text">クラス管理
                    <i class="bi bi-chevron-right" aria-hidden="true"></i>
                    <i class="bi bi-chevron-up" aria-hidden="true"></i>
                </a>
                <ul class="collapse list-unstyled sub-list" id="classManagement">
                    <li>
                        <a href="/class/manage"> クラス一覧 </a>
                    </li>
                    <li>
                        <a href="/class/registration"> クラス新規登録 </a>
                    </li>
                </ul>
            </li>   

            <hr class="solid">
            <li class="active">
                <a href="#studentManagement" data-bs-toggle="collapse" aria-expanded="false" class="dropdown-text">生徒管理
                    <i class="bi bi-chevron-right" aria-hidden="true"></i>
                    <i class="bi bi-chevron-up" aria-hidden="true"></i>
                </a>
                <ul class="collapse list-unstyled sub-list" id="studentManagement">
                    <li>
                        <a href="/student/status"> 生徒登録状況 </a>
                    </li>
                    <li>
                        <a href="/teacher/dashboard"> 生徒利用状況 </a>
                    </li>
                    <li>
                        <a href="/student/registration"> 生徒新規登録 </a>
                    </li>                    
                </ul>
            </li>       

            <!-- Reservation Management -->
            <hr class="solid">
            <li class="active">
                <a href="#reservationManagement" data-bs-toggle="collapse" aria-expanded="false" class="dropdown-text">予約管理
                    <i class="bi bi-chevron-right" aria-hidden="true"></i>
                    <i class="bi bi-chevron-up" aria-hidden="true"></i>
                </a>
                <ul class="collapse list-unstyled sub-list" id="reservationManagement">
                    <li>
                        <a href="/lesson/reservation"> レッスン予約 </a>
                    </li>
                    <li>
                        <a href="#"> 確認・変更 </a>
                    </li>                 
                </ul>
            </li>  
            
            <hr class="solid">
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-text">マニュアル</a>
            </li> 
            
        </ul>
    </nav>
</div>
    
<script type="text/javascript">
    var dropdownText = document.getElementsByClassName("dropdown-text");
    
    for (var i = 0; i < dropdownText.length; i++) {
        dropdownText[i].onclick = function() {
            this.classList.toggle('collapsed');
        }
    }
</script>
    
