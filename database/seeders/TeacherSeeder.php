<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Teacher;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $randPassword = 'Teacher@2022';
        Teacher::create([
            "school_id" => 1,
            "first_name" => 'Administrator',
            "last_name" => 'Teacher',
            "first_name_kana" => 'Administrator',
            "last_name_kana" => 'Teacher',
            "email" => 'teacheradministrator@westacton.com.jp',
            "password" => Hash::make($randPassword),
            "role" => 2,
            "is_verified" => 0
        ]);

        Teacher::create([
            "school_id" => 2,
            "first_name" => 'Head',
            "last_name" => 'Teacher',
            "first_name_kana" => 'Head',
            "last_name_kana" => 'Teacher',
            "email" => 'teacherhead@westacton.com.jp',
            "password" => Hash::make($randPassword),
            "role" => 2,
            "is_verified" => 0
        ]);
    }
}
