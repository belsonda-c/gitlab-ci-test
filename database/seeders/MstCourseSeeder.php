<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MstCourse;

class MstCourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('mst_courses')->insert([
            ['school_id' => 1, 'name' => 'Course 1'],
            ['school_id' => 1, 'name' => 'Course 2'],
            ['school_id' => 1, 'name' => 'Course 3'],
            ['school_id' => 1, 'name' => 'Course 4'],
            ['school_id' => 1, 'name' => 'Course 5'],
        ]);
    }
}
