<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MstTextbooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_textbooks', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('mst_textbook_category_id', 100);
            $table->string('unit', 100);
            $table->string('student_url', 300);
            $table->string('teacher_url', 300);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_textbooks');
    }
}
