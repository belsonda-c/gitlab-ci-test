<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->date('lesson_date');
            $table->time('lesson_start_time');
            $table->time('lesson_end_time');
            $table->tinyInteger('duration');
            $table->integer('student_id');
            $table->integer('class_list_id');
            $table->integer('teacher_id');
            $table->integer('mst_textbook_id');
            $table->string('memo', 1000);
            $table->string('status', 50);
            $table->string('updated_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_reservations');
    }
}
