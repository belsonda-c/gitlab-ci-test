<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonConditionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('lesson_condition_logs');
        Schema::create('lesson_condition_logs', function (Blueprint $table) {
            $table->id();
            $table->integer("lesson_reservation_id");
            $table->integer("actor_id");
            $table->integer("actor_type");
            $table->integer("action");
            $table->timestamp("action_datetime");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_condition_logs');
    }
}
