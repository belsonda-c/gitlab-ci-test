<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SchoolReservationSlots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_reservation_slots', function (Blueprint $table) {
            $table->id();
            $table->integer('school_id');
            $table->date('reservation_slot_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->integer('slot_count');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->string('deleted_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_reservation_slots');
    }
}
