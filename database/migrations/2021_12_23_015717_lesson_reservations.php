<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LessonReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_reservations', function (Blueprint $table) {
            $table->id();
            $table->date('lesson_date');
            $table->integer('school_reservation_slot_id');
            $table->tinyInteger('duration');
            $table->integer('student_id');
            $table->integer('class_list_id');
            $table->integer('tutor_id')->default(0);
            $table->integer('mst_textbook_id');
            $table->string('memo', 1000);
            $table->string('status', 50);
            $table->string('updated_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_reservations');
    }
}
