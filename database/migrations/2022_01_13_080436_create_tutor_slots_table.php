<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_slots', function (Blueprint $table) {
            $table->id();
            $table->integer('tutor_id');
            $table->integer('reservation_slot_id');
            $table->tinyInteger('status')->comment('1 - open, 2 - booked');
            $table->timestamps();
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->softDeletes();
            $table->string('deleted_by', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_slots');
    }
}
