<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClassAttendees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_attendees', function (Blueprint $table) {
            $table->id();
            $table->integer('class_list_id');
            $table->integer('student_id');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->string('deleted_by', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_attendees');
    }
}
