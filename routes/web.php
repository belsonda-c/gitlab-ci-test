<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\ClassesController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\LessonReservationController;
use App\Http\Controllers\SchoolReservationSlotController;
use App\Http\Controllers\LessonMonitoringController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('loginProcess', [CustomAuthController::class, 'login']);
Route::get('signout', [CustomAuthController::class, 'signOut']);

Route::group(['prefix' => 'password'], function() { 
    Route::get('/forget', [CustomAuthController::class, 'forget']);
    Route::post('/reset/link', [CustomAuthController::class, 'resetLink']);
    Route::get('/reset/{token}', [CustomAuthController::class, 'resetForm']);
    Route::post('/reset', [CustomAuthController::class, 'passwordReset']);
});

Route::group(['middleware' => ['auth:teacher'], 'prefix' => 'teacher'], function() {
    Route::get('/dashboard', [TeacherController::class, 'dashboard']);
    Route::get('/list', [TeacherController::class, 'index']);
    Route::get('/registration', [TeacherController::class, 'create']);
    Route::post('/register', [TeacherController::class, 'store']);
    Route::get('/edit/{id}', [TeacherController::class, 'edit']);
    Route::post('/update/{id}', [TeacherController::class, 'update']);
    Route::get('/get/{id}', [TeacherController::class, 'show']);
    Route::get('/confirm/{id}', [TeacherController::class, 'confirmDelete']);
    Route::delete('/delete/{id}', [TeacherController::class, 'destroy']);
    Route::get('/deleted', [TeacherController::class, 'remove']);
});

Route::group(['prefix'=>'class'], function(){
    Route::get('/manage',[ClassesController::class,'showAll'])->name('/manage');
    Route::get('/search',[ClassesController::class,'search'])->name('/search');
    Route::get('/registration',[ClassesController::class,'register'])->name('/registration');
    Route::get('/view/{id}', [ClassesController::class, 'classView'])->name('/view');
    Route::post('/register', [ClassesController::class, 'store'])->name('/register');
    Route::get('/delete/{id}', [ClassesController::class, 'confirmDelete'])->name('/delete');
    Route::delete('/confirm/{id}', [ClassesController::class, 'destroy']);
    Route::get('/deleted}', [ClassesController::class, 'remove'])->name('/deleted');
});

Route::group(['prefix'=>'student'], function(){
    Route::get('/record', [StudentController::class, 'record']);
    Route::get('/registration', [StudentController::class, 'create']);
    Route::post('/register', [StudentController::class, 'store']);
    Route::get('/details/{id}', [StudentController::class, 'details']);
    Route::get('/update/{id}', [StudentController::class, 'edit']);
    Route::post('/updated/{id}', [StudentController::class, 'update'])->name('/updated');
    Route::get('/delete/{id}', [StudentController::class, 'delete']); 
    Route::delete('/delete-confirm/{id}', [StudentController::class, 'de stroy']);
    Route::get('/deleted', [StudentController::class, 'deleted']);
    Route::get('/status', [StudentController::class, 'status']);
    Route::get('/condition/{schoolId}/{classId}', [StudentController::class, 'condition']);
    Route::get('/generate-pdf-details/{id}', [StudentController::class, 'generatePdfDetails']);
    Route::get('/generate-pdf-class-list/{id}', [StudentController::class, 'generatePdfByClassListId']);
});

Route::group(['prefix'=>'material'], function(){
    Route::get('/change/{id}', [MaterialController::class, 'change']);
    Route::post('/update/{id}', [MaterialController::class, 'update']);
});

Route::group(["prefix" => "lesson-monitoring"], function() {
    Route::get("/logs/{lessonReservationId}", [ LessonMonitoringController::class, "logs"]);
    Route::get("/callback", [ LessonMonitoringController::class, "bellbirdCallback"]);
});

Route::group(['prefix'=>'reservation'], function(){
    Route::get('/list',[LessonReservationController::class,'list'])->name('/list');
    Route::get('/edit/{id}',[LessonReservationController::class, 'edit']);
    Route::post('/update/{id}',[LessonReservationController::class, 'update']);
    Route::get('/cancel/{id}',[LessonReservationController::class, 'cancel']);
    Route::get('/booking/{id}/{datetime}',[LessonReservationController::class, 'booking']);
    Route::post('/reserve',[LessonReservationController::class, 'reserve']);

    Route::group(['prefix'=>'api'], function(){
        Route::post('/checkClass',[LessonReservationController::class, 'checkClass']);
    });

    Route::post('/reservation',[LessonReservationController::class, 'reservation']);
    Route::get('/edit/{slot}/{class}',[LessonReservationController::class, 'edit']);
    Route::post('/update/{id}',[LessonReservationController::class, 'update']);
    Route::get('/cancel/{id}',[LessonReservationController::class, 'cancel']);
    Route::get('/deleteReservation/{id}',[LessonReservationController::class, 'deleteByReservationSlotId']);
    Route::get('/slots',[SchoolReservationSlotController::class, 'show']);
});

Route::group(['middleware' => ['auth:teacher'], 'prefix'=>'lesson'], function(){
    Route::get('/reservation', [LessonReservationController::class, 'index']);
    Route::get('/reservation/view', [LessonReservationController::class, 'view']);
    Route::post('/reservation', [LessonReservationController::class, 'store']);
    Route::post('/reservation/verify', [LessonReservationController::class, 'exists']);
    Route::delete('/reservation/{id}', [LessonReservationController::class, 'destroy']);
    Route::get('/search',[LessonReservationController::class,'search'])->name('/search');
});