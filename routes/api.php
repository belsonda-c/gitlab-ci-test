<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\LessonMonitoringController;
use App\Http\Controllers\LessonReservationController;
use App\Http\Controllers\ClassesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/lesson-monitoring", [ LessonMonitoringController::class, "getAll"]);

Route::group(["prefix" => "lesson-reservation"], function() {
    Route::get("", [ LessonReservationController::class, "getAll"]);
    Route::get("/{lessonReservationId}", [ LessonReservationController::class, "getById"]);
});

Route::group(["prefix" => "class"], function() {
    Route::get("", [ ClassesController::class, "apiGetAll"]);
    Route::get("/{id}", [ ClassesController::class, "apiGetById"]);
});


